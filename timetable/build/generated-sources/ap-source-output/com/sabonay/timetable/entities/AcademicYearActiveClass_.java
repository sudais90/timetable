package com.sabonay.timetable.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-02-27T15:02:50")
@StaticMetamodel(AcademicYearActiveClass.class)
public class AcademicYearActiveClass_ { 

    public static volatile SingularAttribute<AcademicYearActiveClass, String> schoolClass;
    public static volatile SingularAttribute<AcademicYearActiveClass, String> updated;
    public static volatile SingularAttribute<AcademicYearActiveClass, String> schoolNumber;
    public static volatile SingularAttribute<AcademicYearActiveClass, String> academicYearClassId;
    public static volatile SingularAttribute<AcademicYearActiveClass, String> lastModifiedBy;
    public static volatile SingularAttribute<AcademicYearActiveClass, Date> lastModifiedDate;
    public static volatile SingularAttribute<AcademicYearActiveClass, String> deleted;
    public static volatile SingularAttribute<AcademicYearActiveClass, String> academicYear;

}