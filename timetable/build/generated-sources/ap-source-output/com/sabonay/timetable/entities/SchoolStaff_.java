package com.sabonay.timetable.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-02-27T15:02:50")
@StaticMetamodel(SchoolStaff.class)
public class SchoolStaff_ { 

    public static volatile SingularAttribute<SchoolStaff, String> region;
    public static volatile SingularAttribute<SchoolStaff, Date> dateOfBirth;
    public static volatile SingularAttribute<SchoolStaff, String> contactNumber;
    public static volatile SingularAttribute<SchoolStaff, String> inService;
    public static volatile SingularAttribute<SchoolStaff, String> staffId;
    public static volatile SingularAttribute<SchoolStaff, String> hometown;
    public static volatile SingularAttribute<SchoolStaff, String> surname;
    public static volatile SingularAttribute<SchoolStaff, String> emailAddress;
    public static volatile SingularAttribute<SchoolStaff, String> maritalStatus;
    public static volatile SingularAttribute<SchoolStaff, String> deleted;
    public static volatile SingularAttribute<SchoolStaff, byte[]> picture;
    public static volatile SingularAttribute<SchoolStaff, String> updated;
    public static volatile SingularAttribute<SchoolStaff, String> schoolNumber;
    public static volatile SingularAttribute<SchoolStaff, String> subjectTeachingId;
    public static volatile SingularAttribute<SchoolStaff, String> diabilitiesAllergies;
    public static volatile SingularAttribute<SchoolStaff, String> gender;
    public static volatile SingularAttribute<SchoolStaff, Date> dateOfAppointment;
    public static volatile SingularAttribute<SchoolStaff, String> othernames;
    public static volatile SingularAttribute<SchoolStaff, String> regionId;
    public static volatile SingularAttribute<SchoolStaff, String> staffCategory;

}