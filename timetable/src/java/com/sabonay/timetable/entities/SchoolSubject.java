/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sabonay.timetable.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Amina
 */
@Entity
@Table(name = "school_subject")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SchoolSubject.findAll", query = "SELECT s FROM SchoolSubject s"),
    @NamedQuery(name = "SchoolSubject.findBySelectedSubject", query = "SELECT s FROM SchoolSubject s WHERE s.selectedSubject = :selectedSubject"),
    @NamedQuery(name = "SchoolSubject.findBySubjectCode", query = "SELECT s FROM SchoolSubject s WHERE s.subjectCode = :subjectCode"),
    @NamedQuery(name = "SchoolSubject.findBySubjectName", query = "SELECT s FROM SchoolSubject s WHERE s.subjectName = :subjectName"),
    @NamedQuery(name = "SchoolSubject.findBySubjectInitials", query = "SELECT s FROM SchoolSubject s WHERE s.subjectInitials = :subjectInitials"),
    @NamedQuery(name = "SchoolSubject.findBySubjectCategory", query = "SELECT s FROM SchoolSubject s WHERE s.subjectCategory = :subjectCategory"),
    @NamedQuery(name = "SchoolSubject.findByDeleted", query = "SELECT s FROM SchoolSubject s WHERE s.deleted = :deleted"),
    @NamedQuery(name = "SchoolSubject.findByUpdated", query = "SELECT s FROM SchoolSubject s WHERE s.updated = :updated")})
public class SchoolSubject implements Serializable {
    private static final long serialVersionUID = 1L;
    @Column(name = "selected_subject")
    private Boolean selectedSubject;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "subject_code")
    private String subjectCode;
    @Size(max = 255)
    @Column(name = "subject_name")
    private String subjectName;
    @Size(max = 100)
    @Column(name = "subject_initials")
    private String subjectInitials;
    @Size(max = 255)
    @Column(name = "subject_category")
    private String subjectCategory;
    @Size(max = 255)
    @Column(name = "deleted")
    private String deleted;
    @Size(max = 255)
    @Column(name = "updated")
    private String updated;

    public SchoolSubject() {
    }

    public SchoolSubject(String subjectCode) {
        this.subjectCode = subjectCode;
    }

    public Boolean getSelectedSubject() {
        return selectedSubject;
    }

    public void setSelectedSubject(Boolean selectedSubject) {
        this.selectedSubject = selectedSubject;
    }

    public String getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getSubjectInitials() {
        return subjectInitials;
    }

    public void setSubjectInitials(String subjectInitials) {
        this.subjectInitials = subjectInitials;
    }

    public String getSubjectCategory() {
        return subjectCategory;
    }

    public void setSubjectCategory(String subjectCategory) {
        this.subjectCategory = subjectCategory;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (subjectCode != null ? subjectCode.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SchoolSubject)) {
            return false;
        }
        SchoolSubject other = (SchoolSubject) object;
        if ((this.subjectCode == null && other.subjectCode != null) || (this.subjectCode != null && !this.subjectCode.equals(other.subjectCode))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sabonay.timetable.entities.SchoolSubject[ subjectCode=" + subjectCode + " ]";
    }
    
}
