package com.sabonay.timetable.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-02-27T15:02:50")
@StaticMetamodel(Classroom.class)
public class Classroom_ { 

    public static volatile SingularAttribute<Classroom, String> updated;
    public static volatile SingularAttribute<Classroom, Integer> classroomId;
    public static volatile SingularAttribute<Classroom, String> deleted;
    public static volatile SingularAttribute<Classroom, String> classroomName;

}