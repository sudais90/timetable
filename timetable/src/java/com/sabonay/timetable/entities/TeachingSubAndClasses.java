/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sabonay.timetable.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Amina
 */
@Entity
@Table(name = "teaching_sub_and_classes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TeachingSubAndClasses.findAll", query = "SELECT t FROM TeachingSubAndClasses t"),
    @NamedQuery(name = "TeachingSubAndClasses.findByTeachingSubAndClassesId", query = "SELECT t FROM TeachingSubAndClasses t WHERE t.teachingSubAndClassesId = :teachingSubAndClassesId"),
    @NamedQuery(name = "TeachingSubAndClasses.findByTeacherId", query = "SELECT t FROM TeachingSubAndClasses t WHERE t.teacherId = :teacherId"),
    @NamedQuery(name = "TeachingSubAndClasses.findBySubject", query = "SELECT t FROM TeachingSubAndClasses t WHERE t.subject = :subject"),
    @NamedQuery(name = "TeachingSubAndClasses.findByAcademicTerm", query = "SELECT t FROM TeachingSubAndClasses t WHERE t.academicTerm = :academicTerm"),
    @NamedQuery(name = "TeachingSubAndClasses.findBySchoolNumber", query = "SELECT t FROM TeachingSubAndClasses t WHERE t.schoolNumber = :schoolNumber"),
    @NamedQuery(name = "TeachingSubAndClasses.findByDeleted", query = "SELECT t FROM TeachingSubAndClasses t WHERE t.deleted = :deleted"),
    @NamedQuery(name = "TeachingSubAndClasses.findByUpdated", query = "SELECT t FROM TeachingSubAndClasses t WHERE t.updated = :updated")})
public class TeachingSubAndClasses implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "teaching_sub_and_classes_id")
    private String teachingSubAndClassesId;
    @Size(max = 20)
    @Column(name = "teacher_id")
    private String teacherId;
    @Size(max = 50)
    @Column(name = "subject")
    private String subject;
    @Lob
    @Size(max = 65535)
    @Column(name = "teaching_classes")
    private String teachingClasses;
    @Size(max = 20)
    @Column(name = "academic_term")
    private String academicTerm;
    @Size(max = 20)
    @Column(name = "school_number")
    private String schoolNumber;
    @Size(max = 5)
    @Column(name = "deleted")
    private String deleted;
    @Size(max = 5)
    @Column(name = "updated")
    private String updated;

    public TeachingSubAndClasses() {
    }

    public TeachingSubAndClasses(String teachingSubAndClassesId) {
        this.teachingSubAndClassesId = teachingSubAndClassesId;
    }

    public String getTeachingSubAndClassesId() {
        return teachingSubAndClassesId;
    }

    public void setTeachingSubAndClassesId(String teachingSubAndClassesId) {
        this.teachingSubAndClassesId = teachingSubAndClassesId;
    }

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTeachingClasses() {
        return teachingClasses;
    }

    public void setTeachingClasses(String teachingClasses) {
        this.teachingClasses = teachingClasses;
    }

    public String getAcademicTerm() {
        return academicTerm;
    }

    public void setAcademicTerm(String academicTerm) {
        this.academicTerm = academicTerm;
    }

    public String getSchoolNumber() {
        return schoolNumber;
    }

    public void setSchoolNumber(String schoolNumber) {
        this.schoolNumber = schoolNumber;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (teachingSubAndClassesId != null ? teachingSubAndClassesId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TeachingSubAndClasses)) {
            return false;
        }
        TeachingSubAndClasses other = (TeachingSubAndClasses) object;
        if ((this.teachingSubAndClassesId == null && other.teachingSubAndClassesId != null) || (this.teachingSubAndClassesId != null && !this.teachingSubAndClassesId.equals(other.teachingSubAndClassesId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sabonay.timetable.entities.TeachingSubAndClasses[ teachingSubAndClassesId=" + teachingSubAndClassesId + " ]";
    }
    
}
