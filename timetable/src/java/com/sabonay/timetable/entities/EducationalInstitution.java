/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sabonay.timetable.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Amina
 */
@Entity
@Table(name = "educational_institution")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EducationalInstitution.findAll", query = "SELECT e FROM EducationalInstitution e"),
    @NamedQuery(name = "EducationalInstitution.findBySchoolNumber", query = "SELECT e FROM EducationalInstitution e WHERE e.schoolNumber = :schoolNumber"),
    @NamedQuery(name = "EducationalInstitution.findBySchoolMotor", query = "SELECT e FROM EducationalInstitution e WHERE e.schoolMotor = :schoolMotor"),
    @NamedQuery(name = "EducationalInstitution.findBySchoolAlias", query = "SELECT e FROM EducationalInstitution e WHERE e.schoolAlias = :schoolAlias"),
    @NamedQuery(name = "EducationalInstitution.findBySchoolName", query = "SELECT e FROM EducationalInstitution e WHERE e.schoolName = :schoolName"),
    @NamedQuery(name = "EducationalInstitution.findByEducationInstitutionCycle", query = "SELECT e FROM EducationalInstitution e WHERE e.educationInstitutionCycle = :educationInstitutionCycle"),
    @NamedQuery(name = "EducationalInstitution.findByStudentUpdatePersonalInfo", query = "SELECT e FROM EducationalInstitution e WHERE e.studentUpdatePersonalInfo = :studentUpdatePersonalInfo"),
    @NamedQuery(name = "EducationalInstitution.findByAverageExamScore", query = "SELECT e FROM EducationalInstitution e WHERE e.averageExamScore = :averageExamScore"),
    @NamedQuery(name = "EducationalInstitution.findByTotalExamMark", query = "SELECT e FROM EducationalInstitution e WHERE e.totalExamMark = :totalExamMark"),
    @NamedQuery(name = "EducationalInstitution.findByTotalClassMark", query = "SELECT e FROM EducationalInstitution e WHERE e.totalClassMark = :totalClassMark"),
    @NamedQuery(name = "EducationalInstitution.findBySchoolContactNumber", query = "SELECT e FROM EducationalInstitution e WHERE e.schoolContactNumber = :schoolContactNumber"),
    @NamedQuery(name = "EducationalInstitution.findByCurrentTerm", query = "SELECT e FROM EducationalInstitution e WHERE e.currentTerm = :currentTerm"),
    @NamedQuery(name = "EducationalInstitution.findByEducationalLevel", query = "SELECT e FROM EducationalInstitution e WHERE e.educationalLevel = :educationalLevel"),
    @NamedQuery(name = "EducationalInstitution.findBySchoolAddress", query = "SELECT e FROM EducationalInstitution e WHERE e.schoolAddress = :schoolAddress"),
    @NamedQuery(name = "EducationalInstitution.findByAverageClassScore", query = "SELECT e FROM EducationalInstitution e WHERE e.averageClassScore = :averageClassScore"),
    @NamedQuery(name = "EducationalInstitution.findByDateOfRegistration", query = "SELECT e FROM EducationalInstitution e WHERE e.dateOfRegistration = :dateOfRegistration"),
    @NamedQuery(name = "EducationalInstitution.findByMasterUsername", query = "SELECT e FROM EducationalInstitution e WHERE e.masterUsername = :masterUsername"),
    @NamedQuery(name = "EducationalInstitution.findByMasterPassword", query = "SELECT e FROM EducationalInstitution e WHERE e.masterPassword = :masterPassword"),
    @NamedQuery(name = "EducationalInstitution.findBySystemStatus", query = "SELECT e FROM EducationalInstitution e WHERE e.systemStatus = :systemStatus"),
    @NamedQuery(name = "EducationalInstitution.findByGradingSystem", query = "SELECT e FROM EducationalInstitution e WHERE e.gradingSystem = :gradingSystem"),
    @NamedQuery(name = "EducationalInstitution.findBySchSendingId", query = "SELECT e FROM EducationalInstitution e WHERE e.schSendingId = :schSendingId")})
public class EducationalInstitution implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "school_number")
    private String schoolNumber;
    @Size(max = 255)
    @Column(name = "school_motor")
    private String schoolMotor;
    @Size(max = 244)
    @Column(name = "school_alias")
    private String schoolAlias;
    @Size(max = 255)
    @Column(name = "school_name")
    private String schoolName;
    @Size(max = 255)
    @Column(name = "education_institution_cycle")
    private String educationInstitutionCycle;
    @Size(max = 255)
    @Column(name = "student_update_personal_info")
    private String studentUpdatePersonalInfo;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "average_exam_score")
    private Double averageExamScore;
    @Column(name = "total_exam_mark")
    private Double totalExamMark;
    @Column(name = "total_class_mark")
    private Double totalClassMark;
    @Size(max = 255)
    @Column(name = "school_contact_number")
    private String schoolContactNumber;
    @Size(max = 255)
    @Column(name = "current_term")
    private String currentTerm;
    @Size(max = 255)
    @Column(name = "educational_level")
    private String educationalLevel;
    @Size(max = 255)
    @Column(name = "school_address")
    private String schoolAddress;
    @Column(name = "average_class_score")
    private Double averageClassScore;
    @Column(name = "date_of_registration")
    @Temporal(TemporalType.DATE)
    private Date dateOfRegistration;
    @Size(max = 200)
    @Column(name = "master_username")
    private String masterUsername;
    @Size(max = 200)
    @Column(name = "master_password")
    private String masterPassword;
    @Size(max = 50)
    @Column(name = "system_status")
    private String systemStatus;
    @Column(name = "grading_system")
    private Integer gradingSystem;
    @Size(max = 11)
    @Column(name = "sch_sending_id")
    private String schSendingId;

    public EducationalInstitution() {
    }

    public EducationalInstitution(String schoolNumber) {
        this.schoolNumber = schoolNumber;
    }

    public String getSchoolNumber() {
        return schoolNumber;
    }

    public void setSchoolNumber(String schoolNumber) {
        this.schoolNumber = schoolNumber;
    }

    public String getSchoolMotor() {
        return schoolMotor;
    }

    public void setSchoolMotor(String schoolMotor) {
        this.schoolMotor = schoolMotor;
    }

    public String getSchoolAlias() {
        return schoolAlias;
    }

    public void setSchoolAlias(String schoolAlias) {
        this.schoolAlias = schoolAlias;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getEducationInstitutionCycle() {
        return educationInstitutionCycle;
    }

    public void setEducationInstitutionCycle(String educationInstitutionCycle) {
        this.educationInstitutionCycle = educationInstitutionCycle;
    }

    public String getStudentUpdatePersonalInfo() {
        return studentUpdatePersonalInfo;
    }

    public void setStudentUpdatePersonalInfo(String studentUpdatePersonalInfo) {
        this.studentUpdatePersonalInfo = studentUpdatePersonalInfo;
    }

    public Double getAverageExamScore() {
        return averageExamScore;
    }

    public void setAverageExamScore(Double averageExamScore) {
        this.averageExamScore = averageExamScore;
    }

    public Double getTotalExamMark() {
        return totalExamMark;
    }

    public void setTotalExamMark(Double totalExamMark) {
        this.totalExamMark = totalExamMark;
    }

    public Double getTotalClassMark() {
        return totalClassMark;
    }

    public void setTotalClassMark(Double totalClassMark) {
        this.totalClassMark = totalClassMark;
    }

    public String getSchoolContactNumber() {
        return schoolContactNumber;
    }

    public void setSchoolContactNumber(String schoolContactNumber) {
        this.schoolContactNumber = schoolContactNumber;
    }

    public String getCurrentTerm() {
        return currentTerm;
    }

    public void setCurrentTerm(String currentTerm) {
        this.currentTerm = currentTerm;
    }

    public String getEducationalLevel() {
        return educationalLevel;
    }

    public void setEducationalLevel(String educationalLevel) {
        this.educationalLevel = educationalLevel;
    }

    public String getSchoolAddress() {
        return schoolAddress;
    }

    public void setSchoolAddress(String schoolAddress) {
        this.schoolAddress = schoolAddress;
    }

    public Double getAverageClassScore() {
        return averageClassScore;
    }

    public void setAverageClassScore(Double averageClassScore) {
        this.averageClassScore = averageClassScore;
    }

    public Date getDateOfRegistration() {
        return dateOfRegistration;
    }

    public void setDateOfRegistration(Date dateOfRegistration) {
        this.dateOfRegistration = dateOfRegistration;
    }

    public String getMasterUsername() {
        return masterUsername;
    }

    public void setMasterUsername(String masterUsername) {
        this.masterUsername = masterUsername;
    }

    public String getMasterPassword() {
        return masterPassword;
    }

    public void setMasterPassword(String masterPassword) {
        this.masterPassword = masterPassword;
    }

    public String getSystemStatus() {
        return systemStatus;
    }

    public void setSystemStatus(String systemStatus) {
        this.systemStatus = systemStatus;
    }

    public Integer getGradingSystem() {
        return gradingSystem;
    }

    public void setGradingSystem(Integer gradingSystem) {
        this.gradingSystem = gradingSystem;
    }

    public String getSchSendingId() {
        return schSendingId;
    }

    public void setSchSendingId(String schSendingId) {
        this.schSendingId = schSendingId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (schoolNumber != null ? schoolNumber.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EducationalInstitution)) {
            return false;
        }
        EducationalInstitution other = (EducationalInstitution) object;
        if ((this.schoolNumber == null && other.schoolNumber != null) || (this.schoolNumber != null && !this.schoolNumber.equals(other.schoolNumber))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sabonay.timetable.entities.EducationalInstitution[ schoolNumber=" + schoolNumber + " ]";
    }
    
}
