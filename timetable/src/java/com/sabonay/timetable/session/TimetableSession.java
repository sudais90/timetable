/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sabonay.timetable.session;

import com.sabonay.timetable.entities.AcademicTerm;
import com.sabonay.timetable.entities.AcademicYear;
import com.sabonay.timetable.entities.Period;
import com.sabonay.timetable.entities.SchoolClass;
import com.sabonay.timetable.entities.SchoolStaff;
import com.sabonay.timetable.entities.SchoolSubject;
import com.sabonay.timetable.entities.Setting;
import com.sabonay.timetable.entities.TeacherSubjectClass;
import com.sabonay.timetable.entities.TeachingSubAndClasses;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Amina
 */
@Stateless
public class TimetableSession {

    @PersistenceContext(unitName = "timetablePU")
    private EntityManager em;

    public boolean persist(Object object) {
        try {
            em.persist(object);
            em.flush();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean deleteObjects(Object o) {
        try {
            em.remove(em.merge(o));
            em.flush();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public List<SchoolSubject> schoolSubjectGetAll() {
        try {
            return (List<SchoolSubject>) em.createNamedQuery("SchoolSubject.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<AcademicTerm> academicTermGetAll() {
        try {
            return (List<AcademicTerm>) em.createNamedQuery("AcademicTerm.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<AcademicYear> academicYearGetAll() {
        try {
            return (List<AcademicYear>) em.createNamedQuery("AcademicYear.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<SchoolClass> schoolClassGetAll() {
        try {
            return (List<SchoolClass>) em.createNamedQuery("SchoolClass.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public SchoolClass findBySchoolClass(String id) {
        return em.find(SchoolClass.class, id);
    }

    public SchoolSubject findBySchoolSubject(String id) {
        return em.find(SchoolSubject.class, id);
    }

    public List<SchoolStaff> schoolStaffGetAll() {
        try {
            return (List<SchoolStaff>) em.createNamedQuery("SchoolStaff.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<TeacherSubjectClass> teacherSubjectTeacherGetAll() {
        try {
            return (List<TeacherSubjectClass>) em.createNamedQuery("TeacherSubjectClass.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<TeachingSubAndClasses> teacherSubjectClassCombinationGetAll(String term) {
        try {
            return (List<TeachingSubAndClasses>) em.createNamedQuery("TeachingSubAndClasses.findByAcademicTerm").setParameter("academicTerm", term).getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public SchoolSubject findBySubjectCode(String code) {
        try {
            return (SchoolSubject) em.createNamedQuery("SchoolSubject.findBySubjectCode").setParameter("subjectCode", code).getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public SchoolStaff findByTeacherId(String id) {
        try {
            return (SchoolStaff) em.createNamedQuery("SchoolStaff.findByStaffId").setParameter("staffId", id).getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Setting findCurrentTerm(String term) {
        try {
            return (Setting) em.createNamedQuery("Setting.findBySettingsKey").setParameter("settingsKey", term).getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Period> periodsGetAll() {
        try {
            return (List<Period>) em.createNamedQuery("Period.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<TeacherSubjectClass> getTeacherSubjectByClass(String className) {
        try {
            return (List<TeacherSubjectClass>) em.createNamedQuery("TeacherSubjectClass.findBySchoolClassId").setParameter("schoolClassId", className).getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
