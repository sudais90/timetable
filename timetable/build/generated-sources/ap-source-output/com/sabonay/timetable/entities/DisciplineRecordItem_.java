package com.sabonay.timetable.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-02-27T15:02:50")
@StaticMetamodel(DisciplineRecordItem.class)
public class DisciplineRecordItem_ { 

    public static volatile SingularAttribute<DisciplineRecordItem, String> recordItemDescription;
    public static volatile SingularAttribute<DisciplineRecordItem, String> updated;
    public static volatile SingularAttribute<DisciplineRecordItem, String> schoolNumber;
    public static volatile SingularAttribute<DisciplineRecordItem, String> recordItemName;
    public static volatile SingularAttribute<DisciplineRecordItem, String> deleted;
    public static volatile SingularAttribute<DisciplineRecordItem, String> disciplineRecordItemId;

}