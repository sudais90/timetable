package com.sabonay.timetable.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-02-27T15:02:50")
@StaticMetamodel(AcademicTermActivity.class)
public class AcademicTermActivity_ { 

    public static volatile SingularAttribute<AcademicTermActivity, String> termActivitiesId;
    public static volatile SingularAttribute<AcademicTermActivity, String> activityNotes;
    public static volatile SingularAttribute<AcademicTermActivity, String> updated;
    public static volatile SingularAttribute<AcademicTermActivity, String> schoolNumber;
    public static volatile SingularAttribute<AcademicTermActivity, Date> activityEndDate;
    public static volatile SingularAttribute<AcademicTermActivity, String> activityName;
    public static volatile SingularAttribute<AcademicTermActivity, String> deleted;
    public static volatile SingularAttribute<AcademicTermActivity, String> academicTerm;
    public static volatile SingularAttribute<AcademicTermActivity, Date> activityStartDate;

}