package com.sabonay.timetable.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-02-27T15:02:50")
@StaticMetamodel(SchoolClassSchoolSubject.class)
public class SchoolClassSchoolSubject_ { 

    public static volatile SingularAttribute<SchoolClassSchoolSubject, String> schoolClassSchoolSubjectId;
    public static volatile SingularAttribute<SchoolClassSchoolSubject, String> updated;
    public static volatile SingularAttribute<SchoolClassSchoolSubject, String> schoolSubjectId;
    public static volatile SingularAttribute<SchoolClassSchoolSubject, String> schoolClassId;
    public static volatile SingularAttribute<SchoolClassSchoolSubject, String> deleted;

}