package com.sabonay.timetable.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-02-27T15:02:50")
@StaticMetamodel(StudentTermReportNote.class)
public class StudentTermReportNote_ { 

    public static volatile SingularAttribute<StudentTermReportNote, String> promotionStatus;
    public static volatile SingularAttribute<StudentTermReportNote, String> student;
    public static volatile SingularAttribute<StudentTermReportNote, String> updated;
    public static volatile SingularAttribute<StudentTermReportNote, String> schoolNumber;
    public static volatile SingularAttribute<StudentTermReportNote, String> lastModifiedBy;
    public static volatile SingularAttribute<StudentTermReportNote, Date> lastModifiedDate;
    public static volatile SingularAttribute<StudentTermReportNote, String> classPromotedTo;
    public static volatile SingularAttribute<StudentTermReportNote, String> deleted;
    public static volatile SingularAttribute<StudentTermReportNote, String> studentTermReportNoteId;
    public static volatile SingularAttribute<StudentTermReportNote, String> academicTerm;

}