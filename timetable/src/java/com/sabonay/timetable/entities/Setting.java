/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sabonay.timetable.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Amina
 */
@Entity
@Table(name = "setting")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Setting.findAll", query = "SELECT s FROM Setting s"),
    @NamedQuery(name = "Setting.findBySettingId", query = "SELECT s FROM Setting s WHERE s.settingId = :settingId"),
    @NamedQuery(name = "Setting.findBySettingsKey", query = "SELECT s FROM Setting s WHERE s.settingsKey = :settingsKey"),
    @NamedQuery(name = "Setting.findBySchoolNumber", query = "SELECT s FROM Setting s WHERE s.schoolNumber = :schoolNumber"),
    @NamedQuery(name = "Setting.findByDeleted", query = "SELECT s FROM Setting s WHERE s.deleted = :deleted"),
    @NamedQuery(name = "Setting.findByUpdated", query = "SELECT s FROM Setting s WHERE s.updated = :updated")})
public class Setting implements Serializable {
    private static final long serialVersionUID = 1L;
    @Size(max = 200)
    @Column(name = "setting_id")
    private String settingId;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "settings_key")
    private String settingsKey;
    @Lob
    @Column(name = "settings_value")
    private byte[] settingsValue;
    @Size(max = 100)
    @Column(name = "school_number")
    private String schoolNumber;
    @Size(max = 5)
    @Column(name = "deleted")
    private String deleted;
    @Size(max = 5)
    @Column(name = "updated")
    private String updated;

    public Setting() {
    }

    public Setting(String settingsKey) {
        this.settingsKey = settingsKey;
    }

    public String getSettingId() {
        return settingId;
    }

    public void setSettingId(String settingId) {
        this.settingId = settingId;
    }

    public String getSettingsKey() {
        return settingsKey;
    }

    public void setSettingsKey(String settingsKey) {
        this.settingsKey = settingsKey;
    }

    public byte[] getSettingsValue() {
        return settingsValue;
    }

    public void setSettingsValue(byte[] settingsValue) {
        this.settingsValue = settingsValue;
    }

    public String getSchoolNumber() {
        return schoolNumber;
    }

    public void setSchoolNumber(String schoolNumber) {
        this.schoolNumber = schoolNumber;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (settingsKey != null ? settingsKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Setting)) {
            return false;
        }
        Setting other = (Setting) object;
        if ((this.settingsKey == null && other.settingsKey != null) || (this.settingsKey != null && !this.settingsKey.equals(other.settingsKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sabonay.timetable.entities.Setting[ settingsKey=" + settingsKey + " ]";
    }
    
}
