package com.sabonay.timetable.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-02-27T15:02:50")
@StaticMetamodel(StudentMark.class)
public class StudentMark_ { 

    public static volatile SingularAttribute<StudentMark, String> student;
    public static volatile SingularAttribute<StudentMark, String> updated;
    public static volatile SingularAttribute<StudentMark, String> subject;
    public static volatile SingularAttribute<StudentMark, String> schoolNumber;
    public static volatile SingularAttribute<StudentMark, String> marksId;
    public static volatile SingularAttribute<StudentMark, Double> examMark;
    public static volatile SingularAttribute<StudentMark, Double> classMark;
    public static volatile SingularAttribute<StudentMark, String> studentClass;
    public static volatile SingularAttribute<StudentMark, String> deleted;
    public static volatile SingularAttribute<StudentMark, String> academicTerm;

}