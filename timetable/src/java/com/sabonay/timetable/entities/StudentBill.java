/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sabonay.timetable.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Amina
 */
@Entity
@Table(name = "student_bill")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "StudentBill.findAll", query = "SELECT s FROM StudentBill s"),
    @NamedQuery(name = "StudentBill.findByStudentBillId", query = "SELECT s FROM StudentBill s WHERE s.studentBillId = :studentBillId"),
    @NamedQuery(name = "StudentBill.findByStudentBillType", query = "SELECT s FROM StudentBill s WHERE s.studentBillType = :studentBillType"),
    @NamedQuery(name = "StudentBill.findByBillItem", query = "SELECT s FROM StudentBill s WHERE s.billItem = :billItem"),
    @NamedQuery(name = "StudentBill.findByDayStudentAmt", query = "SELECT s FROM StudentBill s WHERE s.dayStudentAmt = :dayStudentAmt"),
    @NamedQuery(name = "StudentBill.findByBoardingStudentAmt", query = "SELECT s FROM StudentBill s WHERE s.boardingStudentAmt = :boardingStudentAmt"),
    @NamedQuery(name = "StudentBill.findByAcademicTerm", query = "SELECT s FROM StudentBill s WHERE s.academicTerm = :academicTerm"),
    @NamedQuery(name = "StudentBill.findBySchoolProgram", query = "SELECT s FROM StudentBill s WHERE s.schoolProgram = :schoolProgram"),
    @NamedQuery(name = "StudentBill.findByEducationalLevel", query = "SELECT s FROM StudentBill s WHERE s.educationalLevel = :educationalLevel"),
    @NamedQuery(name = "StudentBill.findBySchoolNumber", query = "SELECT s FROM StudentBill s WHERE s.schoolNumber = :schoolNumber"),
    @NamedQuery(name = "StudentBill.findByDeleted", query = "SELECT s FROM StudentBill s WHERE s.deleted = :deleted"),
    @NamedQuery(name = "StudentBill.findByUpdated", query = "SELECT s FROM StudentBill s WHERE s.updated = :updated"),
    @NamedQuery(name = "StudentBill.findBySchoolClass", query = "SELECT s FROM StudentBill s WHERE s.schoolClass = :schoolClass"),
    @NamedQuery(name = "StudentBill.findByStudent", query = "SELECT s FROM StudentBill s WHERE s.student = :student"),
    @NamedQuery(name = "StudentBill.findByGender", query = "SELECT s FROM StudentBill s WHERE s.gender = :gender")})
public class StudentBill implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "student_bill_id")
    private String studentBillId;
    @Size(max = 255)
    @Column(name = "student_bill_type")
    private String studentBillType;
    @Size(max = 255)
    @Column(name = "bill_item")
    private String billItem;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "day_student_amt")
    private Double dayStudentAmt;
    @Column(name = "boarding_student_amt")
    private Double boardingStudentAmt;
    @Size(max = 255)
    @Column(name = "academic_term")
    private String academicTerm;
    @Size(max = 255)
    @Column(name = "school_program")
    private String schoolProgram;
    @Size(max = 255)
    @Column(name = "educational_level")
    private String educationalLevel;
    @Size(max = 255)
    @Column(name = "school_number")
    private String schoolNumber;
    @Size(max = 255)
    @Column(name = "deleted")
    private String deleted;
    @Size(max = 255)
    @Column(name = "updated")
    private String updated;
    @Size(max = 50)
    @Column(name = "school_class")
    private String schoolClass;
    @Size(max = 50)
    @Column(name = "student")
    private String student;
    @Size(max = 50)
    @Column(name = "gender")
    private String gender;

    public StudentBill() {
    }

    public StudentBill(String studentBillId) {
        this.studentBillId = studentBillId;
    }

    public String getStudentBillId() {
        return studentBillId;
    }

    public void setStudentBillId(String studentBillId) {
        this.studentBillId = studentBillId;
    }

    public String getStudentBillType() {
        return studentBillType;
    }

    public void setStudentBillType(String studentBillType) {
        this.studentBillType = studentBillType;
    }

    public String getBillItem() {
        return billItem;
    }

    public void setBillItem(String billItem) {
        this.billItem = billItem;
    }

    public Double getDayStudentAmt() {
        return dayStudentAmt;
    }

    public void setDayStudentAmt(Double dayStudentAmt) {
        this.dayStudentAmt = dayStudentAmt;
    }

    public Double getBoardingStudentAmt() {
        return boardingStudentAmt;
    }

    public void setBoardingStudentAmt(Double boardingStudentAmt) {
        this.boardingStudentAmt = boardingStudentAmt;
    }

    public String getAcademicTerm() {
        return academicTerm;
    }

    public void setAcademicTerm(String academicTerm) {
        this.academicTerm = academicTerm;
    }

    public String getSchoolProgram() {
        return schoolProgram;
    }

    public void setSchoolProgram(String schoolProgram) {
        this.schoolProgram = schoolProgram;
    }

    public String getEducationalLevel() {
        return educationalLevel;
    }

    public void setEducationalLevel(String educationalLevel) {
        this.educationalLevel = educationalLevel;
    }

    public String getSchoolNumber() {
        return schoolNumber;
    }

    public void setSchoolNumber(String schoolNumber) {
        this.schoolNumber = schoolNumber;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getSchoolClass() {
        return schoolClass;
    }

    public void setSchoolClass(String schoolClass) {
        this.schoolClass = schoolClass;
    }

    public String getStudent() {
        return student;
    }

    public void setStudent(String student) {
        this.student = student;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (studentBillId != null ? studentBillId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StudentBill)) {
            return false;
        }
        StudentBill other = (StudentBill) object;
        if ((this.studentBillId == null && other.studentBillId != null) || (this.studentBillId != null && !this.studentBillId.equals(other.studentBillId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sabonay.timetable.entities.StudentBill[ studentBillId=" + studentBillId + " ]";
    }
    
}
