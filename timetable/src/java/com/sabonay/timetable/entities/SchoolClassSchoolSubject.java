/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sabonay.timetable.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Amina
 */
@Entity
@Table(name = "school_class_school_subject")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SchoolClassSchoolSubject.findAll", query = "SELECT s FROM SchoolClassSchoolSubject s"),
    @NamedQuery(name = "SchoolClassSchoolSubject.findBySchoolClassSchoolSubjectId", query = "SELECT s FROM SchoolClassSchoolSubject s WHERE s.schoolClassSchoolSubjectId = :schoolClassSchoolSubjectId"),
    @NamedQuery(name = "SchoolClassSchoolSubject.findBySchoolSubjectId", query = "SELECT s FROM SchoolClassSchoolSubject s WHERE s.schoolSubjectId = :schoolSubjectId"),
    @NamedQuery(name = "SchoolClassSchoolSubject.findBySchoolClassId", query = "SELECT s FROM SchoolClassSchoolSubject s WHERE s.schoolClassId = :schoolClassId"),
    @NamedQuery(name = "SchoolClassSchoolSubject.findByUpdated", query = "SELECT s FROM SchoolClassSchoolSubject s WHERE s.updated = :updated"),
    @NamedQuery(name = "SchoolClassSchoolSubject.findByDeleted", query = "SELECT s FROM SchoolClassSchoolSubject s WHERE s.deleted = :deleted")})
public class SchoolClassSchoolSubject implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "school_class_school_subject_id")
    private String schoolClassSchoolSubjectId;
    @Size(max = 50)
    @Column(name = "school_subject_id")
    private String schoolSubjectId;
    @Size(max = 50)
    @Column(name = "school_class_id")
    private String schoolClassId;
    @Size(max = 10)
    @Column(name = "updated")
    private String updated;
    @Size(max = 10)
    @Column(name = "deleted")
    private String deleted;

    public SchoolClassSchoolSubject() {
    }

    public SchoolClassSchoolSubject(String schoolClassSchoolSubjectId) {
        this.schoolClassSchoolSubjectId = schoolClassSchoolSubjectId;
    }

    public String getSchoolClassSchoolSubjectId() {
        return schoolClassSchoolSubjectId;
    }

    public void setSchoolClassSchoolSubjectId(String schoolClassSchoolSubjectId) {
        this.schoolClassSchoolSubjectId = schoolClassSchoolSubjectId;
    }

    public String getSchoolSubjectId() {
        return schoolSubjectId;
    }

    public void setSchoolSubjectId(String schoolSubjectId) {
        this.schoolSubjectId = schoolSubjectId;
    }

    public String getSchoolClassId() {
        return schoolClassId;
    }

    public void setSchoolClassId(String schoolClassId) {
        this.schoolClassId = schoolClassId;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (schoolClassSchoolSubjectId != null ? schoolClassSchoolSubjectId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SchoolClassSchoolSubject)) {
            return false;
        }
        SchoolClassSchoolSubject other = (SchoolClassSchoolSubject) object;
        if ((this.schoolClassSchoolSubjectId == null && other.schoolClassSchoolSubjectId != null) || (this.schoolClassSchoolSubjectId != null && !this.schoolClassSchoolSubjectId.equals(other.schoolClassSchoolSubjectId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sabonay.timetable.entities.SchoolClassSchoolSubject[ schoolClassSchoolSubjectId=" + schoolClassSchoolSubjectId + " ]";
    }
    
}
