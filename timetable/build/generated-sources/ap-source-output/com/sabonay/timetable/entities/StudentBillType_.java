package com.sabonay.timetable.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-02-27T15:02:50")
@StaticMetamodel(StudentBillType.class)
public class StudentBillType_ { 

    public static volatile SingularAttribute<StudentBillType, String> studentBillTypeId;
    public static volatile SingularAttribute<StudentBillType, String> billTypeDescription;
    public static volatile SingularAttribute<StudentBillType, String> orderBy;
    public static volatile SingularAttribute<StudentBillType, String> updated;
    public static volatile SingularAttribute<StudentBillType, String> schoolNumber;
    public static volatile SingularAttribute<StudentBillType, String> billTypeName;
    public static volatile SingularAttribute<StudentBillType, String> deleted;

}