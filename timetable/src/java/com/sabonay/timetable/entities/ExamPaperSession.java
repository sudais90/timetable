/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sabonay.timetable.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Amina
 */
@Entity
@Table(name = "exam_paper_session")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ExamPaperSession.findAll", query = "SELECT e FROM ExamPaperSession e"),
    @NamedQuery(name = "ExamPaperSession.findByPaperSessionId", query = "SELECT e FROM ExamPaperSession e WHERE e.paperSessionId = :paperSessionId"),
    @NamedQuery(name = "ExamPaperSession.findByStartTime", query = "SELECT e FROM ExamPaperSession e WHERE e.startTime = :startTime"),
    @NamedQuery(name = "ExamPaperSession.findByEndTime", query = "SELECT e FROM ExamPaperSession e WHERE e.endTime = :endTime"),
    @NamedQuery(name = "ExamPaperSession.findByPaperSessionName", query = "SELECT e FROM ExamPaperSession e WHERE e.paperSessionName = :paperSessionName"),
    @NamedQuery(name = "ExamPaperSession.findByDeleted", query = "SELECT e FROM ExamPaperSession e WHERE e.deleted = :deleted"),
    @NamedQuery(name = "ExamPaperSession.findByUpdated", query = "SELECT e FROM ExamPaperSession e WHERE e.updated = :updated")})
public class ExamPaperSession implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "paper_session_id")
    private String paperSessionId;
    @Column(name = "start_time")
    @Temporal(TemporalType.TIME)
    private Date startTime;
    @Column(name = "end_time")
    @Temporal(TemporalType.TIME)
    private Date endTime;
    @Size(max = 100)
    @Column(name = "paper_session_name")
    private String paperSessionName;
    @Size(max = 10)
    @Column(name = "deleted")
    private String deleted;
    @Size(max = 10)
    @Column(name = "updated")
    private String updated;

    public ExamPaperSession() {
    }

    public ExamPaperSession(String paperSessionId) {
        this.paperSessionId = paperSessionId;
    }

    public String getPaperSessionId() {
        return paperSessionId;
    }

    public void setPaperSessionId(String paperSessionId) {
        this.paperSessionId = paperSessionId;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getPaperSessionName() {
        return paperSessionName;
    }

    public void setPaperSessionName(String paperSessionName) {
        this.paperSessionName = paperSessionName;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (paperSessionId != null ? paperSessionId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ExamPaperSession)) {
            return false;
        }
        ExamPaperSession other = (ExamPaperSession) object;
        if ((this.paperSessionId == null && other.paperSessionId != null) || (this.paperSessionId != null && !this.paperSessionId.equals(other.paperSessionId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sabonay.timetable.entities.ExamPaperSession[ paperSessionId=" + paperSessionId + " ]";
    }
    
}
