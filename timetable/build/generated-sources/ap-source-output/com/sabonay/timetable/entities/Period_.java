package com.sabonay.timetable.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-02-27T15:02:50")
@StaticMetamodel(Period.class)
public class Period_ { 

    public static volatile SingularAttribute<Period, Date> startTime;
    public static volatile SingularAttribute<Period, Integer> periodId;
    public static volatile SingularAttribute<Period, Boolean> periodBreak;
    public static volatile SingularAttribute<Period, Short> updated;
    public static volatile SingularAttribute<Period, Date> endTime;
    public static volatile SingularAttribute<Period, Short> deleted;
    public static volatile SingularAttribute<Period, String> periodName;

}