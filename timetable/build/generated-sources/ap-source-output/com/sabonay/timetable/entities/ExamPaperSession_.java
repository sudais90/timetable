package com.sabonay.timetable.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-02-27T15:02:50")
@StaticMetamodel(ExamPaperSession.class)
public class ExamPaperSession_ { 

    public static volatile SingularAttribute<ExamPaperSession, Date> startTime;
    public static volatile SingularAttribute<ExamPaperSession, String> paperSessionId;
    public static volatile SingularAttribute<ExamPaperSession, String> paperSessionName;
    public static volatile SingularAttribute<ExamPaperSession, String> updated;
    public static volatile SingularAttribute<ExamPaperSession, Date> endTime;
    public static volatile SingularAttribute<ExamPaperSession, String> deleted;

}