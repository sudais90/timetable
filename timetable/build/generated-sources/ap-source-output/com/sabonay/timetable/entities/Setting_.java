package com.sabonay.timetable.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-02-27T15:02:50")
@StaticMetamodel(Setting.class)
public class Setting_ { 

    public static volatile SingularAttribute<Setting, String> settingsKey;
    public static volatile SingularAttribute<Setting, String> updated;
    public static volatile SingularAttribute<Setting, String> schoolNumber;
    public static volatile SingularAttribute<Setting, byte[]> settingsValue;
    public static volatile SingularAttribute<Setting, String> settingId;
    public static volatile SingularAttribute<Setting, String> deleted;

}