/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sabonay.timetable.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Amina
 */
@Entity
@Table(name = "student_mark")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "StudentMark.findAll", query = "SELECT s FROM StudentMark s"),
    @NamedQuery(name = "StudentMark.findByMarksId", query = "SELECT s FROM StudentMark s WHERE s.marksId = :marksId"),
    @NamedQuery(name = "StudentMark.findByAcademicTerm", query = "SELECT s FROM StudentMark s WHERE s.academicTerm = :academicTerm"),
    @NamedQuery(name = "StudentMark.findByStudent", query = "SELECT s FROM StudentMark s WHERE s.student = :student"),
    @NamedQuery(name = "StudentMark.findBySubject", query = "SELECT s FROM StudentMark s WHERE s.subject = :subject"),
    @NamedQuery(name = "StudentMark.findByStudentClass", query = "SELECT s FROM StudentMark s WHERE s.studentClass = :studentClass"),
    @NamedQuery(name = "StudentMark.findByClassMark", query = "SELECT s FROM StudentMark s WHERE s.classMark = :classMark"),
    @NamedQuery(name = "StudentMark.findByExamMark", query = "SELECT s FROM StudentMark s WHERE s.examMark = :examMark"),
    @NamedQuery(name = "StudentMark.findBySchoolNumber", query = "SELECT s FROM StudentMark s WHERE s.schoolNumber = :schoolNumber"),
    @NamedQuery(name = "StudentMark.findByDeleted", query = "SELECT s FROM StudentMark s WHERE s.deleted = :deleted"),
    @NamedQuery(name = "StudentMark.findByUpdated", query = "SELECT s FROM StudentMark s WHERE s.updated = :updated")})
public class StudentMark implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "marks_id")
    private String marksId;
    @Size(max = 255)
    @Column(name = "academic_term")
    private String academicTerm;
    @Size(max = 255)
    @Column(name = "student")
    private String student;
    @Size(max = 255)
    @Column(name = "subject")
    private String subject;
    @Size(max = 255)
    @Column(name = "student_class")
    private String studentClass;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "class_mark")
    private Double classMark;
    @Column(name = "exam_mark")
    private Double examMark;
    @Size(max = 100)
    @Column(name = "school_number")
    private String schoolNumber;
    @Size(max = 244)
    @Column(name = "deleted")
    private String deleted;
    @Size(max = 255)
    @Column(name = "updated")
    private String updated;

    public StudentMark() {
    }

    public StudentMark(String marksId) {
        this.marksId = marksId;
    }

    public String getMarksId() {
        return marksId;
    }

    public void setMarksId(String marksId) {
        this.marksId = marksId;
    }

    public String getAcademicTerm() {
        return academicTerm;
    }

    public void setAcademicTerm(String academicTerm) {
        this.academicTerm = academicTerm;
    }

    public String getStudent() {
        return student;
    }

    public void setStudent(String student) {
        this.student = student;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getStudentClass() {
        return studentClass;
    }

    public void setStudentClass(String studentClass) {
        this.studentClass = studentClass;
    }

    public Double getClassMark() {
        return classMark;
    }

    public void setClassMark(Double classMark) {
        this.classMark = classMark;
    }

    public Double getExamMark() {
        return examMark;
    }

    public void setExamMark(Double examMark) {
        this.examMark = examMark;
    }

    public String getSchoolNumber() {
        return schoolNumber;
    }

    public void setSchoolNumber(String schoolNumber) {
        this.schoolNumber = schoolNumber;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (marksId != null ? marksId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StudentMark)) {
            return false;
        }
        StudentMark other = (StudentMark) object;
        if ((this.marksId == null && other.marksId != null) || (this.marksId != null && !this.marksId.equals(other.marksId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sabonay.timetable.entities.StudentMark[ marksId=" + marksId + " ]";
    }
    
}
