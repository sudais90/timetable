/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sabonay.timetable.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Amina
 */
@Entity
@Table(name = "school_subject_class_teacher")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SchoolSubjectClassTeacher.findAll", query = "SELECT s FROM SchoolSubjectClassTeacher s"),
    @NamedQuery(name = "SchoolSubjectClassTeacher.findBySchoolSubjectClassTeacherId", query = "SELECT s FROM SchoolSubjectClassTeacher s WHERE s.schoolSubjectClassTeacherId = :schoolSubjectClassTeacherId"),
    @NamedQuery(name = "SchoolSubjectClassTeacher.findBySchoolSubjectId", query = "SELECT s FROM SchoolSubjectClassTeacher s WHERE s.schoolSubjectId = :schoolSubjectId"),
    @NamedQuery(name = "SchoolSubjectClassTeacher.findByClassTeacher", query = "SELECT s FROM SchoolSubjectClassTeacher s WHERE s.classTeacher = :classTeacher"),
    @NamedQuery(name = "SchoolSubjectClassTeacher.findByUpdated", query = "SELECT s FROM SchoolSubjectClassTeacher s WHERE s.updated = :updated"),
    @NamedQuery(name = "SchoolSubjectClassTeacher.findByDeleted", query = "SELECT s FROM SchoolSubjectClassTeacher s WHERE s.deleted = :deleted")})
public class SchoolSubjectClassTeacher implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "school_subject_class_teacher_id")
    private String schoolSubjectClassTeacherId;
    @Size(max = 50)
    @Column(name = "school_subject_id")
    private String schoolSubjectId;
    @Size(max = 50)
    @Column(name = "class_teacher")
    private String classTeacher;
    @Size(max = 10)
    @Column(name = "updated")
    private String updated;
    @Size(max = 10)
    @Column(name = "deleted")
    private String deleted;

    public SchoolSubjectClassTeacher() {
    }

    public SchoolSubjectClassTeacher(String schoolSubjectClassTeacherId) {
        this.schoolSubjectClassTeacherId = schoolSubjectClassTeacherId;
    }

    public String getSchoolSubjectClassTeacherId() {
        return schoolSubjectClassTeacherId;
    }

    public void setSchoolSubjectClassTeacherId(String schoolSubjectClassTeacherId) {
        this.schoolSubjectClassTeacherId = schoolSubjectClassTeacherId;
    }

    public String getSchoolSubjectId() {
        return schoolSubjectId;
    }

    public void setSchoolSubjectId(String schoolSubjectId) {
        this.schoolSubjectId = schoolSubjectId;
    }

    public String getClassTeacher() {
        return classTeacher;
    }

    public void setClassTeacher(String classTeacher) {
        this.classTeacher = classTeacher;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (schoolSubjectClassTeacherId != null ? schoolSubjectClassTeacherId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SchoolSubjectClassTeacher)) {
            return false;
        }
        SchoolSubjectClassTeacher other = (SchoolSubjectClassTeacher) object;
        if ((this.schoolSubjectClassTeacherId == null && other.schoolSubjectClassTeacherId != null) || (this.schoolSubjectClassTeacherId != null && !this.schoolSubjectClassTeacherId.equals(other.schoolSubjectClassTeacherId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sabonay.timetable.entities.SchoolSubjectClassTeacher[ schoolSubjectClassTeacherId=" + schoolSubjectClassTeacherId + " ]";
    }
    
}
