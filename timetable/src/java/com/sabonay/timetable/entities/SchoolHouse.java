/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sabonay.timetable.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Amina
 */
@Entity
@Table(name = "school_house")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SchoolHouse.findAll", query = "SELECT s FROM SchoolHouse s"),
    @NamedQuery(name = "SchoolHouse.findBySchoolHouseId", query = "SELECT s FROM SchoolHouse s WHERE s.schoolHouseId = :schoolHouseId"),
    @NamedQuery(name = "SchoolHouse.findByHouseName", query = "SELECT s FROM SchoolHouse s WHERE s.houseName = :houseName"),
    @NamedQuery(name = "SchoolHouse.findByInmatesGender", query = "SELECT s FROM SchoolHouse s WHERE s.inmatesGender = :inmatesGender"),
    @NamedQuery(name = "SchoolHouse.findByLastModifiedDate", query = "SELECT s FROM SchoolHouse s WHERE s.lastModifiedDate = :lastModifiedDate"),
    @NamedQuery(name = "SchoolHouse.findByLastModifiedBy", query = "SELECT s FROM SchoolHouse s WHERE s.lastModifiedBy = :lastModifiedBy"),
    @NamedQuery(name = "SchoolHouse.findBySchoolNumber", query = "SELECT s FROM SchoolHouse s WHERE s.schoolNumber = :schoolNumber"),
    @NamedQuery(name = "SchoolHouse.findByDeleted", query = "SELECT s FROM SchoolHouse s WHERE s.deleted = :deleted"),
    @NamedQuery(name = "SchoolHouse.findByUpdated", query = "SELECT s FROM SchoolHouse s WHERE s.updated = :updated"),
    @NamedQuery(name = "SchoolHouse.findByHouseWarder", query = "SELECT s FROM SchoolHouse s WHERE s.houseWarder = :houseWarder"),
    @NamedQuery(name = "SchoolHouse.findByOtherHouseWarders", query = "SELECT s FROM SchoolHouse s WHERE s.otherHouseWarders = :otherHouseWarders")})
public class SchoolHouse implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "school_house_id")
    private String schoolHouseId;
    @Size(max = 255)
    @Column(name = "house_name")
    private String houseName;
    @Size(max = 255)
    @Column(name = "inmates_gender")
    private String inmatesGender;
    @Column(name = "last_modified_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;
    @Size(max = 255)
    @Column(name = "last_modified_by")
    private String lastModifiedBy;
    @Size(max = 244)
    @Column(name = "school_number")
    private String schoolNumber;
    @Size(max = 20)
    @Column(name = "deleted")
    private String deleted;
    @Size(max = 20)
    @Column(name = "updated")
    private String updated;
    @Size(max = 50)
    @Column(name = "house_warder")
    private String houseWarder;
    @Size(max = 255)
    @Column(name = "other_house_warders")
    private String otherHouseWarders;

    public SchoolHouse() {
    }

    public SchoolHouse(String schoolHouseId) {
        this.schoolHouseId = schoolHouseId;
    }

    public String getSchoolHouseId() {
        return schoolHouseId;
    }

    public void setSchoolHouseId(String schoolHouseId) {
        this.schoolHouseId = schoolHouseId;
    }

    public String getHouseName() {
        return houseName;
    }

    public void setHouseName(String houseName) {
        this.houseName = houseName;
    }

    public String getInmatesGender() {
        return inmatesGender;
    }

    public void setInmatesGender(String inmatesGender) {
        this.inmatesGender = inmatesGender;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getSchoolNumber() {
        return schoolNumber;
    }

    public void setSchoolNumber(String schoolNumber) {
        this.schoolNumber = schoolNumber;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getHouseWarder() {
        return houseWarder;
    }

    public void setHouseWarder(String houseWarder) {
        this.houseWarder = houseWarder;
    }

    public String getOtherHouseWarders() {
        return otherHouseWarders;
    }

    public void setOtherHouseWarders(String otherHouseWarders) {
        this.otherHouseWarders = otherHouseWarders;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (schoolHouseId != null ? schoolHouseId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SchoolHouse)) {
            return false;
        }
        SchoolHouse other = (SchoolHouse) object;
        if ((this.schoolHouseId == null && other.schoolHouseId != null) || (this.schoolHouseId != null && !this.schoolHouseId.equals(other.schoolHouseId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sabonay.timetable.entities.SchoolHouse[ schoolHouseId=" + schoolHouseId + " ]";
    }
    
}
