package com.sabonay.timetable.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-02-27T15:02:50")
@StaticMetamodel(Scholarship.class)
public class Scholarship_ { 

    public static volatile SingularAttribute<Scholarship, String> sponsorContact;
    public static volatile SingularAttribute<Scholarship, Double> amountInvolve;
    public static volatile SingularAttribute<Scholarship, String> scholarshipId;
    public static volatile SingularAttribute<Scholarship, String> sponsorContactPerson;
    public static volatile SingularAttribute<Scholarship, String> updated;
    public static volatile SingularAttribute<Scholarship, String> scholarship;
    public static volatile SingularAttribute<Scholarship, String> lastModifiedBy;
    public static volatile SingularAttribute<Scholarship, Date> lastModifiedDate;
    public static volatile SingularAttribute<Scholarship, String> sponsor;
    public static volatile SingularAttribute<Scholarship, String> scholarshipStatus;
    public static volatile SingularAttribute<Scholarship, String> deleted;

}