/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sabonay.timetable.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Amina
 */
@Entity
@Table(name = "discipline_record")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DisciplineRecord.findAll", query = "SELECT d FROM DisciplineRecord d"),
    @NamedQuery(name = "DisciplineRecord.findByDisciplineRecordId", query = "SELECT d FROM DisciplineRecord d WHERE d.disciplineRecordId = :disciplineRecordId"),
    @NamedQuery(name = "DisciplineRecord.findByDisciplineRecordItem", query = "SELECT d FROM DisciplineRecord d WHERE d.disciplineRecordItem = :disciplineRecordItem"),
    @NamedQuery(name = "DisciplineRecord.findByStudent", query = "SELECT d FROM DisciplineRecord d WHERE d.student = :student"),
    @NamedQuery(name = "DisciplineRecord.findByAcademicTerm", query = "SELECT d FROM DisciplineRecord d WHERE d.academicTerm = :academicTerm"),
    @NamedQuery(name = "DisciplineRecord.findByStudentClass", query = "SELECT d FROM DisciplineRecord d WHERE d.studentClass = :studentClass"),
    @NamedQuery(name = "DisciplineRecord.findBySchoolNumber", query = "SELECT d FROM DisciplineRecord d WHERE d.schoolNumber = :schoolNumber"),
    @NamedQuery(name = "DisciplineRecord.findByDateOfOccurence", query = "SELECT d FROM DisciplineRecord d WHERE d.dateOfOccurence = :dateOfOccurence"),
    @NamedQuery(name = "DisciplineRecord.findByDeleted", query = "SELECT d FROM DisciplineRecord d WHERE d.deleted = :deleted"),
    @NamedQuery(name = "DisciplineRecord.findByUpdated", query = "SELECT d FROM DisciplineRecord d WHERE d.updated = :updated")})
public class DisciplineRecord implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "discipline_record_id")
    private String disciplineRecordId;
    @Size(max = 255)
    @Column(name = "discipline_record_item")
    private String disciplineRecordItem;
    @Size(max = 255)
    @Column(name = "student")
    private String student;
    @Lob
    @Size(max = 65535)
    @Column(name = "record_details")
    private String recordDetails;
    @Lob
    @Size(max = 65535)
    @Column(name = "comment")
    private String comment;
    @Size(max = 255)
    @Column(name = "academic_term")
    private String academicTerm;
    @Size(max = 255)
    @Column(name = "student_class")
    private String studentClass;
    @Size(max = 255)
    @Column(name = "school_number")
    private String schoolNumber;
    @Column(name = "date_of_occurence")
    @Temporal(TemporalType.DATE)
    private Date dateOfOccurence;
    @Size(max = 255)
    @Column(name = "deleted")
    private String deleted;
    @Size(max = 255)
    @Column(name = "updated")
    private String updated;

    public DisciplineRecord() {
    }

    public DisciplineRecord(String disciplineRecordId) {
        this.disciplineRecordId = disciplineRecordId;
    }

    public String getDisciplineRecordId() {
        return disciplineRecordId;
    }

    public void setDisciplineRecordId(String disciplineRecordId) {
        this.disciplineRecordId = disciplineRecordId;
    }

    public String getDisciplineRecordItem() {
        return disciplineRecordItem;
    }

    public void setDisciplineRecordItem(String disciplineRecordItem) {
        this.disciplineRecordItem = disciplineRecordItem;
    }

    public String getStudent() {
        return student;
    }

    public void setStudent(String student) {
        this.student = student;
    }

    public String getRecordDetails() {
        return recordDetails;
    }

    public void setRecordDetails(String recordDetails) {
        this.recordDetails = recordDetails;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getAcademicTerm() {
        return academicTerm;
    }

    public void setAcademicTerm(String academicTerm) {
        this.academicTerm = academicTerm;
    }

    public String getStudentClass() {
        return studentClass;
    }

    public void setStudentClass(String studentClass) {
        this.studentClass = studentClass;
    }

    public String getSchoolNumber() {
        return schoolNumber;
    }

    public void setSchoolNumber(String schoolNumber) {
        this.schoolNumber = schoolNumber;
    }

    public Date getDateOfOccurence() {
        return dateOfOccurence;
    }

    public void setDateOfOccurence(Date dateOfOccurence) {
        this.dateOfOccurence = dateOfOccurence;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (disciplineRecordId != null ? disciplineRecordId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DisciplineRecord)) {
            return false;
        }
        DisciplineRecord other = (DisciplineRecord) object;
        if ((this.disciplineRecordId == null && other.disciplineRecordId != null) || (this.disciplineRecordId != null && !this.disciplineRecordId.equals(other.disciplineRecordId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sabonay.timetable.entities.DisciplineRecord[ disciplineRecordId=" + disciplineRecordId + " ]";
    }
    
}
