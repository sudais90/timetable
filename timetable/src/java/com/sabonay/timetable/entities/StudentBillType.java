/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sabonay.timetable.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Amina
 */
@Entity
@Table(name = "student_bill_type")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "StudentBillType.findAll", query = "SELECT s FROM StudentBillType s"),
    @NamedQuery(name = "StudentBillType.findByStudentBillTypeId", query = "SELECT s FROM StudentBillType s WHERE s.studentBillTypeId = :studentBillTypeId"),
    @NamedQuery(name = "StudentBillType.findByBillTypeName", query = "SELECT s FROM StudentBillType s WHERE s.billTypeName = :billTypeName"),
    @NamedQuery(name = "StudentBillType.findByBillTypeDescription", query = "SELECT s FROM StudentBillType s WHERE s.billTypeDescription = :billTypeDescription"),
    @NamedQuery(name = "StudentBillType.findBySchoolNumber", query = "SELECT s FROM StudentBillType s WHERE s.schoolNumber = :schoolNumber"),
    @NamedQuery(name = "StudentBillType.findByDeleted", query = "SELECT s FROM StudentBillType s WHERE s.deleted = :deleted"),
    @NamedQuery(name = "StudentBillType.findByUpdated", query = "SELECT s FROM StudentBillType s WHERE s.updated = :updated"),
    @NamedQuery(name = "StudentBillType.findByOrderBy", query = "SELECT s FROM StudentBillType s WHERE s.orderBy = :orderBy")})
public class StudentBillType implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "student_bill_type_id")
    private String studentBillTypeId;
    @Size(max = 255)
    @Column(name = "bill_type_name")
    private String billTypeName;
    @Size(max = 255)
    @Column(name = "bill_type_description")
    private String billTypeDescription;
    @Size(max = 50)
    @Column(name = "school_number")
    private String schoolNumber;
    @Size(max = 255)
    @Column(name = "deleted")
    private String deleted;
    @Size(max = 255)
    @Column(name = "updated")
    private String updated;
    @Size(max = 255)
    @Column(name = "order_by")
    private String orderBy;

    public StudentBillType() {
    }

    public StudentBillType(String studentBillTypeId) {
        this.studentBillTypeId = studentBillTypeId;
    }

    public String getStudentBillTypeId() {
        return studentBillTypeId;
    }

    public void setStudentBillTypeId(String studentBillTypeId) {
        this.studentBillTypeId = studentBillTypeId;
    }

    public String getBillTypeName() {
        return billTypeName;
    }

    public void setBillTypeName(String billTypeName) {
        this.billTypeName = billTypeName;
    }

    public String getBillTypeDescription() {
        return billTypeDescription;
    }

    public void setBillTypeDescription(String billTypeDescription) {
        this.billTypeDescription = billTypeDescription;
    }

    public String getSchoolNumber() {
        return schoolNumber;
    }

    public void setSchoolNumber(String schoolNumber) {
        this.schoolNumber = schoolNumber;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (studentBillTypeId != null ? studentBillTypeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StudentBillType)) {
            return false;
        }
        StudentBillType other = (StudentBillType) object;
        if ((this.studentBillTypeId == null && other.studentBillTypeId != null) || (this.studentBillTypeId != null && !this.studentBillTypeId.equals(other.studentBillTypeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sabonay.timetable.entities.StudentBillType[ studentBillTypeId=" + studentBillTypeId + " ]";
    }
    
}
