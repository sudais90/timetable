package com.sabonay.timetable.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-02-27T15:02:50")
@StaticMetamodel(StudentLedger.class)
public class StudentLedger_ { 

    public static volatile SingularAttribute<StudentLedger, String> mediumOfPayment;
    public static volatile SingularAttribute<StudentLedger, String> billSettledBy;
    public static volatile SingularAttribute<StudentLedger, String> mediumOfPaymentNumber;
    public static volatile SingularAttribute<StudentLedger, String> studentBillType;
    public static volatile SingularAttribute<StudentLedger, String> deleted;
    public static volatile SingularAttribute<StudentLedger, String> studentLedgerId;
    public static volatile SingularAttribute<StudentLedger, Date> dateOfPayment;
    public static volatile SingularAttribute<StudentLedger, String> typeOfEntry;
    public static volatile SingularAttribute<StudentLedger, Double> amountInvolved;
    public static volatile SingularAttribute<StudentLedger, String> updated;
    public static volatile SingularAttribute<StudentLedger, String> studentId;
    public static volatile SingularAttribute<StudentLedger, Date> dateOfEntry;
    public static volatile SingularAttribute<StudentLedger, String> schoolNumber;
    public static volatile SingularAttribute<StudentLedger, String> studentBillTypeInitials;
    public static volatile SingularAttribute<StudentLedger, String> recordedBy;
    public static volatile SingularAttribute<StudentLedger, String> termOfEntry;
    public static volatile SingularAttribute<StudentLedger, String> receiptNumber;

}