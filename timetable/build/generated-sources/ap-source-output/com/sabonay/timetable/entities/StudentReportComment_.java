package com.sabonay.timetable.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-02-27T15:02:50")
@StaticMetamodel(StudentReportComment.class)
public class StudentReportComment_ { 

    public static volatile SingularAttribute<StudentReportComment, String> studentAttendance;
    public static volatile SingularAttribute<StudentReportComment, String> expectedAttendance;
    public static volatile SingularAttribute<StudentReportComment, String> student;
    public static volatile SingularAttribute<StudentReportComment, String> updated;
    public static volatile SingularAttribute<StudentReportComment, String> schoolNumber;
    public static volatile SingularAttribute<StudentReportComment, String> studentReportCommentId;
    public static volatile SingularAttribute<StudentReportComment, String> lastModifiedBy;
    public static volatile SingularAttribute<StudentReportComment, Date> lastModifiedDate;
    public static volatile SingularAttribute<StudentReportComment, String> comment;
    public static volatile SingularAttribute<StudentReportComment, String> type;
    public static volatile SingularAttribute<StudentReportComment, String> deleted;
    public static volatile SingularAttribute<StudentReportComment, String> academicTerm;

}