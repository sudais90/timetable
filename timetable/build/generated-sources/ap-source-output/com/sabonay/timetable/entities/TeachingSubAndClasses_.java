package com.sabonay.timetable.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-02-27T15:02:50")
@StaticMetamodel(TeachingSubAndClasses.class)
public class TeachingSubAndClasses_ { 

    public static volatile SingularAttribute<TeachingSubAndClasses, String> teachingClasses;
    public static volatile SingularAttribute<TeachingSubAndClasses, String> updated;
    public static volatile SingularAttribute<TeachingSubAndClasses, String> subject;
    public static volatile SingularAttribute<TeachingSubAndClasses, String> schoolNumber;
    public static volatile SingularAttribute<TeachingSubAndClasses, String> teachingSubAndClassesId;
    public static volatile SingularAttribute<TeachingSubAndClasses, String> deleted;
    public static volatile SingularAttribute<TeachingSubAndClasses, String> teacherId;
    public static volatile SingularAttribute<TeachingSubAndClasses, String> academicTerm;

}