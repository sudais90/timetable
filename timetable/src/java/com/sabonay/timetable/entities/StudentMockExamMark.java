/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sabonay.timetable.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Amina
 */
@Entity
@Table(name = "student_mock_exam_mark")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "StudentMockExamMark.findAll", query = "SELECT s FROM StudentMockExamMark s"),
    @NamedQuery(name = "StudentMockExamMark.findByMockExamMarkId", query = "SELECT s FROM StudentMockExamMark s WHERE s.mockExamMarkId = :mockExamMarkId"),
    @NamedQuery(name = "StudentMockExamMark.findByAcademicYear", query = "SELECT s FROM StudentMockExamMark s WHERE s.academicYear = :academicYear"),
    @NamedQuery(name = "StudentMockExamMark.findByStudent", query = "SELECT s FROM StudentMockExamMark s WHERE s.student = :student"),
    @NamedQuery(name = "StudentMockExamMark.findBySubject", query = "SELECT s FROM StudentMockExamMark s WHERE s.subject = :subject"),
    @NamedQuery(name = "StudentMockExamMark.findByStudentClass", query = "SELECT s FROM StudentMockExamMark s WHERE s.studentClass = :studentClass"),
    @NamedQuery(name = "StudentMockExamMark.findByMockMark", query = "SELECT s FROM StudentMockExamMark s WHERE s.mockMark = :mockMark"),
    @NamedQuery(name = "StudentMockExamMark.findBySchoolNumber", query = "SELECT s FROM StudentMockExamMark s WHERE s.schoolNumber = :schoolNumber"),
    @NamedQuery(name = "StudentMockExamMark.findByDeleted", query = "SELECT s FROM StudentMockExamMark s WHERE s.deleted = :deleted"),
    @NamedQuery(name = "StudentMockExamMark.findByUpdated", query = "SELECT s FROM StudentMockExamMark s WHERE s.updated = :updated")})
public class StudentMockExamMark implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "mock_exam_mark_id")
    private String mockExamMarkId;
    @Size(max = 255)
    @Column(name = "academic_year")
    private String academicYear;
    @Size(max = 255)
    @Column(name = "student")
    private String student;
    @Size(max = 255)
    @Column(name = "subject")
    private String subject;
    @Size(max = 255)
    @Column(name = "student_class")
    private String studentClass;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "mock_mark")
    private Double mockMark;
    @Size(max = 20)
    @Column(name = "school_number")
    private String schoolNumber;
    @Size(max = 244)
    @Column(name = "deleted")
    private String deleted;
    @Size(max = 255)
    @Column(name = "updated")
    private String updated;

    public StudentMockExamMark() {
    }

    public StudentMockExamMark(String mockExamMarkId) {
        this.mockExamMarkId = mockExamMarkId;
    }

    public String getMockExamMarkId() {
        return mockExamMarkId;
    }

    public void setMockExamMarkId(String mockExamMarkId) {
        this.mockExamMarkId = mockExamMarkId;
    }

    public String getAcademicYear() {
        return academicYear;
    }

    public void setAcademicYear(String academicYear) {
        this.academicYear = academicYear;
    }

    public String getStudent() {
        return student;
    }

    public void setStudent(String student) {
        this.student = student;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getStudentClass() {
        return studentClass;
    }

    public void setStudentClass(String studentClass) {
        this.studentClass = studentClass;
    }

    public Double getMockMark() {
        return mockMark;
    }

    public void setMockMark(Double mockMark) {
        this.mockMark = mockMark;
    }

    public String getSchoolNumber() {
        return schoolNumber;
    }

    public void setSchoolNumber(String schoolNumber) {
        this.schoolNumber = schoolNumber;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (mockExamMarkId != null ? mockExamMarkId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StudentMockExamMark)) {
            return false;
        }
        StudentMockExamMark other = (StudentMockExamMark) object;
        if ((this.mockExamMarkId == null && other.mockExamMarkId != null) || (this.mockExamMarkId != null && !this.mockExamMarkId.equals(other.mockExamMarkId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sabonay.timetable.entities.StudentMockExamMark[ mockExamMarkId=" + mockExamMarkId + " ]";
    }
    
}
