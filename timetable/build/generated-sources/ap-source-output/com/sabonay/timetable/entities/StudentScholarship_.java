package com.sabonay.timetable.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-02-27T15:02:50")
@StaticMetamodel(StudentScholarship.class)
public class StudentScholarship_ { 

    public static volatile SingularAttribute<StudentScholarship, Date> dateSigned;
    public static volatile SingularAttribute<StudentScholarship, String> student;
    public static volatile SingularAttribute<StudentScholarship, String> studentSigned;
    public static volatile SingularAttribute<StudentScholarship, String> updated;
    public static volatile SingularAttribute<StudentScholarship, String> scholarship;
    public static volatile SingularAttribute<StudentScholarship, String> scholarStatus;
    public static volatile SingularAttribute<StudentScholarship, String> lastModifiedBy;
    public static volatile SingularAttribute<StudentScholarship, Date> lastModifiedDate;
    public static volatile SingularAttribute<StudentScholarship, String> studentScholarshipId;
    public static volatile SingularAttribute<StudentScholarship, String> deleted;
    public static volatile SingularAttribute<StudentScholarship, String> academicYear;

}