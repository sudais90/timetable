package com.sabonay.timetable.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-02-27T15:02:50")
@StaticMetamodel(ExamPaper.class)
public class ExamPaper_ { 

    public static volatile SingularAttribute<ExamPaper, String> updated;
    public static volatile SingularAttribute<ExamPaper, String> schoolNumber;
    public static volatile SingularAttribute<ExamPaper, String> examRoom;
    public static volatile SingularAttribute<ExamPaper, String> paperSession;
    public static volatile SingularAttribute<ExamPaper, String> subjectCode;
    public static volatile SingularAttribute<ExamPaper, String> examBlock;
    public static volatile SingularAttribute<ExamPaper, String> deleted;
    public static volatile SingularAttribute<ExamPaper, Date> paperDate;
    public static volatile SingularAttribute<ExamPaper, String> academicTermId;
    public static volatile SingularAttribute<ExamPaper, String> invigilator;
    public static volatile SingularAttribute<ExamPaper, String> examPaperId;

}