/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sabonay.timetable.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Amina
 */
@Entity
@Table(name = "period")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Period.findAll", query = "SELECT p FROM Period p"),
    @NamedQuery(name = "Period.findByPeriodId", query = "SELECT p FROM Period p WHERE p.periodId = :periodId"),
    @NamedQuery(name = "Period.findByPeriodName", query = "SELECT p FROM Period p WHERE p.periodName = :periodName"),
    @NamedQuery(name = "Period.findByStartTime", query = "SELECT p FROM Period p WHERE p.startTime = :startTime"),
    @NamedQuery(name = "Period.findByEndTime", query = "SELECT p FROM Period p WHERE p.endTime = :endTime"),
    @NamedQuery(name = "Period.findByDeleted", query = "SELECT p FROM Period p WHERE p.deleted = :deleted"),
    @NamedQuery(name = "Period.findByUpdated", query = "SELECT p FROM Period p WHERE p.updated = :updated")})
public class Period implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "period_id")
    private Integer periodId;
    @Size(max = 50)
    @Column(name = "period_name")
    private String periodName;
    @Column(name = "start_time")
    @Temporal(TemporalType.TIME)
    private Date startTime;
    @Column(name = "end_time")
    @Temporal(TemporalType.TIME)
    private Date endTime;
    @Column(name = "deleted")
    private Short deleted;
    @Column(name = "updated")
    private Short updated;

    @Column(name = "is_break")
    private boolean periodBreak = false;

    public Period() {
    }

    public Period(Integer periodId) {
        this.periodId = periodId;
    }

    public Integer getPeriodId() {
        return periodId;
    }

    public void setPeriodId(Integer periodId) {
        this.periodId = periodId;
    }

    public String getPeriodName() {
        return periodName;
    }

    public void setPeriodName(String periodName) {
        this.periodName = periodName;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Short getDeleted() {
        return deleted;
    }

    public void setDeleted(Short deleted) {
        this.deleted = deleted;
    }

    public Short getUpdated() {
        return updated;
    }

    public void setUpdated(Short updated) {
        this.updated = updated;
    }

    public boolean isPeriodBreak() {
        return periodBreak;
    }

    public void setPeriodBreak(boolean periodBreak) {
        this.periodBreak = periodBreak;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (periodId != null ? periodId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Period)) {
            return false;
        }
        Period other = (Period) object;
        if ((this.periodId == null && other.periodId != null) || (this.periodId != null && !this.periodId.equals(other.periodId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sabonay.timetable.entities.Period[ periodId=" + periodId + " ]";
    }

}
