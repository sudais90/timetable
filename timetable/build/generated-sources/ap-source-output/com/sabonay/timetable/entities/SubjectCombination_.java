package com.sabonay.timetable.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-02-27T15:02:50")
@StaticMetamodel(SubjectCombination.class)
public class SubjectCombination_ { 

    public static volatile SingularAttribute<SubjectCombination, String> subjectCombinationName;
    public static volatile SingularAttribute<SubjectCombination, String> updated;
    public static volatile SingularAttribute<SubjectCombination, String> subjectCombinationCode;
    public static volatile SingularAttribute<SubjectCombination, String> combinationShortName;
    public static volatile SingularAttribute<SubjectCombination, String> subjectsIds;
    public static volatile SingularAttribute<SubjectCombination, String> schoolNumber;
    public static volatile SingularAttribute<SubjectCombination, String> subjectCombinationProgramme;
    public static volatile SingularAttribute<SubjectCombination, String> deleted;
    public static volatile SingularAttribute<SubjectCombination, String> subjectCombinationStatus;

}