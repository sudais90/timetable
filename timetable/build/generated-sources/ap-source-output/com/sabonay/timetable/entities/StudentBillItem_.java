package com.sabonay.timetable.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-02-27T15:02:50")
@StaticMetamodel(StudentBillItem.class)
public class StudentBillItem_ { 

    public static volatile SingularAttribute<StudentBillItem, String> billItemId;
    public static volatile SingularAttribute<StudentBillItem, String> itemName;
    public static volatile SingularAttribute<StudentBillItem, String> itemDescription;
    public static volatile SingularAttribute<StudentBillItem, String> updated;
    public static volatile SingularAttribute<StudentBillItem, String> schoolNumber;
    public static volatile SingularAttribute<StudentBillItem, String> deleted;

}