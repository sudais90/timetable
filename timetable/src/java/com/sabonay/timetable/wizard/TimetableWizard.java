/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sabonay.timetable.wizard;

import com.sabonay.timetable.classfiles.DayWrapper;
import com.sabonay.timetable.classfiles.TeacherSubjectWrapper;
import com.sabonay.timetable.constant.Day;
import com.sabonay.timetable.constant.MessageResolvable;
import com.sabonay.timetable.entities.AcademicTerm;
import com.sabonay.timetable.entities.AcademicYear;
import com.sabonay.timetable.entities.Period;
import com.sabonay.timetable.entities.SchoolClass;
import com.sabonay.timetable.entities.SchoolStaff;
import com.sabonay.timetable.entities.SchoolSubject;
import com.sabonay.timetable.entities.Setting;
import com.sabonay.timetable.entities.TeacherSubjectClass;
import com.sabonay.timetable.entities.TeachingSubAndClasses;
import com.sabonay.timetable.session.TimetableSession;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeSet;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.SelectItem;
import org.primefaces.context.RequestContext;
import org.primefaces.event.CellEditEvent;
import org.primefaces.event.FlowEvent;
import org.primefaces.event.RowEditEvent;
import org.primefaces.extensions.component.timeline.TimelineUpdater;
import org.primefaces.extensions.event.timeline.TimelineModificationEvent;
import org.primefaces.extensions.model.timeline.TimelineEvent;
import org.primefaces.extensions.model.timeline.TimelineGroup;
import org.primefaces.extensions.model.timeline.TimelineModel;

/**
 *
 * @author Amina
 */
@ManagedBean
@ViewScoped
public class TimetableWizard implements Serializable {

    @EJB
    private TimetableSession timetableSession;

    private Integer count = 0;
    private Map<Integer, Object> periodsCount = new HashMap<>();

    private boolean skip;
    private SchoolClass schoolClass = new SchoolClass();
    private SchoolStaff schoolStaff = new SchoolStaff();
    private Period period = new Period();
    private SchoolSubject subject = new SchoolSubject();
    private List<SchoolClass> schoolClasses = new ArrayList<>();
    private List<SchoolSubject> schoolSubjects = new ArrayList<>();

    private TeacherSubjectClass tsc = new TeacherSubjectClass();
    private List<TeacherSubjectClass> teacherSubjectClasses = new ArrayList<>();
    private List<TeachingSubAndClasses> teacherSubjectClassCombinations = new ArrayList<>();
    private String currentTerm = "";

    private List<Period> periods = new ArrayList<>();
    private boolean periodShow = false;
    private boolean subjectShow = false;
    private boolean classShow = false, editShow = false;
    private boolean teacherDialog = false;
    private String className;
    private String classSelectionName;
    private boolean displayModel;
    private List<Day> days = new ArrayList<>();
    private List<String> selectedDays = new ArrayList<>();

    //timeline model parameters
    private TimelineModel model;
    private TimelineEvent event; // current changed event  
    private List<TimelineEvent> overlappedOrders; // all overlapped orders (events) to the changed order (event)  
    private List<TimelineEvent> ordersToMerge; // selected orders (events) in the dialog which should be merged  

    private SelectItem[] teacherSelection, classSelection;
    private List<TeacherSubjectClass> classListings = new ArrayList<>();

    public TimetableWizard() {

    }

    public void save() {
        FacesMessage msg = new FacesMessage("Successful", "Welcome :" + "");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public List<TeacherSubjectClass> getAllTeacherSubjectClass() {
        try {
            return timetableSession.teacherSubjectTeacherGetAll();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @PostConstruct
    public void init() {
        if (getAllTeacherSubjectClass() != null) {
            for (TeacherSubjectClass t : getAllTeacherSubjectClass()) {
                if (timetableSession.deleteObjects(t)) {
                    System.out.println("Deleted Successfully!!");
                }
            }
            System.out.println("Success!!");
        }
        Setting term = timetableSession.findCurrentTerm("CURRENT_TERM");
        if (term != null) {
            try {
                currentTerm = new String(term.getSettingsValue(), "UTF-8");
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(TimetableWizard.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        teacherSubjectClassCombinations = timetableSession.teacherSubjectClassCombinationGetAll(currentTerm);
        System.out.println("List Size is: " + teacherSubjectClassCombinations.size());
        for (TeachingSubAndClasses tasc : teacherSubjectClassCombinations) {
            String classes = tasc.getTeachingClasses();
            List<String> list = new ArrayList<>(Arrays.asList(classes.split("-")));
            for (String a : list) {
                tsc = new TeacherSubjectClass();
                SchoolStaff ct = timetableSession.findByTeacherId(tasc.getTeacherId());
                SchoolSubject ss = timetableSession.findBySubjectCode(tasc.getSubject());
                if (a != null) {
                    tsc.setSchoolClassId(a);
                    if (ct != null) {
                        tsc.setClassTeacherId(ct.getSurname() + " " + ct.getOthernames());
                    }
                    if (ss != null) {
                        tsc.setSchoolSubjectId(ss.getSubjectName());
                    }
                    tsc.setTeacherSubjectClassId(UUID.randomUUID().toString());
                    tsc.setDeleted("No");
                    tsc.setUpdated("No");
                    tsc.setLessonsPerWeek(1);
                    teacherSubjectClasses.add(tsc);
                }
            }

        }
        saveCombination();
    }

    public void editLessonsByClass() {
        try {
            editShow = true;
            classListings = new ArrayList<>();
            classListings = timetableSession.getTeacherSubjectByClass(classSelectionName);
            System.out.println("Success");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<SchoolSubject> getSubjectsData() {
        try {
            return timetableSession.schoolSubjectGetAll();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<AcademicTerm> getAcademicTerm() {
        try {
            return timetableSession.academicTermGetAll();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<AcademicYear> getAcademicYear() {
        try {
            return timetableSession.academicYearGetAll();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<SchoolClass> getClassesData() {
        try {
            return timetableSession.schoolClassGetAll();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Period> getAllPeriods() {
        try {
            return timetableSession.periodsGetAll();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void showTeacherDialog() {
        teacherDialog = true;
    }

    public void addTeacherClassSubjectCombination() {
        try {
            teacherSubjectClasses.add(tsc);
            tsc = new TeacherSubjectClass();
            teacherDialog = false;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void saveCombination() {
        try {
            for (TeacherSubjectClass tsClass : teacherSubjectClasses) {
                if (timetableSession.persist(tsClass)) {
                    System.out.println("success");
                } else {
                    System.out.println("Error saving Class Combination");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<MessageResolvable> getAllDays() {
        List<MessageResolvable> resolvables = Day.asResolvableList();
        return resolvables;
    }

    public void anotherPeriodsDialog() {
        try {
            periods = new ArrayList<>();
            String myTime = "24:00";
            SimpleDateFormat df = new SimpleDateFormat("HH:mm");
            periodShow = true;
            Date d = df.parse(myTime);
            Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
            cal.setTime(d);
            for (int i = 0; i <= count; i++) {
                Period p = new Period();
                p.setPeriodName(i + "");
                p.setStartTime(cal.getTime());
                cal.add(Calendar.MINUTE, 45);
                String newTime = df.format(cal.getTime());
                Date da = df.parse(newTime);
                cal.setTime(da);
                p.setEndTime(cal.getTime());
                cal.add(Calendar.MINUTE, 5);
                String lastTime = df.format(cal.getTime());
                Date dl = df.parse(lastTime);
                cal.setTime(dl);
                p.setPeriodBreak(false);
                periods.add(p);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Periods editing functionalities
    public void onRowEdit(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Period Edited", ((Period) event.getObject()).getPeriodId() + "");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Edit Cancelled", ((Period) event.getObject()).getPeriodId() + "");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onCellEdit(CellEditEvent event) {
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();

        if (newValue != null && !newValue.equals(oldValue)) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Cell Changed", "Old: " + oldValue + ", New:" + newValue);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    //incell editing for teaching subject class combination
    public void onRowEditSub(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Period Edited", ((TeacherSubjectClass) event.getObject()).getTeacherSubjectClassId() + "");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowCancelSub(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Edit Cancelled", ((TeacherSubjectClass) event.getObject()).getTeacherSubjectClassId() + "");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onCellEditSub(CellEditEvent event) {
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();

        if (newValue != null && !newValue.equals(oldValue)) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Cell Changed", "Old: " + oldValue + ", New:" + newValue);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void savePeriod() {
        try {
            if (getAllPeriods() != null) {
                for (Period p : getAllPeriods()) {
                    if (timetableSession.deleteObjects(p)) {
                        System.out.println("Success delete Period!!");
                    }
                }
            }
            for (Period p : periods) {
                p.setPeriodId((int) (Math.random() * 10));
                if (timetableSession.persist(p)) {
                    FacesMessage msg = new FacesMessage("Periods saved successfully" + "");
                    FacesContext.getCurrentInstance().addMessage(null, msg);
                } else {
                    System.out.println("Error saving periods");
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        periodShow = false;
    }

    public SchoolClass getStudentClass(String id) {
        return timetableSession.findBySchoolClass(id);
    }

    public SchoolSubject getSubject(String id) {
        return timetableSession.findBySchoolSubject(id);
    }

    @FacesConverter(forClass = SchoolClass.class, value = "schoolClassConverter")
    public class SchoolClassConverter implements Converter {

        SchoolClass schoolClass;

        @Override
        public Object getAsObject(FacesContext context, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            TimetableWizard controller = (TimetableWizard) context.getApplication().getELResolver()
                    .getValue(context.getELContext(), null, "timetableWizard");
            return controller.getStudentClass((getKey(value)));
        }

        String getKey(String value) {
            java.lang.String key;
            key = value;
            return key;
        }

        String getStringKey(java.lang.Long value) {
            StringBuilder sb = new StringBuilder();
            sb.append(sb);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext context, UIComponent component, Object value) {
            if (value == null) {
                return null;
            }
            if (value instanceof SchoolClass) {
                SchoolClass sc = (SchoolClass) value;
                return getStringKey(Long.parseLong(sc.getClassCode()));
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Object {0} is of type {1}; expected type {2}", new Object[]{value, value.getClass().getName(), SchoolClass.class.getName()});
                return null;
            }
        }

    }

    @FacesConverter(forClass = SchoolSubject.class, value = "schoolSubjectConverter")
    public class SchoolSubjectConverter implements Converter {

        SchoolSubject schoolSubject;

        @Override
        public Object getAsObject(FacesContext context, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            TimetableWizard controller = (TimetableWizard) context.getApplication().getELResolver()
                    .getValue(context.getELContext(), null, "timetableWizard");
            return controller.getSubject((getKey(value)));
        }

        String getKey(String value) {
            java.lang.String key;
            key = value;
            return key;
        }

        String getStringKey(java.lang.Long value) {
            StringBuilder sb = new StringBuilder();
            sb.append(sb);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext context, UIComponent component, Object value) {
            if (value == null) {
                return null;
            }
            if (value instanceof SchoolSubject) {
                SchoolSubject sc = (SchoolSubject) value;
                return getStringKey(Long.parseLong(sc.getSubjectCode()));
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Object {0} is of type {1}; expected type {2}", new Object[]{value, value.getClass().getName(), SchoolSubject.class.getName()});
                return null;
            }
        }

    }

    public void subjectPanelShow() {
        subjectShow = true;
    }

    public void subjectPanelHide() {
        classShow = false;
    }

    public void classPanelShow() {
        classShow = true;
    }

    public void classPanelHide() {
        subjectShow = false;
    }

    public Period getPeriod() {
        return period;
    }

    public void setPeriod(Period period) {
        this.period = period;
    }

    public SchoolSubject getSubject() {
        return subject;
    }

    public void setSubject(SchoolSubject subject) {
        this.subject = subject;
    }

    public boolean isSkip() {
        return skip;
    }

    public void setSkip(boolean skip) {
        this.skip = skip;
    }

    public String onFlowProcess(FlowEvent event) {
        if (skip) {
            skip = false;   //reset in case user goes back
            return "confirm";
        } else {
            return event.getNewStep();
        }
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Map<Integer, Object> getPeriodsCount() {
        for (int i = 0; i <= 30; i++) {
            periodsCount.put(i, i);
        }
        return periodsCount;
    }

    public void setPeriodsCount(Map<Integer, Object> periodsCount) {
        this.periodsCount = periodsCount;
    }

    public SchoolClass getSchoolClass() {
        return schoolClass;
    }

    public void setSchoolClass(SchoolClass schoolClass) {
        this.schoolClass = schoolClass;
    }

    public List<Period> getPeriods() {
        return periods;
    }

    public void setPeriods(List<Period> periods) {
        this.periods = periods;
    }

    public boolean isPeriodShow() {
        return periodShow;
    }

    public void setPeriodShow(boolean periodShow) {
        this.periodShow = periodShow;
    }

    public boolean isSubjectShow() {
        return subjectShow;
    }

    public void setSubjectShow(boolean subjectShow) {
        this.subjectShow = subjectShow;
    }

    public boolean isClassShow() {
        return classShow;
    }

    public void setClassShow(boolean classShow) {
        this.classShow = classShow;
    }

    public SchoolStaff getSchoolStaff() {
        return schoolStaff;
    }

    public void setSchoolStaff(SchoolStaff schoolStaff) {
        this.schoolStaff = schoolStaff;
    }

    public TeacherSubjectClass getTsc() {
        return tsc;
    }

    public void setTsc(TeacherSubjectClass tsc) {
        this.tsc = tsc;
    }

    public List<TeacherSubjectClass> getTeacherSubjectClasses() {
        return teacherSubjectClasses;
    }

    public void setTeacherSubjectClasses(List<TeacherSubjectClass> teacherSubjectClasses) {
        this.teacherSubjectClasses = teacherSubjectClasses;
    }

    public boolean isTeacherDialog() {
        return teacherDialog;
    }

    public void setTeacherDialog(boolean teacherDialog) {
        this.teacherDialog = teacherDialog;
    }

    public SelectItem[] getTeacherSelection() {
        List<SchoolStaff> st = new ArrayList<>();
        List<SchoolStaff> ststs = new ArrayList<>();
        st = timetableSession.schoolStaffGetAll();
        for (SchoolStaff sst : st) {
            if (sst.getSubjectTeachingId() != null) {
                ststs.add(sst);
            }
        }
        teacherSelection = new SelectItem[ststs.size()];
        int c = 0;
        for (SchoolStaff a : ststs) {
            teacherSelection[c++] = new SelectItem(a.getSurname() + " " + a.getOthernames(), a.getSurname() + " " + a.getOthernames());
        }
        return teacherSelection;
    }

    public SelectItem[] getClassSelection() {
        List<SchoolClass> clases = new ArrayList<>();
        clases = timetableSession.schoolClassGetAll();
        classSelection = new SelectItem[clases.size()];
        int sc = 0;
        for (SchoolClass clase : clases) {
            classSelection[sc++] = new SelectItem(clase.getClassName(), clase.getClassName());
        }
        return classSelection;
    }

    public void initialize() {
        // create timeline model
        model = new TimelineModel();
        displayModel = true;
        List<TimelineGroup> groups = new ArrayList<>();
        List<TeacherSubjectClass> classList = new ArrayList<>();
        classList = timetableSession.getTeacherSubjectByClass(className);
        // create groups
        for (String d : selectedDays) {
            TimelineGroup group = new TimelineGroup(d, new DayWrapper(d, (int) (Math.random())));
            // add group to the groups list
            groups.add(group);
        }

        for (TimelineGroup g : groups) {
            model.addGroup(g);
        }

        // Server-side dates should be in UTC. They will be converted to a local dates in UI according to provided TimeZone.
        // Submitted local dates in UI are converted back to UTC, so that server receives all dates in UTC again.
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        // iterate over groups
        for (int j = 1; j <= 5; j++) {
            cal.set(2014, Calendar.DECEMBER, 22, 7, 0, 0);
            // iterate over events in the same group

            for (TeacherSubjectClass t : classList) {

                cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY) + 3 * (Math.random() < 0.2 ? 1 : 0));
                Date startDate = cal.getTime();

                cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY) + 2 + (int) Math.floor(Math.random() * 4));
                Date endDate = cal.getTime();

                TeacherSubjectWrapper wrapper = new TeacherSubjectWrapper(t.getClassTeacherId(), t.getSchoolSubjectId());
                model.add(new TimelineEvent(wrapper, startDate, endDate, false, "id" + j));

            }
        }
    }

    public TimelineModel getModel() {
        return model;
    }

    public void onChange(TimelineModificationEvent e) {
        // get changed event and update the model
        event = e.getTimelineEvent();
        model.update(event);

        // get overlapped events of the same group as for the changed event
        TreeSet<TimelineEvent> overlappedEvents = model.getOverlappedEvents(event);

        if (overlappedEvents == null) {
            // nothing to merge
            return;
        }

        // list of orders which can be merged in the dialog
        overlappedOrders = new ArrayList<>(overlappedEvents);

        // no pre-selection
        ordersToMerge = null;

        // update the dialog's content and show the dialog
        RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.update("overlappedOrdersInner");
        requestContext.execute("PF('overlapEventsWdgt').show()");
    }

    public void onDelete(TimelineModificationEvent e) {
        // keep the model up-to-date
        model.delete(e.getTimelineEvent());
    }

    public void merge() {
        // merge orders and update UI if the user selected some orders to be merged
        if (ordersToMerge != null && !ordersToMerge.isEmpty()) {
            model.merge(event, ordersToMerge, TimelineUpdater.getCurrentInstance(":mainForm:timeline"));
        } else {
            FacesMessage msg
                    = new FacesMessage(FacesMessage.SEVERITY_INFO, "Nothing to merge, please choose orders to be merged", null);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }

        overlappedOrders = null;
        ordersToMerge = null;
    }

    public String getSelectedOrder() {
        if (event == null) {
            return null;
        }

        return ((TeacherSubjectClass) event.getData()).getClassTeacherId();
    }

    public List<TimelineEvent> getOverlappedOrders() {
        return overlappedOrders;
    }

    public List<TimelineEvent> getOrdersToMerge() {
        return ordersToMerge;
    }

    public void setOrdersToMerge(List<TimelineEvent> ordersToMerge) {
        this.ordersToMerge = ordersToMerge;
    }

    public void setClassSelection(SelectItem[] classSelection) {
        this.classSelection = classSelection;
    }

    public void setTeacherSelection(SelectItem[] teacherSelection) {
        this.teacherSelection = teacherSelection;
    }

    public List<SchoolClass> getSchoolClasses() {
        return schoolClasses;
    }

    public void setSchoolClasses(List<SchoolClass> schoolClasses) {
        this.schoolClasses = schoolClasses;
    }

    public List<SchoolSubject> getSchoolSubjects() {
        return schoolSubjects;
    }

    public void setSchoolSubjects(List<SchoolSubject> schoolSubjects) {
        this.schoolSubjects = schoolSubjects;
    }

    public List<Day> getDays() {
        return days;
    }

    public Map<Day, String> getDaysSelection() {
        Map<Day, String> selected = new HashMap<>();
        for (Day d : Day.asList()) {
            selected.put(d, d.getLabel());
        }
        return selected;
    }

    public void setDays(List<Day> days) {
        this.days = days;
    }

    public List<TeachingSubAndClasses> getTeacherSubjectClassCombinations() {
        return teacherSubjectClassCombinations;
    }

    public void setTeacherSubjectClassCombinations(List<TeachingSubAndClasses> teacherSubjectClassCombinations) {
        this.teacherSubjectClassCombinations = teacherSubjectClassCombinations;
    }

    public String getCurrentTerm() {
        return currentTerm;
    }

    public void setCurrentTerm(String currentTerm) {
        this.currentTerm = currentTerm;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public boolean isDisplayModel() {
        return displayModel;
    }

    public void setDisplayModel(boolean displayModel) {
        this.displayModel = displayModel;
    }

    public void setModel(TimelineModel model) {
        this.model = model;
    }

    public TimelineEvent getEvent() {
        return event;
    }

    public void setEvent(TimelineEvent event) {
        this.event = event;
    }

    public boolean isEditShow() {
        return editShow;
    }

    public void setEditShow(boolean editShow) {
        this.editShow = editShow;
    }

    public String getClassSelectionName() {
        return classSelectionName;
    }

    public void setClassSelectionName(String classSelectionName) {
        this.classSelectionName = classSelectionName;
    }

    public List<TeacherSubjectClass> getClassListings() {
        return classListings;
    }

    public void setClassListings(List<TeacherSubjectClass> classListings) {
        this.classListings = classListings;
    }

    public List<String> getSelectedDays() {
        return selectedDays;
    }

    public void setSelectedDays(List<String> selectedDays) {
        this.selectedDays = selectedDays;
    }

}
