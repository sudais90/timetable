package com.sabonay.timetable.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-02-27T15:02:50")
@StaticMetamodel(SmsBlast.class)
public class SmsBlast_ { 

    public static volatile SingularAttribute<SmsBlast, String> smsSubject;
    public static volatile SingularAttribute<SmsBlast, String> updated;
    public static volatile SingularAttribute<SmsBlast, String> lastModifiedBy;
    public static volatile SingularAttribute<SmsBlast, Date> lastModifiedDate;
    public static volatile SingularAttribute<SmsBlast, String> smsBlastId;
    public static volatile SingularAttribute<SmsBlast, Date> smsSendDate;
    public static volatile SingularAttribute<SmsBlast, String> contactGroup;
    public static volatile SingularAttribute<SmsBlast, String> smsText;
    public static volatile SingularAttribute<SmsBlast, String> contactGroupValue;
    public static volatile SingularAttribute<SmsBlast, String> deleted;

}