/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sabonay.timetable.constant;

/**
 *
 * @author Amina
 */
public interface MessageResolvable {

    public String getMessageKey();

    public void setLable(String label);

    public String getLabel();
}
