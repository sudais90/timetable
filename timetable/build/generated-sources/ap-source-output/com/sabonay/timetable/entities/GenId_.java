package com.sabonay.timetable.entities;

import com.sabonay.timetable.entities.GenIdPK;
import java.math.BigInteger;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-02-27T15:02:50")
@StaticMetamodel(GenId.class)
public class GenId_ { 

    public static volatile SingularAttribute<GenId, Integer> id;
    public static volatile SingularAttribute<GenId, BigInteger> idBig;
    public static volatile SingularAttribute<GenId, GenIdPK> genIdPK;

}