package com.sabonay.timetable.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-02-27T15:02:50")
@StaticMetamodel(SmsMark.class)
public class SmsMark_ { 

    public static volatile SingularAttribute<SmsMark, String> position;
    public static volatile SingularAttribute<SmsMark, String> studentId;
    public static volatile SingularAttribute<SmsMark, String> smsMarkId;
    public static volatile SingularAttribute<SmsMark, String> mark;
    public static volatile SingularAttribute<SmsMark, String> subjectCode;
    public static volatile SingularAttribute<SmsMark, String> studentClass;

}