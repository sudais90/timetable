/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sabonay.timetable.classfiles;

import java.io.Serializable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

/**
 *
 * @author AMINA
 */
public class DayWrapper implements Serializable {

    private int id;
    private String day;

    public DayWrapper() {
    }

    public DayWrapper(String day, int i) {
        this.day = day;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

}
