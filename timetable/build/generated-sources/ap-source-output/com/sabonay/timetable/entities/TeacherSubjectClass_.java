package com.sabonay.timetable.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-02-27T15:02:50")
@StaticMetamodel(TeacherSubjectClass.class)
public class TeacherSubjectClass_ { 

    public static volatile SingularAttribute<TeacherSubjectClass, Integer> lessonsPerWeek;
    public static volatile SingularAttribute<TeacherSubjectClass, String> updated;
    public static volatile SingularAttribute<TeacherSubjectClass, String> schoolSubjectId;
    public static volatile SingularAttribute<TeacherSubjectClass, String> schoolClassId;
    public static volatile SingularAttribute<TeacherSubjectClass, String> teacherSubjectClassId;
    public static volatile SingularAttribute<TeacherSubjectClass, String> classTeacherId;
    public static volatile SingularAttribute<TeacherSubjectClass, String> deleted;

}