/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sabonay.timetable.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Amina
 */
@Entity
@Table(name = "academic_year")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AcademicYear.findAll", query = "SELECT a FROM AcademicYear a"),
    @NamedQuery(name = "AcademicYear.findByAcademicYearId", query = "SELECT a FROM AcademicYear a WHERE a.academicYearId = :academicYearId"),
    @NamedQuery(name = "AcademicYear.findByBeginDate", query = "SELECT a FROM AcademicYear a WHERE a.beginDate = :beginDate"),
    @NamedQuery(name = "AcademicYear.findByEndDate", query = "SELECT a FROM AcademicYear a WHERE a.endDate = :endDate"),
    @NamedQuery(name = "AcademicYear.findBySchoolNumber", query = "SELECT a FROM AcademicYear a WHERE a.schoolNumber = :schoolNumber"),
    @NamedQuery(name = "AcademicYear.findByDeleted", query = "SELECT a FROM AcademicYear a WHERE a.deleted = :deleted"),
    @NamedQuery(name = "AcademicYear.findByUpdated", query = "SELECT a FROM AcademicYear a WHERE a.updated = :updated")})
public class AcademicYear implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "academic_year_id")
    private String academicYearId;
    @Column(name = "begin_date")
    @Temporal(TemporalType.DATE)
    private Date beginDate;
    @Column(name = "end_date")
    @Temporal(TemporalType.DATE)
    private Date endDate;
    @Size(max = 50)
    @Column(name = "school_number")
    private String schoolNumber;
    @Size(max = 10)
    @Column(name = "deleted")
    private String deleted;
    @Size(max = 10)
    @Column(name = "updated")
    private String updated;

    public AcademicYear() {
    }

    public AcademicYear(String academicYearId) {
        this.academicYearId = academicYearId;
    }

    public String getAcademicYearId() {
        return academicYearId;
    }

    public void setAcademicYearId(String academicYearId) {
        this.academicYearId = academicYearId;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getSchoolNumber() {
        return schoolNumber;
    }

    public void setSchoolNumber(String schoolNumber) {
        this.schoolNumber = schoolNumber;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (academicYearId != null ? academicYearId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AcademicYear)) {
            return false;
        }
        AcademicYear other = (AcademicYear) object;
        if ((this.academicYearId == null && other.academicYearId != null) || (this.academicYearId != null && !this.academicYearId.equals(other.academicYearId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sabonay.timetable.entities.AcademicYear[ academicYearId=" + academicYearId + " ]";
    }
    
}
