package com.sabonay.timetable.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-02-27T15:02:50")
@StaticMetamodel(SchoolHouse.class)
public class SchoolHouse_ { 

    public static volatile SingularAttribute<SchoolHouse, String> inmatesGender;
    public static volatile SingularAttribute<SchoolHouse, String> schoolHouseId;
    public static volatile SingularAttribute<SchoolHouse, String> otherHouseWarders;
    public static volatile SingularAttribute<SchoolHouse, String> houseName;
    public static volatile SingularAttribute<SchoolHouse, String> updated;
    public static volatile SingularAttribute<SchoolHouse, String> schoolNumber;
    public static volatile SingularAttribute<SchoolHouse, String> houseWarder;
    public static volatile SingularAttribute<SchoolHouse, String> lastModifiedBy;
    public static volatile SingularAttribute<SchoolHouse, Date> lastModifiedDate;
    public static volatile SingularAttribute<SchoolHouse, String> deleted;

}