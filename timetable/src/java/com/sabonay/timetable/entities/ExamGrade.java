/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sabonay.timetable.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Amina
 */
@Entity
@Table(name = "exam_grade")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ExamGrade.findAll", query = "SELECT e FROM ExamGrade e"),
    @NamedQuery(name = "ExamGrade.findByExamGradeId", query = "SELECT e FROM ExamGrade e WHERE e.examGradeId = :examGradeId"),
    @NamedQuery(name = "ExamGrade.findByGradeLowerBound", query = "SELECT e FROM ExamGrade e WHERE e.gradeLowerBound = :gradeLowerBound"),
    @NamedQuery(name = "ExamGrade.findByGradeUpperBound", query = "SELECT e FROM ExamGrade e WHERE e.gradeUpperBound = :gradeUpperBound"),
    @NamedQuery(name = "ExamGrade.findByGradeName", query = "SELECT e FROM ExamGrade e WHERE e.gradeName = :gradeName"),
    @NamedQuery(name = "ExamGrade.findByGradeDescription", query = "SELECT e FROM ExamGrade e WHERE e.gradeDescription = :gradeDescription"),
    @NamedQuery(name = "ExamGrade.findBySchoolNumber", query = "SELECT e FROM ExamGrade e WHERE e.schoolNumber = :schoolNumber"),
    @NamedQuery(name = "ExamGrade.findByDeleted", query = "SELECT e FROM ExamGrade e WHERE e.deleted = :deleted"),
    @NamedQuery(name = "ExamGrade.findByUpdated", query = "SELECT e FROM ExamGrade e WHERE e.updated = :updated")})
public class ExamGrade implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "exam_grade_id")
    private String examGradeId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "grade_lower_bound")
    private Double gradeLowerBound;
    @Column(name = "grade_upper_bound")
    private Double gradeUpperBound;
    @Size(max = 255)
    @Column(name = "grade_name")
    private String gradeName;
    @Size(max = 255)
    @Column(name = "grade_description")
    private String gradeDescription;
    @Size(max = 50)
    @Column(name = "school_number")
    private String schoolNumber;
    @Size(max = 255)
    @Column(name = "deleted")
    private String deleted;
    @Size(max = 255)
    @Column(name = "updated")
    private String updated;

    public ExamGrade() {
    }

    public ExamGrade(String examGradeId) {
        this.examGradeId = examGradeId;
    }

    public String getExamGradeId() {
        return examGradeId;
    }

    public void setExamGradeId(String examGradeId) {
        this.examGradeId = examGradeId;
    }

    public Double getGradeLowerBound() {
        return gradeLowerBound;
    }

    public void setGradeLowerBound(Double gradeLowerBound) {
        this.gradeLowerBound = gradeLowerBound;
    }

    public Double getGradeUpperBound() {
        return gradeUpperBound;
    }

    public void setGradeUpperBound(Double gradeUpperBound) {
        this.gradeUpperBound = gradeUpperBound;
    }

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }

    public String getGradeDescription() {
        return gradeDescription;
    }

    public void setGradeDescription(String gradeDescription) {
        this.gradeDescription = gradeDescription;
    }

    public String getSchoolNumber() {
        return schoolNumber;
    }

    public void setSchoolNumber(String schoolNumber) {
        this.schoolNumber = schoolNumber;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (examGradeId != null ? examGradeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ExamGrade)) {
            return false;
        }
        ExamGrade other = (ExamGrade) object;
        if ((this.examGradeId == null && other.examGradeId != null) || (this.examGradeId != null && !this.examGradeId.equals(other.examGradeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sabonay.timetable.entities.ExamGrade[ examGradeId=" + examGradeId + " ]";
    }
    
}
