package com.sabonay.timetable.classfiles;

/**
 *
 * @author J-bos
 */
public class FileContentSettings {

    private String[] startMarkers = null;
    private String[] endMarkers = null;
    private boolean showLineWithMarker = false;

    public String[] getStartMarkers() {
        if (startMarkers == null) {
            return new String[0];
        }
        return startMarkers;
    }

    public String[] getEndMarkers() {
        if (endMarkers == null) {
            return new String[0];
        }
        return endMarkers;
    }

    public boolean isShowLineWithMarker() {
        return showLineWithMarker;
    }

    public FileContentSettings setStartMarkers(String... startMarkers) {
        this.startMarkers = startMarkers;
        return this;
    }

    public FileContentSettings setEndMarkers(String... endMarkers) {
        this.endMarkers = endMarkers;
        return this;
    }

    public FileContentSettings setShowLineWithMarker(boolean showLineWithMarker) {
        this.showLineWithMarker = showLineWithMarker;
        return this;
    }
}
