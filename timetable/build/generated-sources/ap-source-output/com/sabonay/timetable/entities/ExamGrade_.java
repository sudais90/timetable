package com.sabonay.timetable.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-02-27T15:02:50")
@StaticMetamodel(ExamGrade.class)
public class ExamGrade_ { 

    public static volatile SingularAttribute<ExamGrade, Double> gradeLowerBound;
    public static volatile SingularAttribute<ExamGrade, String> gradeName;
    public static volatile SingularAttribute<ExamGrade, String> examGradeId;
    public static volatile SingularAttribute<ExamGrade, String> updated;
    public static volatile SingularAttribute<ExamGrade, String> schoolNumber;
    public static volatile SingularAttribute<ExamGrade, Double> gradeUpperBound;
    public static volatile SingularAttribute<ExamGrade, String> gradeDescription;
    public static volatile SingularAttribute<ExamGrade, String> deleted;

}