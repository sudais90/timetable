/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sabonay.timetable.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Amina
 */
@Entity
@Table(name = "school_staff")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SchoolStaff.findAll", query = "SELECT s FROM SchoolStaff s"),
    @NamedQuery(name = "SchoolStaff.findByStaffId", query = "SELECT s FROM SchoolStaff s WHERE s.staffId = :staffId"),
    @NamedQuery(name = "SchoolStaff.findBySurname", query = "SELECT s FROM SchoolStaff s WHERE s.surname = :surname"),
    @NamedQuery(name = "SchoolStaff.findByOthernames", query = "SELECT s FROM SchoolStaff s WHERE s.othernames = :othernames"),
    @NamedQuery(name = "SchoolStaff.findByGender", query = "SELECT s FROM SchoolStaff s WHERE s.gender = :gender"),
    @NamedQuery(name = "SchoolStaff.findByDateOfBirth", query = "SELECT s FROM SchoolStaff s WHERE s.dateOfBirth = :dateOfBirth"),
    @NamedQuery(name = "SchoolStaff.findByDiabilitiesAllergies", query = "SELECT s FROM SchoolStaff s WHERE s.diabilitiesAllergies = :diabilitiesAllergies"),
    @NamedQuery(name = "SchoolStaff.findByRegionId", query = "SELECT s FROM SchoolStaff s WHERE s.regionId = :regionId"),
    @NamedQuery(name = "SchoolStaff.findByHometown", query = "SELECT s FROM SchoolStaff s WHERE s.hometown = :hometown"),
    @NamedQuery(name = "SchoolStaff.findByDateOfAppointment", query = "SELECT s FROM SchoolStaff s WHERE s.dateOfAppointment = :dateOfAppointment"),
    @NamedQuery(name = "SchoolStaff.findBySubjectTeachingId", query = "SELECT s FROM SchoolStaff s WHERE s.subjectTeachingId = :subjectTeachingId"),
    @NamedQuery(name = "SchoolStaff.findByEmailAddress", query = "SELECT s FROM SchoolStaff s WHERE s.emailAddress = :emailAddress"),
    @NamedQuery(name = "SchoolStaff.findByContactNumber", query = "SELECT s FROM SchoolStaff s WHERE s.contactNumber = :contactNumber"),
    @NamedQuery(name = "SchoolStaff.findByStaffCategory", query = "SELECT s FROM SchoolStaff s WHERE s.staffCategory = :staffCategory"),
    @NamedQuery(name = "SchoolStaff.findBySchoolNumber", query = "SELECT s FROM SchoolStaff s WHERE s.schoolNumber = :schoolNumber"),
    @NamedQuery(name = "SchoolStaff.findByDeleted", query = "SELECT s FROM SchoolStaff s WHERE s.deleted = :deleted"),
    @NamedQuery(name = "SchoolStaff.findByUpdated", query = "SELECT s FROM SchoolStaff s WHERE s.updated = :updated"),
    @NamedQuery(name = "SchoolStaff.findByRegion", query = "SELECT s FROM SchoolStaff s WHERE s.region = :region"),
    @NamedQuery(name = "SchoolStaff.findByMaritalStatus", query = "SELECT s FROM SchoolStaff s WHERE s.maritalStatus = :maritalStatus"),
    @NamedQuery(name = "SchoolStaff.findByInService", query = "SELECT s FROM SchoolStaff s WHERE s.inService = :inService")})
public class SchoolStaff implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 244)
    @Column(name = "staff_id")
    private String staffId;
    @Size(max = 255)
    @Column(name = "surname")
    private String surname;
    @Size(max = 255)
    @Column(name = "othernames")
    private String othernames;
    @Size(max = 255)
    @Column(name = "gender")
    private String gender;
    @Column(name = "date_of_birth")
    @Temporal(TemporalType.DATE)
    private Date dateOfBirth;
    @Size(max = 255)
    @Column(name = "diabilities_allergies")
    private String diabilitiesAllergies;
    @Size(max = 255)
    @Column(name = "region_id")
    private String regionId;
    @Size(max = 255)
    @Column(name = "hometown")
    private String hometown;
    @Column(name = "date_of_appointment")
    @Temporal(TemporalType.DATE)
    private Date dateOfAppointment;
    @Size(max = 255)
    @Column(name = "subject_teaching_id")
    private String subjectTeachingId;
    @Size(max = 255)
    @Column(name = "email_address")
    private String emailAddress;
    @Size(max = 255)
    @Column(name = "contact_number")
    private String contactNumber;
    @Size(max = 255)
    @Column(name = "staff_category")
    private String staffCategory;
    @Lob
    @Column(name = "picture")
    private byte[] picture;
    @Size(max = 100)
    @Column(name = "school_number")
    private String schoolNumber;
    @Size(max = 20)
    @Column(name = "deleted")
    private String deleted;
    @Size(max = 20)
    @Column(name = "updated")
    private String updated;
    @Size(max = 200)
    @Column(name = "region")
    private String region;
    @Size(max = 200)
    @Column(name = "marital_status")
    private String maritalStatus;
    @Size(max = 20)
    @Column(name = "in_service")
    private String inService;

    public SchoolStaff() {
    }

    public SchoolStaff(String staffId) {
        this.staffId = staffId;
    }

    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getOthernames() {
        return othernames;
    }

    public void setOthernames(String othernames) {
        this.othernames = othernames;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getDiabilitiesAllergies() {
        return diabilitiesAllergies;
    }

    public void setDiabilitiesAllergies(String diabilitiesAllergies) {
        this.diabilitiesAllergies = diabilitiesAllergies;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public String getHometown() {
        return hometown;
    }

    public void setHometown(String hometown) {
        this.hometown = hometown;
    }

    public Date getDateOfAppointment() {
        return dateOfAppointment;
    }

    public void setDateOfAppointment(Date dateOfAppointment) {
        this.dateOfAppointment = dateOfAppointment;
    }

    public String getSubjectTeachingId() {
        return subjectTeachingId;
    }

    public void setSubjectTeachingId(String subjectTeachingId) {
        this.subjectTeachingId = subjectTeachingId;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getStaffCategory() {
        return staffCategory;
    }

    public void setStaffCategory(String staffCategory) {
        this.staffCategory = staffCategory;
    }

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    public String getSchoolNumber() {
        return schoolNumber;
    }

    public void setSchoolNumber(String schoolNumber) {
        this.schoolNumber = schoolNumber;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getInService() {
        return inService;
    }

    public void setInService(String inService) {
        this.inService = inService;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (staffId != null ? staffId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SchoolStaff)) {
            return false;
        }
        SchoolStaff other = (SchoolStaff) object;
        if ((this.staffId == null && other.staffId != null) || (this.staffId != null && !this.staffId.equals(other.staffId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sabonay.timetable.entities.SchoolStaff[ staffId=" + staffId + " ]";
    }
    
}
