package com.sabonay.timetable.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-02-27T15:02:50")
@StaticMetamodel(StudentMockExamMark.class)
public class StudentMockExamMark_ { 

    public static volatile SingularAttribute<StudentMockExamMark, String> mockExamMarkId;
    public static volatile SingularAttribute<StudentMockExamMark, String> student;
    public static volatile SingularAttribute<StudentMockExamMark, Double> mockMark;
    public static volatile SingularAttribute<StudentMockExamMark, String> updated;
    public static volatile SingularAttribute<StudentMockExamMark, String> subject;
    public static volatile SingularAttribute<StudentMockExamMark, String> schoolNumber;
    public static volatile SingularAttribute<StudentMockExamMark, String> studentClass;
    public static volatile SingularAttribute<StudentMockExamMark, String> deleted;
    public static volatile SingularAttribute<StudentMockExamMark, String> academicYear;

}