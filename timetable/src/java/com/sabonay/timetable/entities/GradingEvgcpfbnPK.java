/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sabonay.timetable.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Amina
 */
@Embeddable
public class GradingEvgcpfbnPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "schid")
    private String schid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "grade_name")
    private int gradeName;

    public GradingEvgcpfbnPK() {
    }

    public GradingEvgcpfbnPK(String schid, int gradeName) {
        this.schid = schid;
        this.gradeName = gradeName;
    }

    public String getSchid() {
        return schid;
    }

    public void setSchid(String schid) {
        this.schid = schid;
    }

    public int getGradeName() {
        return gradeName;
    }

    public void setGradeName(int gradeName) {
        this.gradeName = gradeName;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (schid != null ? schid.hashCode() : 0);
        hash += (int) gradeName;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GradingEvgcpfbnPK)) {
            return false;
        }
        GradingEvgcpfbnPK other = (GradingEvgcpfbnPK) object;
        if ((this.schid == null && other.schid != null) || (this.schid != null && !this.schid.equals(other.schid))) {
            return false;
        }
        if (this.gradeName != other.gradeName) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sabonay.timetable.entities.GradingEvgcpfbnPK[ schid=" + schid + ", gradeName=" + gradeName + " ]";
    }
    
}
