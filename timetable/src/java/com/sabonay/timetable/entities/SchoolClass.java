/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sabonay.timetable.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Amina
 */
@Entity
@Table(name = "school_class")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SchoolClass.findAll", query = "SELECT s FROM SchoolClass s"),
    @NamedQuery(name = "SchoolClass.findByClassCode", query = "SELECT s FROM SchoolClass s WHERE s.classCode = :classCode"),
    @NamedQuery(name = "SchoolClass.findByClassName", query = "SELECT s FROM SchoolClass s WHERE s.className = :className"),
    @NamedQuery(name = "SchoolClass.findByEducationalLevel", query = "SELECT s FROM SchoolClass s WHERE s.educationalLevel = :educationalLevel"),
    @NamedQuery(name = "SchoolClass.findByClassProgrammeCode", query = "SELECT s FROM SchoolClass s WHERE s.classProgrammeCode = :classProgrammeCode"),
    @NamedQuery(name = "SchoolClass.findBySchoolNumber", query = "SELECT s FROM SchoolClass s WHERE s.schoolNumber = :schoolNumber"),
    @NamedQuery(name = "SchoolClass.findByDeleted", query = "SELECT s FROM SchoolClass s WHERE s.deleted = :deleted"),
    @NamedQuery(name = "SchoolClass.findByUpdated", query = "SELECT s FROM SchoolClass s WHERE s.updated = :updated"),
    @NamedQuery(name = "SchoolClass.findByLastModifiedBy", query = "SELECT s FROM SchoolClass s WHERE s.lastModifiedBy = :lastModifiedBy"),
    @NamedQuery(name = "SchoolClass.findByLastModifiedDate", query = "SELECT s FROM SchoolClass s WHERE s.lastModifiedDate = :lastModifiedDate")})
public class SchoolClass implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "class_code")
    private String classCode;
    @Size(max = 200)
    @Column(name = "class_name")
    private String className;
    @Size(max = 255)
    @Column(name = "educational_level")
    private String educationalLevel;
    @Size(max = 255)
    @Column(name = "class_programme_code")
    private String classProgrammeCode;
    @Size(max = 50)
    @Column(name = "school_number")
    private String schoolNumber;
    @Size(max = 255)
    @Column(name = "deleted")
    private String deleted;
    @Size(max = 255)
    @Column(name = "updated")
    private String updated;
    @Size(max = 200)
    @Column(name = "last_modified_by")
    private String lastModifiedBy;
    @Column(name = "last_modified_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;

    public SchoolClass() {
    }

    public SchoolClass(String classCode) {
        this.classCode = classCode;
    }

    public String getClassCode() {
        return classCode;
    }

    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getEducationalLevel() {
        return educationalLevel;
    }

    public void setEducationalLevel(String educationalLevel) {
        this.educationalLevel = educationalLevel;
    }

    public String getClassProgrammeCode() {
        return classProgrammeCode;
    }

    public void setClassProgrammeCode(String classProgrammeCode) {
        this.classProgrammeCode = classProgrammeCode;
    }

    public String getSchoolNumber() {
        return schoolNumber;
    }

    public void setSchoolNumber(String schoolNumber) {
        this.schoolNumber = schoolNumber;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (classCode != null ? classCode.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SchoolClass)) {
            return false;
        }
        SchoolClass other = (SchoolClass) object;
        if ((this.classCode == null && other.classCode != null) || (this.classCode != null && !this.classCode.equals(other.classCode))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sabonay.timetable.entities.SchoolClass[ classCode=" + classCode + " ]";
    }
    
}
