package com.sabonay.timetable.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-02-27T15:02:50")
@StaticMetamodel(StudentBill.class)
public class StudentBill_ { 

    public static volatile SingularAttribute<StudentBill, String> studentBillId;
    public static volatile SingularAttribute<StudentBill, Double> boardingStudentAmt;
    public static volatile SingularAttribute<StudentBill, Double> dayStudentAmt;
    public static volatile SingularAttribute<StudentBill, String> studentBillType;
    public static volatile SingularAttribute<StudentBill, String> deleted;
    public static volatile SingularAttribute<StudentBill, String> schoolProgram;
    public static volatile SingularAttribute<StudentBill, String> schoolClass;
    public static volatile SingularAttribute<StudentBill, String> student;
    public static volatile SingularAttribute<StudentBill, String> updated;
    public static volatile SingularAttribute<StudentBill, String> schoolNumber;
    public static volatile SingularAttribute<StudentBill, String> billItem;
    public static volatile SingularAttribute<StudentBill, String> gender;
    public static volatile SingularAttribute<StudentBill, String> educationalLevel;
    public static volatile SingularAttribute<StudentBill, String> academicTerm;

}