package com.sabonay.timetable.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-02-27T15:02:50")
@StaticMetamodel(EducationalInstitution.class)
public class EducationalInstitution_ { 

    public static volatile SingularAttribute<EducationalInstitution, String> schoolName;
    public static volatile SingularAttribute<EducationalInstitution, String> educationInstitutionCycle;
    public static volatile SingularAttribute<EducationalInstitution, String> schoolContactNumber;
    public static volatile SingularAttribute<EducationalInstitution, String> schoolAlias;
    public static volatile SingularAttribute<EducationalInstitution, String> currentTerm;
    public static volatile SingularAttribute<EducationalInstitution, String> systemStatus;
    public static volatile SingularAttribute<EducationalInstitution, String> schoolAddress;
    public static volatile SingularAttribute<EducationalInstitution, Double> averageClassScore;
    public static volatile SingularAttribute<EducationalInstitution, String> schoolMotor;
    public static volatile SingularAttribute<EducationalInstitution, String> schSendingId;
    public static volatile SingularAttribute<EducationalInstitution, Double> totalExamMark;
    public static volatile SingularAttribute<EducationalInstitution, Double> averageExamScore;
    public static volatile SingularAttribute<EducationalInstitution, String> studentUpdatePersonalInfo;
    public static volatile SingularAttribute<EducationalInstitution, String> schoolNumber;
    public static volatile SingularAttribute<EducationalInstitution, String> masterPassword;
    public static volatile SingularAttribute<EducationalInstitution, Double> totalClassMark;
    public static volatile SingularAttribute<EducationalInstitution, String> educationalLevel;
    public static volatile SingularAttribute<EducationalInstitution, Integer> gradingSystem;
    public static volatile SingularAttribute<EducationalInstitution, Date> dateOfRegistration;
    public static volatile SingularAttribute<EducationalInstitution, String> masterUsername;

}