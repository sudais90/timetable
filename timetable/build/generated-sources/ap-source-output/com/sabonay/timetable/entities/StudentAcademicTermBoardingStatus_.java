package com.sabonay.timetable.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-02-27T15:02:50")
@StaticMetamodel(StudentAcademicTermBoardingStatus.class)
public class StudentAcademicTermBoardingStatus_ { 

    public static volatile SingularAttribute<StudentAcademicTermBoardingStatus, String> studentAcademicTermBoardingStatusId;
    public static volatile SingularAttribute<StudentAcademicTermBoardingStatus, String> student;
    public static volatile SingularAttribute<StudentAcademicTermBoardingStatus, String> updated;
    public static volatile SingularAttribute<StudentAcademicTermBoardingStatus, String> schoolNumber;
    public static volatile SingularAttribute<StudentAcademicTermBoardingStatus, String> boardingStatus;
    public static volatile SingularAttribute<StudentAcademicTermBoardingStatus, String> lastModifiedBy;
    public static volatile SingularAttribute<StudentAcademicTermBoardingStatus, Date> lastModifiedDate;
    public static volatile SingularAttribute<StudentAcademicTermBoardingStatus, String> deleted;
    public static volatile SingularAttribute<StudentAcademicTermBoardingStatus, String> academicTerm;

}