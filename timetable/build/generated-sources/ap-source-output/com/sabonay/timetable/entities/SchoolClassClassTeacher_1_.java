package com.sabonay.timetable.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-02-27T15:02:50")
@StaticMetamodel(SchoolClassClassTeacher_1.class)
public class SchoolClassClassTeacher_1_ { 

    public static volatile SingularAttribute<SchoolClassClassTeacher_1, String> updated;
    public static volatile SingularAttribute<SchoolClassClassTeacher_1, String> schoolClassId;
    public static volatile SingularAttribute<SchoolClassClassTeacher_1, String> classTeacherId;
    public static volatile SingularAttribute<SchoolClassClassTeacher_1, String> deleted;
    public static volatile SingularAttribute<SchoolClassClassTeacher_1, String> schoolClassTeacherId;

}