package com.sabonay.timetable.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-02-27T15:02:50")
@StaticMetamodel(DisciplineRecord.class)
public class DisciplineRecord_ { 

    public static volatile SingularAttribute<DisciplineRecord, String> recordDetails;
    public static volatile SingularAttribute<DisciplineRecord, String> student;
    public static volatile SingularAttribute<DisciplineRecord, String> updated;
    public static volatile SingularAttribute<DisciplineRecord, String> schoolNumber;
    public static volatile SingularAttribute<DisciplineRecord, Date> dateOfOccurence;
    public static volatile SingularAttribute<DisciplineRecord, String> disciplineRecordItem;
    public static volatile SingularAttribute<DisciplineRecord, String> comment;
    public static volatile SingularAttribute<DisciplineRecord, String> studentClass;
    public static volatile SingularAttribute<DisciplineRecord, String> deleted;
    public static volatile SingularAttribute<DisciplineRecord, String> academicTerm;
    public static volatile SingularAttribute<DisciplineRecord, String> disciplineRecordId;

}