package com.sabonay.timetable.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-02-27T15:02:50")
@StaticMetamodel(ClassMembership.class)
public class ClassMembership_ { 

    public static volatile SingularAttribute<ClassMembership, String> classMembershipId;
    public static volatile SingularAttribute<ClassMembership, String> studentId;
    public static volatile SingularAttribute<ClassMembership, String> updated;
    public static volatile SingularAttribute<ClassMembership, String> schoolNumber;
    public static volatile SingularAttribute<ClassMembership, String> className;
    public static volatile SingularAttribute<ClassMembership, String> studentSubjectCombinationsCode;
    public static volatile SingularAttribute<ClassMembership, String> deleted;
    public static volatile SingularAttribute<ClassMembership, String> academicYear;

}