/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sabonay.timetable.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Amina
 */
@Entity
@Table(name = "class_membership")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ClassMembership.findAll", query = "SELECT c FROM ClassMembership c"),
    @NamedQuery(name = "ClassMembership.findByClassMembershipId", query = "SELECT c FROM ClassMembership c WHERE c.classMembershipId = :classMembershipId"),
    @NamedQuery(name = "ClassMembership.findByAcademicYear", query = "SELECT c FROM ClassMembership c WHERE c.academicYear = :academicYear"),
    @NamedQuery(name = "ClassMembership.findByClassName", query = "SELECT c FROM ClassMembership c WHERE c.className = :className"),
    @NamedQuery(name = "ClassMembership.findByStudentId", query = "SELECT c FROM ClassMembership c WHERE c.studentId = :studentId"),
    @NamedQuery(name = "ClassMembership.findByStudentSubjectCombinationsCode", query = "SELECT c FROM ClassMembership c WHERE c.studentSubjectCombinationsCode = :studentSubjectCombinationsCode"),
    @NamedQuery(name = "ClassMembership.findBySchoolNumber", query = "SELECT c FROM ClassMembership c WHERE c.schoolNumber = :schoolNumber"),
    @NamedQuery(name = "ClassMembership.findByDeleted", query = "SELECT c FROM ClassMembership c WHERE c.deleted = :deleted"),
    @NamedQuery(name = "ClassMembership.findByUpdated", query = "SELECT c FROM ClassMembership c WHERE c.updated = :updated")})
public class ClassMembership implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 244)
    @Column(name = "class_membership_id")
    private String classMembershipId;
    @Size(max = 255)
    @Column(name = "academic_year")
    private String academicYear;
    @Size(max = 255)
    @Column(name = "class_name")
    private String className;
    @Size(max = 255)
    @Column(name = "student_id")
    private String studentId;
    @Size(max = 255)
    @Column(name = "student_subject_combinations_code")
    private String studentSubjectCombinationsCode;
    @Size(max = 50)
    @Column(name = "school_number")
    private String schoolNumber;
    @Size(max = 255)
    @Column(name = "deleted")
    private String deleted;
    @Size(max = 233)
    @Column(name = "updated")
    private String updated;

    public ClassMembership() {
    }

    public ClassMembership(String classMembershipId) {
        this.classMembershipId = classMembershipId;
    }

    public String getClassMembershipId() {
        return classMembershipId;
    }

    public void setClassMembershipId(String classMembershipId) {
        this.classMembershipId = classMembershipId;
    }

    public String getAcademicYear() {
        return academicYear;
    }

    public void setAcademicYear(String academicYear) {
        this.academicYear = academicYear;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getStudentSubjectCombinationsCode() {
        return studentSubjectCombinationsCode;
    }

    public void setStudentSubjectCombinationsCode(String studentSubjectCombinationsCode) {
        this.studentSubjectCombinationsCode = studentSubjectCombinationsCode;
    }

    public String getSchoolNumber() {
        return schoolNumber;
    }

    public void setSchoolNumber(String schoolNumber) {
        this.schoolNumber = schoolNumber;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (classMembershipId != null ? classMembershipId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClassMembership)) {
            return false;
        }
        ClassMembership other = (ClassMembership) object;
        if ((this.classMembershipId == null && other.classMembershipId != null) || (this.classMembershipId != null && !this.classMembershipId.equals(other.classMembershipId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sabonay.timetable.entities.ClassMembership[ classMembershipId=" + classMembershipId + " ]";
    }
    
}
