package com.sabonay.timetable.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-02-27T15:02:50")
@StaticMetamodel(AcademicTerm.class)
public class AcademicTerm_ { 

    public static volatile SingularAttribute<AcademicTerm, Date> examBeginDate;
    public static volatile SingularAttribute<AcademicTerm, String> academicYearId;
    public static volatile SingularAttribute<AcademicTerm, String> updated;
    public static volatile SingularAttribute<AcademicTerm, Date> examEndDate;
    public static volatile SingularAttribute<AcademicTerm, String> schoolNumber;
    public static volatile SingularAttribute<AcademicTerm, String> schoolTerm;
    public static volatile SingularAttribute<AcademicTerm, Date> endDate;
    public static volatile SingularAttribute<AcademicTerm, Date> beginDate;
    public static volatile SingularAttribute<AcademicTerm, String> deleted;
    public static volatile SingularAttribute<AcademicTerm, String> academicTermId;

}