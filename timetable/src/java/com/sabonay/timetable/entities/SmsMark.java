/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sabonay.timetable.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Amina
 */
@Entity
@Table(name = "sms_mark")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SmsMark.findAll", query = "SELECT s FROM SmsMark s"),
    @NamedQuery(name = "SmsMark.findBySmsMarkId", query = "SELECT s FROM SmsMark s WHERE s.smsMarkId = :smsMarkId"),
    @NamedQuery(name = "SmsMark.findByStudentClass", query = "SELECT s FROM SmsMark s WHERE s.studentClass = :studentClass"),
    @NamedQuery(name = "SmsMark.findBySubjectCode", query = "SELECT s FROM SmsMark s WHERE s.subjectCode = :subjectCode"),
    @NamedQuery(name = "SmsMark.findByMark", query = "SELECT s FROM SmsMark s WHERE s.mark = :mark"),
    @NamedQuery(name = "SmsMark.findByPosition", query = "SELECT s FROM SmsMark s WHERE s.position = :position"),
    @NamedQuery(name = "SmsMark.findByStudentId", query = "SELECT s FROM SmsMark s WHERE s.studentId = :studentId")})
public class SmsMark implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "sms_mark_id")
    private String smsMarkId;
    @Size(max = 30)
    @Column(name = "student_class")
    private String studentClass;
    @Size(max = 30)
    @Column(name = "subject_code")
    private String subjectCode;
    @Size(max = 5)
    @Column(name = "mark")
    private String mark;
    @Size(max = 5)
    @Column(name = "position")
    private String position;
    @Size(max = 10)
    @Column(name = "student_id")
    private String studentId;

    public SmsMark() {
    }

    public SmsMark(String smsMarkId) {
        this.smsMarkId = smsMarkId;
    }

    public String getSmsMarkId() {
        return smsMarkId;
    }

    public void setSmsMarkId(String smsMarkId) {
        this.smsMarkId = smsMarkId;
    }

    public String getStudentClass() {
        return studentClass;
    }

    public void setStudentClass(String studentClass) {
        this.studentClass = studentClass;
    }

    public String getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (smsMarkId != null ? smsMarkId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SmsMark)) {
            return false;
        }
        SmsMark other = (SmsMark) object;
        if ((this.smsMarkId == null && other.smsMarkId != null) || (this.smsMarkId != null && !this.smsMarkId.equals(other.smsMarkId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sabonay.timetable.entities.SmsMark[ smsMarkId=" + smsMarkId + " ]";
    }
    
}
