/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sabonay.timetable.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Amina
 */
@Entity
@Table(name = "scholarship")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Scholarship.findAll", query = "SELECT s FROM Scholarship s"),
    @NamedQuery(name = "Scholarship.findByScholarshipId", query = "SELECT s FROM Scholarship s WHERE s.scholarshipId = :scholarshipId"),
    @NamedQuery(name = "Scholarship.findByScholarship", query = "SELECT s FROM Scholarship s WHERE s.scholarship = :scholarship"),
    @NamedQuery(name = "Scholarship.findByAmountInvolve", query = "SELECT s FROM Scholarship s WHERE s.amountInvolve = :amountInvolve"),
    @NamedQuery(name = "Scholarship.findBySponsor", query = "SELECT s FROM Scholarship s WHERE s.sponsor = :sponsor"),
    @NamedQuery(name = "Scholarship.findBySponsorContactPerson", query = "SELECT s FROM Scholarship s WHERE s.sponsorContactPerson = :sponsorContactPerson"),
    @NamedQuery(name = "Scholarship.findBySponsorContact", query = "SELECT s FROM Scholarship s WHERE s.sponsorContact = :sponsorContact"),
    @NamedQuery(name = "Scholarship.findByDeleted", query = "SELECT s FROM Scholarship s WHERE s.deleted = :deleted"),
    @NamedQuery(name = "Scholarship.findByUpdated", query = "SELECT s FROM Scholarship s WHERE s.updated = :updated"),
    @NamedQuery(name = "Scholarship.findByLastModifiedBy", query = "SELECT s FROM Scholarship s WHERE s.lastModifiedBy = :lastModifiedBy"),
    @NamedQuery(name = "Scholarship.findByLastModifiedDate", query = "SELECT s FROM Scholarship s WHERE s.lastModifiedDate = :lastModifiedDate"),
    @NamedQuery(name = "Scholarship.findByScholarshipStatus", query = "SELECT s FROM Scholarship s WHERE s.scholarshipStatus = :scholarshipStatus")})
public class Scholarship implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "scholarship_id")
    private String scholarshipId;
    @Size(max = 50)
    @Column(name = "scholarship")
    private String scholarship;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "amount_involve")
    private Double amountInvolve;
    @Size(max = 50)
    @Column(name = "Sponsor")
    private String sponsor;
    @Size(max = 50)
    @Column(name = "sponsor_contact_person")
    private String sponsorContactPerson;
    @Size(max = 50)
    @Column(name = "sponsor_contact")
    private String sponsorContact;
    @Size(max = 50)
    @Column(name = "deleted")
    private String deleted;
    @Size(max = 50)
    @Column(name = "updated")
    private String updated;
    @Size(max = 50)
    @Column(name = "last_modified_by")
    private String lastModifiedBy;
    @Column(name = "last_modified_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;
    @Size(max = 50)
    @Column(name = "scholarship_status")
    private String scholarshipStatus;

    public Scholarship() {
    }

    public Scholarship(String scholarshipId) {
        this.scholarshipId = scholarshipId;
    }

    public String getScholarshipId() {
        return scholarshipId;
    }

    public void setScholarshipId(String scholarshipId) {
        this.scholarshipId = scholarshipId;
    }

    public String getScholarship() {
        return scholarship;
    }

    public void setScholarship(String scholarship) {
        this.scholarship = scholarship;
    }

    public Double getAmountInvolve() {
        return amountInvolve;
    }

    public void setAmountInvolve(Double amountInvolve) {
        this.amountInvolve = amountInvolve;
    }

    public String getSponsor() {
        return sponsor;
    }

    public void setSponsor(String sponsor) {
        this.sponsor = sponsor;
    }

    public String getSponsorContactPerson() {
        return sponsorContactPerson;
    }

    public void setSponsorContactPerson(String sponsorContactPerson) {
        this.sponsorContactPerson = sponsorContactPerson;
    }

    public String getSponsorContact() {
        return sponsorContact;
    }

    public void setSponsorContact(String sponsorContact) {
        this.sponsorContact = sponsorContact;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getScholarshipStatus() {
        return scholarshipStatus;
    }

    public void setScholarshipStatus(String scholarshipStatus) {
        this.scholarshipStatus = scholarshipStatus;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (scholarshipId != null ? scholarshipId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Scholarship)) {
            return false;
        }
        Scholarship other = (Scholarship) object;
        if ((this.scholarshipId == null && other.scholarshipId != null) || (this.scholarshipId != null && !this.scholarshipId.equals(other.scholarshipId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sabonay.timetable.entities.Scholarship[ scholarshipId=" + scholarshipId + " ]";
    }
    
}
