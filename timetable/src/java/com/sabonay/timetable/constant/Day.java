/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sabonay.timetable.constant;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Amina
 */
public enum Day implements MessageResolvable {

    SUNDAY("sunday", "Sunday"),
    MONDAY("monday", "Monday"),
    TUESDAY("tuesday", "Tuesday"),
    WEDNESDAY("wednesday", "Wednesday"),
    THURSDAY("thursday", "Thursday"),
    FRIDAY("friday", "Friday"),
    SATURDAY("saturday", "Saturday");

    private String label;
    private final String messageKey;

    Day(String messageKey, String label) {
        this.messageKey = messageKey;
        this.label = label;
    }

    @Override
    public String getMessageKey() {
        return messageKey;
    }

    @Override
    public String getLabel() {
        return label;
    }

    @Override
    public void setLable(String label) {
        if (label == null || label.isEmpty()) {
            return;
        }
        this.label = label;
    }

    @Override
    public String toString() {
        return label;
    }
    
    public static List<Day> asList(){
        return Arrays.asList(Day.values());
    }
    
    public static List<MessageResolvable> asResolvableList(){
        List<MessageResolvable> resolvables = new ArrayList<>();
        resolvables.addAll(asList());
        return resolvables;
    }

}
