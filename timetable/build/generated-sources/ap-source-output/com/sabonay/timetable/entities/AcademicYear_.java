package com.sabonay.timetable.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-02-27T15:02:50")
@StaticMetamodel(AcademicYear.class)
public class AcademicYear_ { 

    public static volatile SingularAttribute<AcademicYear, String> academicYearId;
    public static volatile SingularAttribute<AcademicYear, String> updated;
    public static volatile SingularAttribute<AcademicYear, String> schoolNumber;
    public static volatile SingularAttribute<AcademicYear, Date> endDate;
    public static volatile SingularAttribute<AcademicYear, Date> beginDate;
    public static volatile SingularAttribute<AcademicYear, String> deleted;

}