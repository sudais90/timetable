/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sabonay.timetable.classfiles;

import java.io.Serializable;

/**
 *
 * @author J-bos
 */
public class Order implements Serializable {

    private int number;
    private String imagePath;

    public Order(int number, String imagePath) {
        this.number = number;
        this.imagePath = imagePath;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getImagePath() {
        return imagePath;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Order order = (Order) o;

        if (number != order.number) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return number;
    }
}
