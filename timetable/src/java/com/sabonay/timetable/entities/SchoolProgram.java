/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sabonay.timetable.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Amina
 */
@Entity
@Table(name = "school_program")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SchoolProgram.findAll", query = "SELECT s FROM SchoolProgram s"),
    @NamedQuery(name = "SchoolProgram.findByProgramCode", query = "SELECT s FROM SchoolProgram s WHERE s.programCode = :programCode"),
    @NamedQuery(name = "SchoolProgram.findByProgramName", query = "SELECT s FROM SchoolProgram s WHERE s.programName = :programName"),
    @NamedQuery(name = "SchoolProgram.findByViewOrder", query = "SELECT s FROM SchoolProgram s WHERE s.viewOrder = :viewOrder"),
    @NamedQuery(name = "SchoolProgram.findByDeleted", query = "SELECT s FROM SchoolProgram s WHERE s.deleted = :deleted"),
    @NamedQuery(name = "SchoolProgram.findByUpdated", query = "SELECT s FROM SchoolProgram s WHERE s.updated = :updated")})
public class SchoolProgram implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "program_code")
    private String programCode;
    @Size(max = 255)
    @Column(name = "program_name")
    private String programName;
    @Column(name = "view_order")
    private Integer viewOrder;
    @Size(max = 10)
    @Column(name = "deleted")
    private String deleted;
    @Size(max = 20)
    @Column(name = "updated")
    private String updated;

    public SchoolProgram() {
    }

    public SchoolProgram(String programCode) {
        this.programCode = programCode;
    }

    public String getProgramCode() {
        return programCode;
    }

    public void setProgramCode(String programCode) {
        this.programCode = programCode;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    public Integer getViewOrder() {
        return viewOrder;
    }

    public void setViewOrder(Integer viewOrder) {
        this.viewOrder = viewOrder;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (programCode != null ? programCode.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SchoolProgram)) {
            return false;
        }
        SchoolProgram other = (SchoolProgram) object;
        if ((this.programCode == null && other.programCode != null) || (this.programCode != null && !this.programCode.equals(other.programCode))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sabonay.timetable.entities.SchoolProgram[ programCode=" + programCode + " ]";
    }
    
}
