package com.sabonay.timetable.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-02-27T15:02:50")
@StaticMetamodel(Student.class)
public class Student_ { 

    public static volatile SingularAttribute<Student, String> studentCategory;
    public static volatile SingularAttribute<Student, String> region;
    public static volatile SingularAttribute<Student, String> classAdmittedTo;
    public static volatile SingularAttribute<Student, String> disabilities;
    public static volatile SingularAttribute<Student, String> surname;
    public static volatile SingularAttribute<Student, String> guardianContactNumber;
    public static volatile SingularAttribute<Student, String> guardianOccupation;
    public static volatile SingularAttribute<Student, String> guardianName;
    public static volatile SingularAttribute<Student, String> updated;
    public static volatile SingularAttribute<Student, String> educationLevel;
    public static volatile SingularAttribute<Student, Date> dateOfbirth;
    public static volatile SingularAttribute<Student, String> programmeOffered;
    public static volatile SingularAttribute<Student, String> gender;
    public static volatile SingularAttribute<Student, String> othernames;
    public static volatile SingularAttribute<Student, String> relationToGuardian;
    public static volatile SingularAttribute<Student, String> hometown;
    public static volatile SingularAttribute<Student, String> studentFullId;
    public static volatile SingularAttribute<Student, String> studentPassword;
    public static volatile SingularAttribute<Student, String> guardianPhysicalAddress;
    public static volatile SingularAttribute<Student, Date> dateOfAdmission;
    public static volatile SingularAttribute<Student, String> currentStatus;
    public static volatile SingularAttribute<Student, String> deleted;
    public static volatile SingularAttribute<Student, String> guardianPostalAddress;
    public static volatile SingularAttribute<Student, String> houseOfResidence;
    public static volatile SingularAttribute<Student, String> schoolNumber;
    public static volatile SingularAttribute<Student, String> lastModifiedBy;
    public static volatile SingularAttribute<Student, Date> lastModifiedDate;
    public static volatile SingularAttribute<Student, String> studentBasicId;
    public static volatile SingularAttribute<Student, String> admittedBy;
    public static volatile SingularAttribute<Student, String> academicYear;

}