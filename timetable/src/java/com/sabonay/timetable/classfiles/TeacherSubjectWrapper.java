/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sabonay.timetable.classfiles;

import java.io.Serializable;

/**
 *
 * @author AMINA
 */
public class TeacherSubjectWrapper implements Serializable {

    private String teacher;
    private String subject;

    public TeacherSubjectWrapper() {
    }

    public TeacherSubjectWrapper(String teacher, String subject) {
        this.teacher = teacher;
        this.subject = subject;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

}
