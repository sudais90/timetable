/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sabonay.timetable.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Amina
 */
@Entity
@Table(name = "exam_paper")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ExamPaper.findAll", query = "SELECT e FROM ExamPaper e"),
    @NamedQuery(name = "ExamPaper.findByExamPaperId", query = "SELECT e FROM ExamPaper e WHERE e.examPaperId = :examPaperId"),
    @NamedQuery(name = "ExamPaper.findByPaperDate", query = "SELECT e FROM ExamPaper e WHERE e.paperDate = :paperDate"),
    @NamedQuery(name = "ExamPaper.findBySubjectCode", query = "SELECT e FROM ExamPaper e WHERE e.subjectCode = :subjectCode"),
    @NamedQuery(name = "ExamPaper.findByExamRoom", query = "SELECT e FROM ExamPaper e WHERE e.examRoom = :examRoom"),
    @NamedQuery(name = "ExamPaper.findByExamBlock", query = "SELECT e FROM ExamPaper e WHERE e.examBlock = :examBlock"),
    @NamedQuery(name = "ExamPaper.findByPaperSession", query = "SELECT e FROM ExamPaper e WHERE e.paperSession = :paperSession"),
    @NamedQuery(name = "ExamPaper.findByAcademicTermId", query = "SELECT e FROM ExamPaper e WHERE e.academicTermId = :academicTermId"),
    @NamedQuery(name = "ExamPaper.findByInvigilator", query = "SELECT e FROM ExamPaper e WHERE e.invigilator = :invigilator"),
    @NamedQuery(name = "ExamPaper.findBySchoolNumber", query = "SELECT e FROM ExamPaper e WHERE e.schoolNumber = :schoolNumber"),
    @NamedQuery(name = "ExamPaper.findByDeleted", query = "SELECT e FROM ExamPaper e WHERE e.deleted = :deleted"),
    @NamedQuery(name = "ExamPaper.findByUpdated", query = "SELECT e FROM ExamPaper e WHERE e.updated = :updated")})
public class ExamPaper implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "exam_paper_id")
    private String examPaperId;
    @Column(name = "paper_date")
    @Temporal(TemporalType.DATE)
    private Date paperDate;
    @Size(max = 200)
    @Column(name = "subject_code")
    private String subjectCode;
    @Size(max = 200)
    @Column(name = "exam_room")
    private String examRoom;
    @Size(max = 200)
    @Column(name = "exam_block")
    private String examBlock;
    @Size(max = 200)
    @Column(name = "paper_session")
    private String paperSession;
    @Size(max = 200)
    @Column(name = "academic_term_id")
    private String academicTermId;
    @Size(max = 200)
    @Column(name = "invigilator")
    private String invigilator;
    @Size(max = 200)
    @Column(name = "school_number")
    private String schoolNumber;
    @Size(max = 10)
    @Column(name = "deleted")
    private String deleted;
    @Size(max = 10)
    @Column(name = "updated")
    private String updated;

    public ExamPaper() {
    }

    public ExamPaper(String examPaperId) {
        this.examPaperId = examPaperId;
    }

    public String getExamPaperId() {
        return examPaperId;
    }

    public void setExamPaperId(String examPaperId) {
        this.examPaperId = examPaperId;
    }

    public Date getPaperDate() {
        return paperDate;
    }

    public void setPaperDate(Date paperDate) {
        this.paperDate = paperDate;
    }

    public String getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }

    public String getExamRoom() {
        return examRoom;
    }

    public void setExamRoom(String examRoom) {
        this.examRoom = examRoom;
    }

    public String getExamBlock() {
        return examBlock;
    }

    public void setExamBlock(String examBlock) {
        this.examBlock = examBlock;
    }

    public String getPaperSession() {
        return paperSession;
    }

    public void setPaperSession(String paperSession) {
        this.paperSession = paperSession;
    }

    public String getAcademicTermId() {
        return academicTermId;
    }

    public void setAcademicTermId(String academicTermId) {
        this.academicTermId = academicTermId;
    }

    public String getInvigilator() {
        return invigilator;
    }

    public void setInvigilator(String invigilator) {
        this.invigilator = invigilator;
    }

    public String getSchoolNumber() {
        return schoolNumber;
    }

    public void setSchoolNumber(String schoolNumber) {
        this.schoolNumber = schoolNumber;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (examPaperId != null ? examPaperId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ExamPaper)) {
            return false;
        }
        ExamPaper other = (ExamPaper) object;
        if ((this.examPaperId == null && other.examPaperId != null) || (this.examPaperId != null && !this.examPaperId.equals(other.examPaperId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sabonay.timetable.entities.ExamPaper[ examPaperId=" + examPaperId + " ]";
    }
    
}
