package com.sabonay.timetable.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-02-27T15:02:50")
@StaticMetamodel(SchoolSubject.class)
public class SchoolSubject_ { 

    public static volatile SingularAttribute<SchoolSubject, String> subjectName;
    public static volatile SingularAttribute<SchoolSubject, String> subjectCategory;
    public static volatile SingularAttribute<SchoolSubject, String> updated;
    public static volatile SingularAttribute<SchoolSubject, String> subjectInitials;
    public static volatile SingularAttribute<SchoolSubject, String> subjectCode;
    public static volatile SingularAttribute<SchoolSubject, String> deleted;
    public static volatile SingularAttribute<SchoolSubject, Boolean> selectedSubject;

}