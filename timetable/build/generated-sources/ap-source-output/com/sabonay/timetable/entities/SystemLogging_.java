package com.sabonay.timetable.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-02-27T15:02:50")
@StaticMetamodel(SystemLogging.class)
public class SystemLogging_ { 

    public static volatile SingularAttribute<SystemLogging, String> id;
    public static volatile SingularAttribute<SystemLogging, String> machineName;
    public static volatile SingularAttribute<SystemLogging, Date> capturedTime;
    public static volatile SingularAttribute<SystemLogging, String> updated;
    public static volatile SingularAttribute<SystemLogging, String> schoolNumber;
    public static volatile SingularAttribute<SystemLogging, String> systemUser;
    public static volatile SingularAttribute<SystemLogging, String> deleted;
    public static volatile SingularAttribute<SystemLogging, String> academicTerm;
    public static volatile SingularAttribute<SystemLogging, String> actions;

}