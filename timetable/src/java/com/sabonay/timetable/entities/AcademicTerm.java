/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sabonay.timetable.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Amina
 */
@Entity
@Table(name = "academic_term")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AcademicTerm.findAll", query = "SELECT a FROM AcademicTerm a"),
    @NamedQuery(name = "AcademicTerm.findByAcademicTermId", query = "SELECT a FROM AcademicTerm a WHERE a.academicTermId = :academicTermId"),
    @NamedQuery(name = "AcademicTerm.findByAcademicYearId", query = "SELECT a FROM AcademicTerm a WHERE a.academicYearId = :academicYearId"),
    @NamedQuery(name = "AcademicTerm.findByBeginDate", query = "SELECT a FROM AcademicTerm a WHERE a.beginDate = :beginDate"),
    @NamedQuery(name = "AcademicTerm.findByEndDate", query = "SELECT a FROM AcademicTerm a WHERE a.endDate = :endDate"),
    @NamedQuery(name = "AcademicTerm.findByExamBeginDate", query = "SELECT a FROM AcademicTerm a WHERE a.examBeginDate = :examBeginDate"),
    @NamedQuery(name = "AcademicTerm.findByExamEndDate", query = "SELECT a FROM AcademicTerm a WHERE a.examEndDate = :examEndDate"),
    @NamedQuery(name = "AcademicTerm.findBySchoolNumber", query = "SELECT a FROM AcademicTerm a WHERE a.schoolNumber = :schoolNumber"),
    @NamedQuery(name = "AcademicTerm.findByDeleted", query = "SELECT a FROM AcademicTerm a WHERE a.deleted = :deleted"),
    @NamedQuery(name = "AcademicTerm.findByUpdated", query = "SELECT a FROM AcademicTerm a WHERE a.updated = :updated"),
    @NamedQuery(name = "AcademicTerm.findBySchoolTerm", query = "SELECT a FROM AcademicTerm a WHERE a.schoolTerm = :schoolTerm")})
public class AcademicTerm implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "academic_term_id")
    private String academicTermId;
    @Size(max = 100)
    @Column(name = "academic_year_id")
    private String academicYearId;
    @Column(name = "begin_date")
    @Temporal(TemporalType.DATE)
    private Date beginDate;
    @Column(name = "end_Date")
    @Temporal(TemporalType.DATE)
    private Date endDate;
    @Column(name = "exam_begin_date")
    @Temporal(TemporalType.DATE)
    private Date examBeginDate;
    @Column(name = "exam_end_date")
    @Temporal(TemporalType.DATE)
    private Date examEndDate;
    @Size(max = 50)
    @Column(name = "school_number")
    private String schoolNumber;
    @Size(max = 10)
    @Column(name = "deleted")
    private String deleted;
    @Size(max = 10)
    @Column(name = "updated")
    private String updated;
    @Size(max = 45)
    @Column(name = "school_term")
    private String schoolTerm;

    public AcademicTerm() {
    }

    public AcademicTerm(String academicTermId) {
        this.academicTermId = academicTermId;
    }

    public String getAcademicTermId() {
        return academicTermId;
    }

    public void setAcademicTermId(String academicTermId) {
        this.academicTermId = academicTermId;
    }

    public String getAcademicYearId() {
        return academicYearId;
    }

    public void setAcademicYearId(String academicYearId) {
        this.academicYearId = academicYearId;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getExamBeginDate() {
        return examBeginDate;
    }

    public void setExamBeginDate(Date examBeginDate) {
        this.examBeginDate = examBeginDate;
    }

    public Date getExamEndDate() {
        return examEndDate;
    }

    public void setExamEndDate(Date examEndDate) {
        this.examEndDate = examEndDate;
    }

    public String getSchoolNumber() {
        return schoolNumber;
    }

    public void setSchoolNumber(String schoolNumber) {
        this.schoolNumber = schoolNumber;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getSchoolTerm() {
        return schoolTerm;
    }

    public void setSchoolTerm(String schoolTerm) {
        this.schoolTerm = schoolTerm;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (academicTermId != null ? academicTermId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AcademicTerm)) {
            return false;
        }
        AcademicTerm other = (AcademicTerm) object;
        if ((this.academicTermId == null && other.academicTermId != null) || (this.academicTermId != null && !this.academicTermId.equals(other.academicTermId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sabonay.timetable.entities.AcademicTerm[ academicTermId=" + academicTermId + " ]";
    }
    
}
