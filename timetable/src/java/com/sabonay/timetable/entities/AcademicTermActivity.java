/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sabonay.timetable.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Amina
 */
@Entity
@Table(name = "academic_term_activity")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AcademicTermActivity.findAll", query = "SELECT a FROM AcademicTermActivity a"),
    @NamedQuery(name = "AcademicTermActivity.findByTermActivitiesId", query = "SELECT a FROM AcademicTermActivity a WHERE a.termActivitiesId = :termActivitiesId"),
    @NamedQuery(name = "AcademicTermActivity.findByAcademicTerm", query = "SELECT a FROM AcademicTermActivity a WHERE a.academicTerm = :academicTerm"),
    @NamedQuery(name = "AcademicTermActivity.findByActivityStartDate", query = "SELECT a FROM AcademicTermActivity a WHERE a.activityStartDate = :activityStartDate"),
    @NamedQuery(name = "AcademicTermActivity.findByActivityEndDate", query = "SELECT a FROM AcademicTermActivity a WHERE a.activityEndDate = :activityEndDate"),
    @NamedQuery(name = "AcademicTermActivity.findByActivityName", query = "SELECT a FROM AcademicTermActivity a WHERE a.activityName = :activityName"),
    @NamedQuery(name = "AcademicTermActivity.findBySchoolNumber", query = "SELECT a FROM AcademicTermActivity a WHERE a.schoolNumber = :schoolNumber"),
    @NamedQuery(name = "AcademicTermActivity.findByDeleted", query = "SELECT a FROM AcademicTermActivity a WHERE a.deleted = :deleted"),
    @NamedQuery(name = "AcademicTermActivity.findByUpdated", query = "SELECT a FROM AcademicTermActivity a WHERE a.updated = :updated")})
public class AcademicTermActivity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "term_activities_id")
    private String termActivitiesId;
    @Size(max = 255)
    @Column(name = "academic_term")
    private String academicTerm;
    @Column(name = "activity_start_date")
    @Temporal(TemporalType.DATE)
    private Date activityStartDate;
    @Column(name = "activity_end_date")
    @Temporal(TemporalType.DATE)
    private Date activityEndDate;
    @Size(max = 255)
    @Column(name = "activity_name")
    private String activityName;
    @Lob
    @Size(max = 65535)
    @Column(name = "activity_notes")
    private String activityNotes;
    @Size(max = 255)
    @Column(name = "school_number")
    private String schoolNumber;
    @Size(max = 5)
    @Column(name = "deleted")
    private String deleted;
    @Size(max = 5)
    @Column(name = "updated")
    private String updated;

    public AcademicTermActivity() {
    }

    public AcademicTermActivity(String termActivitiesId) {
        this.termActivitiesId = termActivitiesId;
    }

    public String getTermActivitiesId() {
        return termActivitiesId;
    }

    public void setTermActivitiesId(String termActivitiesId) {
        this.termActivitiesId = termActivitiesId;
    }

    public String getAcademicTerm() {
        return academicTerm;
    }

    public void setAcademicTerm(String academicTerm) {
        this.academicTerm = academicTerm;
    }

    public Date getActivityStartDate() {
        return activityStartDate;
    }

    public void setActivityStartDate(Date activityStartDate) {
        this.activityStartDate = activityStartDate;
    }

    public Date getActivityEndDate() {
        return activityEndDate;
    }

    public void setActivityEndDate(Date activityEndDate) {
        this.activityEndDate = activityEndDate;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public String getActivityNotes() {
        return activityNotes;
    }

    public void setActivityNotes(String activityNotes) {
        this.activityNotes = activityNotes;
    }

    public String getSchoolNumber() {
        return schoolNumber;
    }

    public void setSchoolNumber(String schoolNumber) {
        this.schoolNumber = schoolNumber;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (termActivitiesId != null ? termActivitiesId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AcademicTermActivity)) {
            return false;
        }
        AcademicTermActivity other = (AcademicTermActivity) object;
        if ((this.termActivitiesId == null && other.termActivitiesId != null) || (this.termActivitiesId != null && !this.termActivitiesId.equals(other.termActivitiesId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sabonay.timetable.entities.AcademicTermActivity[ termActivitiesId=" + termActivitiesId + " ]";
    }
    
}
