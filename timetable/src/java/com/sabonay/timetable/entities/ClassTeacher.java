/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sabonay.timetable.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Amina
 */
@Entity
@Table(name = "class_teacher")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ClassTeacher.findAll", query = "SELECT c FROM ClassTeacher c"),
    @NamedQuery(name = "ClassTeacher.findByClassTeacherId", query = "SELECT c FROM ClassTeacher c WHERE c.classTeacherId = :classTeacherId"),
    @NamedQuery(name = "ClassTeacher.findByAcademicYear", query = "SELECT c FROM ClassTeacher c WHERE c.academicYear = :academicYear"),
    @NamedQuery(name = "ClassTeacher.findBySchoolClass", query = "SELECT c FROM ClassTeacher c WHERE c.schoolClass = :schoolClass"),
    @NamedQuery(name = "ClassTeacher.findByTeacherId", query = "SELECT c FROM ClassTeacher c WHERE c.teacherId = :teacherId"),
    @NamedQuery(name = "ClassTeacher.findBySchoolNumber", query = "SELECT c FROM ClassTeacher c WHERE c.schoolNumber = :schoolNumber"),
    @NamedQuery(name = "ClassTeacher.findByDeleted", query = "SELECT c FROM ClassTeacher c WHERE c.deleted = :deleted"),
    @NamedQuery(name = "ClassTeacher.findByUpdated", query = "SELECT c FROM ClassTeacher c WHERE c.updated = :updated")})
public class ClassTeacher implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "class_teacher_id")
    private String classTeacherId;
    @Size(max = 255)
    @Column(name = "academic_year")
    private String academicYear;
    @Size(max = 255)
    @Column(name = "school_class")
    private String schoolClass;
    @Size(max = 255)
    @Column(name = "teacher_id")
    private String teacherId;
    @Size(max = 255)
    @Column(name = "school_number")
    private String schoolNumber;
    @Size(max = 5)
    @Column(name = "deleted")
    private String deleted;
    @Size(max = 5)
    @Column(name = "updated")
    private String updated;

    public ClassTeacher() {
    }

    public ClassTeacher(String classTeacherId) {
        this.classTeacherId = classTeacherId;
    }

    public String getClassTeacherId() {
        return classTeacherId;
    }

    public void setClassTeacherId(String classTeacherId) {
        this.classTeacherId = classTeacherId;
    }

    public String getAcademicYear() {
        return academicYear;
    }

    public void setAcademicYear(String academicYear) {
        this.academicYear = academicYear;
    }

    public String getSchoolClass() {
        return schoolClass;
    }

    public void setSchoolClass(String schoolClass) {
        this.schoolClass = schoolClass;
    }

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getSchoolNumber() {
        return schoolNumber;
    }

    public void setSchoolNumber(String schoolNumber) {
        this.schoolNumber = schoolNumber;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (classTeacherId != null ? classTeacherId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClassTeacher)) {
            return false;
        }
        ClassTeacher other = (ClassTeacher) object;
        if ((this.classTeacherId == null && other.classTeacherId != null) || (this.classTeacherId != null && !this.classTeacherId.equals(other.classTeacherId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sabonay.timetable.entities.ClassTeacher[ classTeacherId=" + classTeacherId + " ]";
    }
    
}
