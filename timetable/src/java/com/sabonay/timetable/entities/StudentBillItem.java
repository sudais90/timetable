/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sabonay.timetable.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Amina
 */
@Entity
@Table(name = "student_bill_item")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "StudentBillItem.findAll", query = "SELECT s FROM StudentBillItem s"),
    @NamedQuery(name = "StudentBillItem.findByBillItemId", query = "SELECT s FROM StudentBillItem s WHERE s.billItemId = :billItemId"),
    @NamedQuery(name = "StudentBillItem.findByItemName", query = "SELECT s FROM StudentBillItem s WHERE s.itemName = :itemName"),
    @NamedQuery(name = "StudentBillItem.findByItemDescription", query = "SELECT s FROM StudentBillItem s WHERE s.itemDescription = :itemDescription"),
    @NamedQuery(name = "StudentBillItem.findBySchoolNumber", query = "SELECT s FROM StudentBillItem s WHERE s.schoolNumber = :schoolNumber"),
    @NamedQuery(name = "StudentBillItem.findByDeleted", query = "SELECT s FROM StudentBillItem s WHERE s.deleted = :deleted"),
    @NamedQuery(name = "StudentBillItem.findByUpdated", query = "SELECT s FROM StudentBillItem s WHERE s.updated = :updated")})
public class StudentBillItem implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "bill_item_id")
    private String billItemId;
    @Size(max = 255)
    @Column(name = "item_name")
    private String itemName;
    @Size(max = 255)
    @Column(name = "item_description")
    private String itemDescription;
    @Size(max = 255)
    @Column(name = "school_number")
    private String schoolNumber;
    @Size(max = 5)
    @Column(name = "deleted")
    private String deleted;
    @Size(max = 5)
    @Column(name = "updated")
    private String updated;

    public StudentBillItem() {
    }

    public StudentBillItem(String billItemId) {
        this.billItemId = billItemId;
    }

    public String getBillItemId() {
        return billItemId;
    }

    public void setBillItemId(String billItemId) {
        this.billItemId = billItemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public String getSchoolNumber() {
        return schoolNumber;
    }

    public void setSchoolNumber(String schoolNumber) {
        this.schoolNumber = schoolNumber;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (billItemId != null ? billItemId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StudentBillItem)) {
            return false;
        }
        StudentBillItem other = (StudentBillItem) object;
        if ((this.billItemId == null && other.billItemId != null) || (this.billItemId != null && !this.billItemId.equals(other.billItemId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sabonay.timetable.entities.StudentBillItem[ billItemId=" + billItemId + " ]";
    }
    
}
