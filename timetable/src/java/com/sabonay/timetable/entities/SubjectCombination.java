/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sabonay.timetable.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Amina
 */
@Entity
@Table(name = "subject_combination")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SubjectCombination.findAll", query = "SELECT s FROM SubjectCombination s"),
    @NamedQuery(name = "SubjectCombination.findBySubjectCombinationCode", query = "SELECT s FROM SubjectCombination s WHERE s.subjectCombinationCode = :subjectCombinationCode"),
    @NamedQuery(name = "SubjectCombination.findBySubjectCombinationProgramme", query = "SELECT s FROM SubjectCombination s WHERE s.subjectCombinationProgramme = :subjectCombinationProgramme"),
    @NamedQuery(name = "SubjectCombination.findBySubjectCombinationName", query = "SELECT s FROM SubjectCombination s WHERE s.subjectCombinationName = :subjectCombinationName"),
    @NamedQuery(name = "SubjectCombination.findByCombinationShortName", query = "SELECT s FROM SubjectCombination s WHERE s.combinationShortName = :combinationShortName"),
    @NamedQuery(name = "SubjectCombination.findBySubjectsIds", query = "SELECT s FROM SubjectCombination s WHERE s.subjectsIds = :subjectsIds"),
    @NamedQuery(name = "SubjectCombination.findBySchoolNumber", query = "SELECT s FROM SubjectCombination s WHERE s.schoolNumber = :schoolNumber"),
    @NamedQuery(name = "SubjectCombination.findByDeleted", query = "SELECT s FROM SubjectCombination s WHERE s.deleted = :deleted"),
    @NamedQuery(name = "SubjectCombination.findByUpdated", query = "SELECT s FROM SubjectCombination s WHERE s.updated = :updated"),
    @NamedQuery(name = "SubjectCombination.findBySubjectCombinationStatus", query = "SELECT s FROM SubjectCombination s WHERE s.subjectCombinationStatus = :subjectCombinationStatus")})
public class SubjectCombination implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "subject_combination_code")
    private String subjectCombinationCode;
    @Size(max = 255)
    @Column(name = "subject_combination_programme")
    private String subjectCombinationProgramme;
    @Size(max = 255)
    @Column(name = "subject_combination_name")
    private String subjectCombinationName;
    @Size(max = 255)
    @Column(name = "combination_short_name")
    private String combinationShortName;
    @Size(max = 255)
    @Column(name = "subjects_ids")
    private String subjectsIds;
    @Size(max = 50)
    @Column(name = "school_number")
    private String schoolNumber;
    @Size(max = 255)
    @Column(name = "deleted")
    private String deleted;
    @Size(max = 255)
    @Column(name = "updated")
    private String updated;
    @Size(max = 200)
    @Column(name = "subject_combination_status")
    private String subjectCombinationStatus;

    public SubjectCombination() {
    }

    public SubjectCombination(String subjectCombinationCode) {
        this.subjectCombinationCode = subjectCombinationCode;
    }

    public String getSubjectCombinationCode() {
        return subjectCombinationCode;
    }

    public void setSubjectCombinationCode(String subjectCombinationCode) {
        this.subjectCombinationCode = subjectCombinationCode;
    }

    public String getSubjectCombinationProgramme() {
        return subjectCombinationProgramme;
    }

    public void setSubjectCombinationProgramme(String subjectCombinationProgramme) {
        this.subjectCombinationProgramme = subjectCombinationProgramme;
    }

    public String getSubjectCombinationName() {
        return subjectCombinationName;
    }

    public void setSubjectCombinationName(String subjectCombinationName) {
        this.subjectCombinationName = subjectCombinationName;
    }

    public String getCombinationShortName() {
        return combinationShortName;
    }

    public void setCombinationShortName(String combinationShortName) {
        this.combinationShortName = combinationShortName;
    }

    public String getSubjectsIds() {
        return subjectsIds;
    }

    public void setSubjectsIds(String subjectsIds) {
        this.subjectsIds = subjectsIds;
    }

    public String getSchoolNumber() {
        return schoolNumber;
    }

    public void setSchoolNumber(String schoolNumber) {
        this.schoolNumber = schoolNumber;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getSubjectCombinationStatus() {
        return subjectCombinationStatus;
    }

    public void setSubjectCombinationStatus(String subjectCombinationStatus) {
        this.subjectCombinationStatus = subjectCombinationStatus;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (subjectCombinationCode != null ? subjectCombinationCode.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SubjectCombination)) {
            return false;
        }
        SubjectCombination other = (SubjectCombination) object;
        if ((this.subjectCombinationCode == null && other.subjectCombinationCode != null) || (this.subjectCombinationCode != null && !this.subjectCombinationCode.equals(other.subjectCombinationCode))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sabonay.timetable.entities.SubjectCombination[ subjectCombinationCode=" + subjectCombinationCode + " ]";
    }
    
}
