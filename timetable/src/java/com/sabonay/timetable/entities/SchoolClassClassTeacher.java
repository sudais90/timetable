/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sabonay.timetable.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Amina
 */
@Entity
@Table(name = "school_class_class_teacher")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SchoolClassClassTeacher.findAll", query = "SELECT s FROM SchoolClassClassTeacher s"),
    @NamedQuery(name = "SchoolClassClassTeacher.findBySchoolClassTeacherId", query = "SELECT s FROM SchoolClassClassTeacher s WHERE s.schoolClassTeacherId = :schoolClassTeacherId"),
    @NamedQuery(name = "SchoolClassClassTeacher.findByClassTeacherId", query = "SELECT s FROM SchoolClassClassTeacher s WHERE s.classTeacherId = :classTeacherId"),
    @NamedQuery(name = "SchoolClassClassTeacher.findBySchoolClassId", query = "SELECT s FROM SchoolClassClassTeacher s WHERE s.schoolClassId = :schoolClassId"),
    @NamedQuery(name = "SchoolClassClassTeacher.findByUpdated", query = "SELECT s FROM SchoolClassClassTeacher s WHERE s.updated = :updated"),
    @NamedQuery(name = "SchoolClassClassTeacher.findByDeleted", query = "SELECT s FROM SchoolClassClassTeacher s WHERE s.deleted = :deleted")})
public class SchoolClassClassTeacher implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "school_class_teacher_id")
    private String schoolClassTeacherId;
    @Size(max = 50)
    @Column(name = "class_teacher_id")
    private String classTeacherId;
    @Size(max = 50)
    @Column(name = "school_class_id")
    private String schoolClassId;
    @Size(max = 10)
    @Column(name = "updated")
    private String updated;
    @Size(max = 10)
    @Column(name = "deleted")
    private String deleted;

    public SchoolClassClassTeacher() {
    }

    public SchoolClassClassTeacher(String schoolClassTeacherId) {
        this.schoolClassTeacherId = schoolClassTeacherId;
    }

    public String getSchoolClassTeacherId() {
        return schoolClassTeacherId;
    }

    public void setSchoolClassTeacherId(String schoolClassTeacherId) {
        this.schoolClassTeacherId = schoolClassTeacherId;
    }

    public String getClassTeacherId() {
        return classTeacherId;
    }

    public void setClassTeacherId(String classTeacherId) {
        this.classTeacherId = classTeacherId;
    }

    public String getSchoolClassId() {
        return schoolClassId;
    }

    public void setSchoolClassId(String schoolClassId) {
        this.schoolClassId = schoolClassId;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (schoolClassTeacherId != null ? schoolClassTeacherId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SchoolClassClassTeacher)) {
            return false;
        }
        SchoolClassClassTeacher other = (SchoolClassClassTeacher) object;
        if ((this.schoolClassTeacherId == null && other.schoolClassTeacherId != null) || (this.schoolClassTeacherId != null && !this.schoolClassTeacherId.equals(other.schoolClassTeacherId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sabonay.timetable.entities.SchoolClassClassTeacher[ schoolClassTeacherId=" + schoolClassTeacherId + " ]";
    }
    
}
