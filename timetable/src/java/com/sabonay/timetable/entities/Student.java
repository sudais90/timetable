/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sabonay.timetable.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Amina
 */
@Entity
@Table(name = "student")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Student.findAll", query = "SELECT s FROM Student s"),
    @NamedQuery(name = "Student.findByStudentBasicId", query = "SELECT s FROM Student s WHERE s.studentBasicId = :studentBasicId"),
    @NamedQuery(name = "Student.findByStudentFullId", query = "SELECT s FROM Student s WHERE s.studentFullId = :studentFullId"),
    @NamedQuery(name = "Student.findBySurname", query = "SELECT s FROM Student s WHERE s.surname = :surname"),
    @NamedQuery(name = "Student.findByOthernames", query = "SELECT s FROM Student s WHERE s.othernames = :othernames"),
    @NamedQuery(name = "Student.findByGender", query = "SELECT s FROM Student s WHERE s.gender = :gender"),
    @NamedQuery(name = "Student.findByDateOfbirth", query = "SELECT s FROM Student s WHERE s.dateOfbirth = :dateOfbirth"),
    @NamedQuery(name = "Student.findByDisabilities", query = "SELECT s FROM Student s WHERE s.disabilities = :disabilities"),
    @NamedQuery(name = "Student.findByEducationLevel", query = "SELECT s FROM Student s WHERE s.educationLevel = :educationLevel"),
    @NamedQuery(name = "Student.findByHometown", query = "SELECT s FROM Student s WHERE s.hometown = :hometown"),
    @NamedQuery(name = "Student.findByDateOfAdmission", query = "SELECT s FROM Student s WHERE s.dateOfAdmission = :dateOfAdmission"),
    @NamedQuery(name = "Student.findByProgrammeOffered", query = "SELECT s FROM Student s WHERE s.programmeOffered = :programmeOffered"),
    @NamedQuery(name = "Student.findByClassAdmittedTo", query = "SELECT s FROM Student s WHERE s.classAdmittedTo = :classAdmittedTo"),
    @NamedQuery(name = "Student.findByStudentCategory", query = "SELECT s FROM Student s WHERE s.studentCategory = :studentCategory"),
    @NamedQuery(name = "Student.findByRelationToGuardian", query = "SELECT s FROM Student s WHERE s.relationToGuardian = :relationToGuardian"),
    @NamedQuery(name = "Student.findByHouseOfResidence", query = "SELECT s FROM Student s WHERE s.houseOfResidence = :houseOfResidence"),
    @NamedQuery(name = "Student.findBySchoolNumber", query = "SELECT s FROM Student s WHERE s.schoolNumber = :schoolNumber"),
    @NamedQuery(name = "Student.findByCurrentStatus", query = "SELECT s FROM Student s WHERE s.currentStatus = :currentStatus"),
    @NamedQuery(name = "Student.findByDeleted", query = "SELECT s FROM Student s WHERE s.deleted = :deleted"),
    @NamedQuery(name = "Student.findByUpdated", query = "SELECT s FROM Student s WHERE s.updated = :updated"),
    @NamedQuery(name = "Student.findByGuardianName", query = "SELECT s FROM Student s WHERE s.guardianName = :guardianName"),
    @NamedQuery(name = "Student.findByGuardianOccupation", query = "SELECT s FROM Student s WHERE s.guardianOccupation = :guardianOccupation"),
    @NamedQuery(name = "Student.findByGuardianContactNumber", query = "SELECT s FROM Student s WHERE s.guardianContactNumber = :guardianContactNumber"),
    @NamedQuery(name = "Student.findByGuardianPostalAddress", query = "SELECT s FROM Student s WHERE s.guardianPostalAddress = :guardianPostalAddress"),
    @NamedQuery(name = "Student.findByGuardianPhysicalAddress", query = "SELECT s FROM Student s WHERE s.guardianPhysicalAddress = :guardianPhysicalAddress"),
    @NamedQuery(name = "Student.findByRegion", query = "SELECT s FROM Student s WHERE s.region = :region"),
    @NamedQuery(name = "Student.findByStudentPassword", query = "SELECT s FROM Student s WHERE s.studentPassword = :studentPassword"),
    @NamedQuery(name = "Student.findByLastModifiedBy", query = "SELECT s FROM Student s WHERE s.lastModifiedBy = :lastModifiedBy"),
    @NamedQuery(name = "Student.findByLastModifiedDate", query = "SELECT s FROM Student s WHERE s.lastModifiedDate = :lastModifiedDate"),
    @NamedQuery(name = "Student.findByAdmittedBy", query = "SELECT s FROM Student s WHERE s.admittedBy = :admittedBy"),
    @NamedQuery(name = "Student.findByAcademicYear", query = "SELECT s FROM Student s WHERE s.academicYear = :academicYear")})
public class Student implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "student_basic_id")
    private String studentBasicId;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "student_full_id")
    private String studentFullId;
    @Size(max = 255)
    @Column(name = "surname")
    private String surname;
    @Size(max = 255)
    @Column(name = "othernames")
    private String othernames;
    @Size(max = 255)
    @Column(name = "gender")
    private String gender;
    @Column(name = "date_Of_birth")
    @Temporal(TemporalType.DATE)
    private Date dateOfbirth;
    @Size(max = 255)
    @Column(name = "disabilities")
    private String disabilities;
    @Size(max = 255)
    @Column(name = "education_level")
    private String educationLevel;
    @Size(max = 255)
    @Column(name = "hometown")
    private String hometown;
    @Column(name = "date_of_admission")
    @Temporal(TemporalType.DATE)
    private Date dateOfAdmission;
    @Size(max = 255)
    @Column(name = "programme_offered")
    private String programmeOffered;
    @Size(max = 255)
    @Column(name = "class_admitted_to")
    private String classAdmittedTo;
    @Size(max = 255)
    @Column(name = "student_category")
    private String studentCategory;
    @Size(max = 255)
    @Column(name = "relation_to_guardian")
    private String relationToGuardian;
    @Size(max = 255)
    @Column(name = "house_of_residence")
    private String houseOfResidence;
    @Size(max = 50)
    @Column(name = "school_number")
    private String schoolNumber;
    @Size(max = 50)
    @Column(name = "current_status")
    private String currentStatus;
    @Size(max = 255)
    @Column(name = "deleted")
    private String deleted;
    @Size(max = 255)
    @Column(name = "updated")
    private String updated;
    @Size(max = 255)
    @Column(name = "guardian_name")
    private String guardianName;
    @Size(max = 255)
    @Column(name = "guardian_occupation")
    private String guardianOccupation;
    @Size(max = 255)
    @Column(name = "guardian_contact_number")
    private String guardianContactNumber;
    @Size(max = 255)
    @Column(name = "guardian_postal_address")
    private String guardianPostalAddress;
    @Size(max = 255)
    @Column(name = "guardian_physical_address")
    private String guardianPhysicalAddress;
    @Size(max = 45)
    @Column(name = "region")
    private String region;
    @Size(max = 200)
    @Column(name = "student_password")
    private String studentPassword;
    @Size(max = 200)
    @Column(name = "last_modified_by")
    private String lastModifiedBy;
    @Column(name = "last_modified_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;
    @Size(max = 255)
    @Column(name = "admitted_by")
    private String admittedBy;
    @Size(max = 255)
    @Column(name = "academic_year")
    private String academicYear;

    public Student() {
    }

    public Student(String studentFullId) {
        this.studentFullId = studentFullId;
    }

    public Student(String studentFullId, String studentBasicId) {
        this.studentFullId = studentFullId;
        this.studentBasicId = studentBasicId;
    }

    public String getStudentBasicId() {
        return studentBasicId;
    }

    public void setStudentBasicId(String studentBasicId) {
        this.studentBasicId = studentBasicId;
    }

    public String getStudentFullId() {
        return studentFullId;
    }

    public void setStudentFullId(String studentFullId) {
        this.studentFullId = studentFullId;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getOthernames() {
        return othernames;
    }

    public void setOthernames(String othernames) {
        this.othernames = othernames;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getDateOfbirth() {
        return dateOfbirth;
    }

    public void setDateOfbirth(Date dateOfbirth) {
        this.dateOfbirth = dateOfbirth;
    }

    public String getDisabilities() {
        return disabilities;
    }

    public void setDisabilities(String disabilities) {
        this.disabilities = disabilities;
    }

    public String getEducationLevel() {
        return educationLevel;
    }

    public void setEducationLevel(String educationLevel) {
        this.educationLevel = educationLevel;
    }

    public String getHometown() {
        return hometown;
    }

    public void setHometown(String hometown) {
        this.hometown = hometown;
    }

    public Date getDateOfAdmission() {
        return dateOfAdmission;
    }

    public void setDateOfAdmission(Date dateOfAdmission) {
        this.dateOfAdmission = dateOfAdmission;
    }

    public String getProgrammeOffered() {
        return programmeOffered;
    }

    public void setProgrammeOffered(String programmeOffered) {
        this.programmeOffered = programmeOffered;
    }

    public String getClassAdmittedTo() {
        return classAdmittedTo;
    }

    public void setClassAdmittedTo(String classAdmittedTo) {
        this.classAdmittedTo = classAdmittedTo;
    }

    public String getStudentCategory() {
        return studentCategory;
    }

    public void setStudentCategory(String studentCategory) {
        this.studentCategory = studentCategory;
    }

    public String getRelationToGuardian() {
        return relationToGuardian;
    }

    public void setRelationToGuardian(String relationToGuardian) {
        this.relationToGuardian = relationToGuardian;
    }

    public String getHouseOfResidence() {
        return houseOfResidence;
    }

    public void setHouseOfResidence(String houseOfResidence) {
        this.houseOfResidence = houseOfResidence;
    }

    public String getSchoolNumber() {
        return schoolNumber;
    }

    public void setSchoolNumber(String schoolNumber) {
        this.schoolNumber = schoolNumber;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getGuardianName() {
        return guardianName;
    }

    public void setGuardianName(String guardianName) {
        this.guardianName = guardianName;
    }

    public String getGuardianOccupation() {
        return guardianOccupation;
    }

    public void setGuardianOccupation(String guardianOccupation) {
        this.guardianOccupation = guardianOccupation;
    }

    public String getGuardianContactNumber() {
        return guardianContactNumber;
    }

    public void setGuardianContactNumber(String guardianContactNumber) {
        this.guardianContactNumber = guardianContactNumber;
    }

    public String getGuardianPostalAddress() {
        return guardianPostalAddress;
    }

    public void setGuardianPostalAddress(String guardianPostalAddress) {
        this.guardianPostalAddress = guardianPostalAddress;
    }

    public String getGuardianPhysicalAddress() {
        return guardianPhysicalAddress;
    }

    public void setGuardianPhysicalAddress(String guardianPhysicalAddress) {
        this.guardianPhysicalAddress = guardianPhysicalAddress;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getStudentPassword() {
        return studentPassword;
    }

    public void setStudentPassword(String studentPassword) {
        this.studentPassword = studentPassword;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getAdmittedBy() {
        return admittedBy;
    }

    public void setAdmittedBy(String admittedBy) {
        this.admittedBy = admittedBy;
    }

    public String getAcademicYear() {
        return academicYear;
    }

    public void setAcademicYear(String academicYear) {
        this.academicYear = academicYear;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (studentFullId != null ? studentFullId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Student)) {
            return false;
        }
        Student other = (Student) object;
        if ((this.studentFullId == null && other.studentFullId != null) || (this.studentFullId != null && !this.studentFullId.equals(other.studentFullId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sabonay.timetable.entities.Student[ studentFullId=" + studentFullId + " ]";
    }
    
}
