package com.sabonay.timetable.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-02-27T15:02:50")
@StaticMetamodel(UserAccount.class)
public class UserAccount_ { 

    public static volatile SingularAttribute<UserAccount, String> username;
    public static volatile SingularAttribute<UserAccount, String> staffId;
    public static volatile SingularAttribute<UserAccount, String> userCategory;
    public static volatile SingularAttribute<UserAccount, String> updated;
    public static volatile SingularAttribute<UserAccount, String> schoolNumber;
    public static volatile SingularAttribute<UserAccount, String> userPassword;
    public static volatile SingularAttribute<UserAccount, String> userAccountId;
    public static volatile SingularAttribute<UserAccount, String> deleted;
    public static volatile SingularAttribute<UserAccount, String> userAccess;

}