package com.sabonay.timetable.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-02-27T15:02:50")
@StaticMetamodel(ClassTeacher.class)
public class ClassTeacher_ { 

    public static volatile SingularAttribute<ClassTeacher, String> schoolClass;
    public static volatile SingularAttribute<ClassTeacher, String> updated;
    public static volatile SingularAttribute<ClassTeacher, String> schoolNumber;
    public static volatile SingularAttribute<ClassTeacher, String> classTeacherId;
    public static volatile SingularAttribute<ClassTeacher, String> deleted;
    public static volatile SingularAttribute<ClassTeacher, String> teacherId;
    public static volatile SingularAttribute<ClassTeacher, String> academicYear;

}