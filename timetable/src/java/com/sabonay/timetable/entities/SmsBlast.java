/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sabonay.timetable.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Amina
 */
@Entity
@Table(name = "sms_blast")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SmsBlast.findAll", query = "SELECT s FROM SmsBlast s"),
    @NamedQuery(name = "SmsBlast.findBySmsBlastId", query = "SELECT s FROM SmsBlast s WHERE s.smsBlastId = :smsBlastId"),
    @NamedQuery(name = "SmsBlast.findBySmsSubject", query = "SELECT s FROM SmsBlast s WHERE s.smsSubject = :smsSubject"),
    @NamedQuery(name = "SmsBlast.findBySmsText", query = "SELECT s FROM SmsBlast s WHERE s.smsText = :smsText"),
    @NamedQuery(name = "SmsBlast.findBySmsSendDate", query = "SELECT s FROM SmsBlast s WHERE s.smsSendDate = :smsSendDate"),
    @NamedQuery(name = "SmsBlast.findByContactGroup", query = "SELECT s FROM SmsBlast s WHERE s.contactGroup = :contactGroup"),
    @NamedQuery(name = "SmsBlast.findByContactGroupValue", query = "SELECT s FROM SmsBlast s WHERE s.contactGroupValue = :contactGroupValue"),
    @NamedQuery(name = "SmsBlast.findByLastModifiedDate", query = "SELECT s FROM SmsBlast s WHERE s.lastModifiedDate = :lastModifiedDate"),
    @NamedQuery(name = "SmsBlast.findByLastModifiedBy", query = "SELECT s FROM SmsBlast s WHERE s.lastModifiedBy = :lastModifiedBy"),
    @NamedQuery(name = "SmsBlast.findByDeleted", query = "SELECT s FROM SmsBlast s WHERE s.deleted = :deleted"),
    @NamedQuery(name = "SmsBlast.findByUpdated", query = "SELECT s FROM SmsBlast s WHERE s.updated = :updated")})
public class SmsBlast implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "sms_blast_id")
    private String smsBlastId;
    @Size(max = 200)
    @Column(name = "sms_subject")
    private String smsSubject;
    @Size(max = 255)
    @Column(name = "sms_text")
    private String smsText;
    @Column(name = "sms_send_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date smsSendDate;
    @Size(max = 45)
    @Column(name = "contact_group")
    private String contactGroup;
    @Size(max = 45)
    @Column(name = "contact_group_value")
    private String contactGroupValue;
    @Column(name = "last_modified_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;
    @Size(max = 45)
    @Column(name = "last_modified_by")
    private String lastModifiedBy;
    @Size(max = 45)
    @Column(name = "deleted")
    private String deleted;
    @Size(max = 45)
    @Column(name = "updated")
    private String updated;

    public SmsBlast() {
    }

    public SmsBlast(String smsBlastId) {
        this.smsBlastId = smsBlastId;
    }

    public String getSmsBlastId() {
        return smsBlastId;
    }

    public void setSmsBlastId(String smsBlastId) {
        this.smsBlastId = smsBlastId;
    }

    public String getSmsSubject() {
        return smsSubject;
    }

    public void setSmsSubject(String smsSubject) {
        this.smsSubject = smsSubject;
    }

    public String getSmsText() {
        return smsText;
    }

    public void setSmsText(String smsText) {
        this.smsText = smsText;
    }

    public Date getSmsSendDate() {
        return smsSendDate;
    }

    public void setSmsSendDate(Date smsSendDate) {
        this.smsSendDate = smsSendDate;
    }

    public String getContactGroup() {
        return contactGroup;
    }

    public void setContactGroup(String contactGroup) {
        this.contactGroup = contactGroup;
    }

    public String getContactGroupValue() {
        return contactGroupValue;
    }

    public void setContactGroupValue(String contactGroupValue) {
        this.contactGroupValue = contactGroupValue;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (smsBlastId != null ? smsBlastId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SmsBlast)) {
            return false;
        }
        SmsBlast other = (SmsBlast) object;
        if ((this.smsBlastId == null && other.smsBlastId != null) || (this.smsBlastId != null && !this.smsBlastId.equals(other.smsBlastId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sabonay.timetable.entities.SmsBlast[ smsBlastId=" + smsBlastId + " ]";
    }
    
}
