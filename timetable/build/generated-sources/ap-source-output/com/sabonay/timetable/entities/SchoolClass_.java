package com.sabonay.timetable.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-02-27T15:02:50")
@StaticMetamodel(SchoolClass.class)
public class SchoolClass_ { 

    public static volatile SingularAttribute<SchoolClass, String> updated;
    public static volatile SingularAttribute<SchoolClass, String> schoolNumber;
    public static volatile SingularAttribute<SchoolClass, String> classCode;
    public static volatile SingularAttribute<SchoolClass, String> lastModifiedBy;
    public static volatile SingularAttribute<SchoolClass, Date> lastModifiedDate;
    public static volatile SingularAttribute<SchoolClass, String> className;
    public static volatile SingularAttribute<SchoolClass, String> educationalLevel;
    public static volatile SingularAttribute<SchoolClass, String> deleted;
    public static volatile SingularAttribute<SchoolClass, String> classProgrammeCode;

}