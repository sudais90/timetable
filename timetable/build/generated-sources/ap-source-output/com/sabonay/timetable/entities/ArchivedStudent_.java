package com.sabonay.timetable.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-02-27T15:02:50")
@StaticMetamodel(ArchivedStudent.class)
public class ArchivedStudent_ { 

    public static volatile SingularAttribute<ArchivedStudent, String> studentCategory;
    public static volatile SingularAttribute<ArchivedStudent, String> region;
    public static volatile SingularAttribute<ArchivedStudent, String> classAdmittedTo;
    public static volatile SingularAttribute<ArchivedStudent, String> disabilities;
    public static volatile SingularAttribute<ArchivedStudent, String> surname;
    public static volatile SingularAttribute<ArchivedStudent, String> guardianContactNumber;
    public static volatile SingularAttribute<ArchivedStudent, String> guardianOccupation;
    public static volatile SingularAttribute<ArchivedStudent, String> guardianName;
    public static volatile SingularAttribute<ArchivedStudent, String> updated;
    public static volatile SingularAttribute<ArchivedStudent, String> educationLevel;
    public static volatile SingularAttribute<ArchivedStudent, Date> dateOfbirth;
    public static volatile SingularAttribute<ArchivedStudent, String> programmeOffered;
    public static volatile SingularAttribute<ArchivedStudent, String> gender;
    public static volatile SingularAttribute<ArchivedStudent, String> othernames;
    public static volatile SingularAttribute<ArchivedStudent, String> relationToGuardian;
    public static volatile SingularAttribute<ArchivedStudent, String> hometown;
    public static volatile SingularAttribute<ArchivedStudent, String> studentFullId;
    public static volatile SingularAttribute<ArchivedStudent, String> studentPassword;
    public static volatile SingularAttribute<ArchivedStudent, String> guardianPhysicalAddress;
    public static volatile SingularAttribute<ArchivedStudent, Date> dateOfAdmission;
    public static volatile SingularAttribute<ArchivedStudent, String> currentStatus;
    public static volatile SingularAttribute<ArchivedStudent, String> deleted;
    public static volatile SingularAttribute<ArchivedStudent, String> guardianPostalAddress;
    public static volatile SingularAttribute<ArchivedStudent, String> houseOfResidence;
    public static volatile SingularAttribute<ArchivedStudent, String> schoolNumber;
    public static volatile SingularAttribute<ArchivedStudent, String> lastModifiedBy;
    public static volatile SingularAttribute<ArchivedStudent, Date> lastModifiedDate;
    public static volatile SingularAttribute<ArchivedStudent, String> studentBasicId;

}