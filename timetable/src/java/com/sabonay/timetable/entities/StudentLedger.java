/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sabonay.timetable.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Amina
 */
@Entity
@Table(name = "student_ledger")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "StudentLedger.findAll", query = "SELECT s FROM StudentLedger s"),
    @NamedQuery(name = "StudentLedger.findByStudentLedgerId", query = "SELECT s FROM StudentLedger s WHERE s.studentLedgerId = :studentLedgerId"),
    @NamedQuery(name = "StudentLedger.findByStudentId", query = "SELECT s FROM StudentLedger s WHERE s.studentId = :studentId"),
    @NamedQuery(name = "StudentLedger.findByTermOfEntry", query = "SELECT s FROM StudentLedger s WHERE s.termOfEntry = :termOfEntry"),
    @NamedQuery(name = "StudentLedger.findByDateOfEntry", query = "SELECT s FROM StudentLedger s WHERE s.dateOfEntry = :dateOfEntry"),
    @NamedQuery(name = "StudentLedger.findByDateOfPayment", query = "SELECT s FROM StudentLedger s WHERE s.dateOfPayment = :dateOfPayment"),
    @NamedQuery(name = "StudentLedger.findByTypeOfEntry", query = "SELECT s FROM StudentLedger s WHERE s.typeOfEntry = :typeOfEntry"),
    @NamedQuery(name = "StudentLedger.findByAmountInvolved", query = "SELECT s FROM StudentLedger s WHERE s.amountInvolved = :amountInvolved"),
    @NamedQuery(name = "StudentLedger.findByReceiptNumber", query = "SELECT s FROM StudentLedger s WHERE s.receiptNumber = :receiptNumber"),
    @NamedQuery(name = "StudentLedger.findByStudentBillType", query = "SELECT s FROM StudentLedger s WHERE s.studentBillType = :studentBillType"),
    @NamedQuery(name = "StudentLedger.findByStudentBillTypeInitials", query = "SELECT s FROM StudentLedger s WHERE s.studentBillTypeInitials = :studentBillTypeInitials"),
    @NamedQuery(name = "StudentLedger.findByBillSettledBy", query = "SELECT s FROM StudentLedger s WHERE s.billSettledBy = :billSettledBy"),
    @NamedQuery(name = "StudentLedger.findByMediumOfPayment", query = "SELECT s FROM StudentLedger s WHERE s.mediumOfPayment = :mediumOfPayment"),
    @NamedQuery(name = "StudentLedger.findByMediumOfPaymentNumber", query = "SELECT s FROM StudentLedger s WHERE s.mediumOfPaymentNumber = :mediumOfPaymentNumber"),
    @NamedQuery(name = "StudentLedger.findByRecordedBy", query = "SELECT s FROM StudentLedger s WHERE s.recordedBy = :recordedBy"),
    @NamedQuery(name = "StudentLedger.findBySchoolNumber", query = "SELECT s FROM StudentLedger s WHERE s.schoolNumber = :schoolNumber"),
    @NamedQuery(name = "StudentLedger.findByDeleted", query = "SELECT s FROM StudentLedger s WHERE s.deleted = :deleted"),
    @NamedQuery(name = "StudentLedger.findByUpdated", query = "SELECT s FROM StudentLedger s WHERE s.updated = :updated")})
public class StudentLedger implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "student_ledger_id")
    private String studentLedgerId;
    @Size(max = 255)
    @Column(name = "student_id")
    private String studentId;
    @Size(max = 255)
    @Column(name = "term_of_entry")
    private String termOfEntry;
    @Column(name = "date_of_entry")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateOfEntry;
    @Column(name = "date_of_payment")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateOfPayment;
    @Size(max = 255)
    @Column(name = "type_of_entry")
    private String typeOfEntry;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "amount_involved")
    private Double amountInvolved;
    @Size(max = 255)
    @Column(name = "receipt_number")
    private String receiptNumber;
    @Size(max = 255)
    @Column(name = "student_bill_type")
    private String studentBillType;
    @Size(max = 255)
    @Column(name = "student_bill_type_initials")
    private String studentBillTypeInitials;
    @Size(max = 255)
    @Column(name = "bill_settled_by")
    private String billSettledBy;
    @Size(max = 255)
    @Column(name = "medium_of_payment")
    private String mediumOfPayment;
    @Size(max = 255)
    @Column(name = "medium_of_payment_number")
    private String mediumOfPaymentNumber;
    @Size(max = 255)
    @Column(name = "recorded_by")
    private String recordedBy;
    @Size(max = 255)
    @Column(name = "school_number")
    private String schoolNumber;
    @Size(max = 5)
    @Column(name = "deleted")
    private String deleted;
    @Size(max = 5)
    @Column(name = "updated")
    private String updated;

    public StudentLedger() {
    }

    public StudentLedger(String studentLedgerId) {
        this.studentLedgerId = studentLedgerId;
    }

    public String getStudentLedgerId() {
        return studentLedgerId;
    }

    public void setStudentLedgerId(String studentLedgerId) {
        this.studentLedgerId = studentLedgerId;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getTermOfEntry() {
        return termOfEntry;
    }

    public void setTermOfEntry(String termOfEntry) {
        this.termOfEntry = termOfEntry;
    }

    public Date getDateOfEntry() {
        return dateOfEntry;
    }

    public void setDateOfEntry(Date dateOfEntry) {
        this.dateOfEntry = dateOfEntry;
    }

    public Date getDateOfPayment() {
        return dateOfPayment;
    }

    public void setDateOfPayment(Date dateOfPayment) {
        this.dateOfPayment = dateOfPayment;
    }

    public String getTypeOfEntry() {
        return typeOfEntry;
    }

    public void setTypeOfEntry(String typeOfEntry) {
        this.typeOfEntry = typeOfEntry;
    }

    public Double getAmountInvolved() {
        return amountInvolved;
    }

    public void setAmountInvolved(Double amountInvolved) {
        this.amountInvolved = amountInvolved;
    }

    public String getReceiptNumber() {
        return receiptNumber;
    }

    public void setReceiptNumber(String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    public String getStudentBillType() {
        return studentBillType;
    }

    public void setStudentBillType(String studentBillType) {
        this.studentBillType = studentBillType;
    }

    public String getStudentBillTypeInitials() {
        return studentBillTypeInitials;
    }

    public void setStudentBillTypeInitials(String studentBillTypeInitials) {
        this.studentBillTypeInitials = studentBillTypeInitials;
    }

    public String getBillSettledBy() {
        return billSettledBy;
    }

    public void setBillSettledBy(String billSettledBy) {
        this.billSettledBy = billSettledBy;
    }

    public String getMediumOfPayment() {
        return mediumOfPayment;
    }

    public void setMediumOfPayment(String mediumOfPayment) {
        this.mediumOfPayment = mediumOfPayment;
    }

    public String getMediumOfPaymentNumber() {
        return mediumOfPaymentNumber;
    }

    public void setMediumOfPaymentNumber(String mediumOfPaymentNumber) {
        this.mediumOfPaymentNumber = mediumOfPaymentNumber;
    }

    public String getRecordedBy() {
        return recordedBy;
    }

    public void setRecordedBy(String recordedBy) {
        this.recordedBy = recordedBy;
    }

    public String getSchoolNumber() {
        return schoolNumber;
    }

    public void setSchoolNumber(String schoolNumber) {
        this.schoolNumber = schoolNumber;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (studentLedgerId != null ? studentLedgerId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StudentLedger)) {
            return false;
        }
        StudentLedger other = (StudentLedger) object;
        if ((this.studentLedgerId == null && other.studentLedgerId != null) || (this.studentLedgerId != null && !this.studentLedgerId.equals(other.studentLedgerId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sabonay.timetable.entities.StudentLedger[ studentLedgerId=" + studentLedgerId + " ]";
    }
    
}
