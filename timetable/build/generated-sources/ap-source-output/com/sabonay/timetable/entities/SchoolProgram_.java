package com.sabonay.timetable.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-02-27T15:02:50")
@StaticMetamodel(SchoolProgram.class)
public class SchoolProgram_ { 

    public static volatile SingularAttribute<SchoolProgram, String> updated;
    public static volatile SingularAttribute<SchoolProgram, Integer> viewOrder;
    public static volatile SingularAttribute<SchoolProgram, String> programCode;
    public static volatile SingularAttribute<SchoolProgram, String> deleted;
    public static volatile SingularAttribute<SchoolProgram, String> programName;

}