package com.sabonay.timetable.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-02-27T15:02:50")
@StaticMetamodel(ReportComment.class)
public class ReportComment_ { 

    public static volatile SingularAttribute<ReportComment, String> reportCommentId;
    public static volatile SingularAttribute<ReportComment, String> updated;
    public static volatile SingularAttribute<ReportComment, String> schoolNumber;
    public static volatile SingularAttribute<ReportComment, String> lastModifiedBy;
    public static volatile SingularAttribute<ReportComment, Date> lastModifiedDate;
    public static volatile SingularAttribute<ReportComment, String> comment;
    public static volatile SingularAttribute<ReportComment, String> type;
    public static volatile SingularAttribute<ReportComment, String> deleted;

}