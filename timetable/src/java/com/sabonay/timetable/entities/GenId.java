/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sabonay.timetable.entities;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Amina
 */
@Entity
@Table(name = "gen_id")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "GenId.findAll", query = "SELECT g FROM GenId g"),
    @NamedQuery(name = "GenId.findByPrimaryKey", query = "SELECT g FROM GenId g WHERE g.genIdPK.primaryKey = :primaryKey"),
    @NamedQuery(name = "GenId.findByExtraInfo", query = "SELECT g FROM GenId g WHERE g.genIdPK.extraInfo = :extraInfo"),
    @NamedQuery(name = "GenId.findById", query = "SELECT g FROM GenId g WHERE g.id = :id"),
    @NamedQuery(name = "GenId.findByIdBig", query = "SELECT g FROM GenId g WHERE g.idBig = :idBig"),
    @NamedQuery(name = "GenId.findByApplication", query = "SELECT g FROM GenId g WHERE g.genIdPK.application = :application")})
public class GenId implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected GenIdPK genIdPK;
    @Column(name = "id")
    private Integer id;
    @Column(name = "id_big")
    private BigInteger idBig;

    public GenId() {
    }

    public GenId(GenIdPK genIdPK) {
        this.genIdPK = genIdPK;
    }

    public GenId(String primaryKey, String extraInfo, String application) {
        this.genIdPK = new GenIdPK(primaryKey, extraInfo, application);
    }

    public GenIdPK getGenIdPK() {
        return genIdPK;
    }

    public void setGenIdPK(GenIdPK genIdPK) {
        this.genIdPK = genIdPK;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigInteger getIdBig() {
        return idBig;
    }

    public void setIdBig(BigInteger idBig) {
        this.idBig = idBig;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (genIdPK != null ? genIdPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GenId)) {
            return false;
        }
        GenId other = (GenId) object;
        if ((this.genIdPK == null && other.genIdPK != null) || (this.genIdPK != null && !this.genIdPK.equals(other.genIdPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sabonay.timetable.entities.GenId[ genIdPK=" + genIdPK + " ]";
    }
    
}
