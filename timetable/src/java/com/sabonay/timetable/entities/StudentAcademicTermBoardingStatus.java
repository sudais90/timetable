/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sabonay.timetable.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Amina
 */
@Entity
@Table(name = "student_academic_term_boarding_status")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "StudentAcademicTermBoardingStatus.findAll", query = "SELECT s FROM StudentAcademicTermBoardingStatus s"),
    @NamedQuery(name = "StudentAcademicTermBoardingStatus.findByStudentAcademicTermBoardingStatusId", query = "SELECT s FROM StudentAcademicTermBoardingStatus s WHERE s.studentAcademicTermBoardingStatusId = :studentAcademicTermBoardingStatusId"),
    @NamedQuery(name = "StudentAcademicTermBoardingStatus.findByStudent", query = "SELECT s FROM StudentAcademicTermBoardingStatus s WHERE s.student = :student"),
    @NamedQuery(name = "StudentAcademicTermBoardingStatus.findByAcademicTerm", query = "SELECT s FROM StudentAcademicTermBoardingStatus s WHERE s.academicTerm = :academicTerm"),
    @NamedQuery(name = "StudentAcademicTermBoardingStatus.findByBoardingStatus", query = "SELECT s FROM StudentAcademicTermBoardingStatus s WHERE s.boardingStatus = :boardingStatus"),
    @NamedQuery(name = "StudentAcademicTermBoardingStatus.findBySchoolNumber", query = "SELECT s FROM StudentAcademicTermBoardingStatus s WHERE s.schoolNumber = :schoolNumber"),
    @NamedQuery(name = "StudentAcademicTermBoardingStatus.findByLastModifiedDate", query = "SELECT s FROM StudentAcademicTermBoardingStatus s WHERE s.lastModifiedDate = :lastModifiedDate"),
    @NamedQuery(name = "StudentAcademicTermBoardingStatus.findByLastModifiedBy", query = "SELECT s FROM StudentAcademicTermBoardingStatus s WHERE s.lastModifiedBy = :lastModifiedBy"),
    @NamedQuery(name = "StudentAcademicTermBoardingStatus.findByDeleted", query = "SELECT s FROM StudentAcademicTermBoardingStatus s WHERE s.deleted = :deleted"),
    @NamedQuery(name = "StudentAcademicTermBoardingStatus.findByUpdated", query = "SELECT s FROM StudentAcademicTermBoardingStatus s WHERE s.updated = :updated")})
public class StudentAcademicTermBoardingStatus implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "student_academic_term_boarding_status_id")
    private String studentAcademicTermBoardingStatusId;
    @Size(max = 45)
    @Column(name = "student")
    private String student;
    @Size(max = 45)
    @Column(name = "academic_term")
    private String academicTerm;
    @Size(max = 45)
    @Column(name = "boarding_status")
    private String boardingStatus;
    @Size(max = 45)
    @Column(name = "school_number")
    private String schoolNumber;
    @Column(name = "last_modified_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;
    @Size(max = 45)
    @Column(name = "last_modified_by")
    private String lastModifiedBy;
    @Size(max = 45)
    @Column(name = "deleted")
    private String deleted;
    @Size(max = 45)
    @Column(name = "updated")
    private String updated;

    public StudentAcademicTermBoardingStatus() {
    }

    public StudentAcademicTermBoardingStatus(String studentAcademicTermBoardingStatusId) {
        this.studentAcademicTermBoardingStatusId = studentAcademicTermBoardingStatusId;
    }

    public String getStudentAcademicTermBoardingStatusId() {
        return studentAcademicTermBoardingStatusId;
    }

    public void setStudentAcademicTermBoardingStatusId(String studentAcademicTermBoardingStatusId) {
        this.studentAcademicTermBoardingStatusId = studentAcademicTermBoardingStatusId;
    }

    public String getStudent() {
        return student;
    }

    public void setStudent(String student) {
        this.student = student;
    }

    public String getAcademicTerm() {
        return academicTerm;
    }

    public void setAcademicTerm(String academicTerm) {
        this.academicTerm = academicTerm;
    }

    public String getBoardingStatus() {
        return boardingStatus;
    }

    public void setBoardingStatus(String boardingStatus) {
        this.boardingStatus = boardingStatus;
    }

    public String getSchoolNumber() {
        return schoolNumber;
    }

    public void setSchoolNumber(String schoolNumber) {
        this.schoolNumber = schoolNumber;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (studentAcademicTermBoardingStatusId != null ? studentAcademicTermBoardingStatusId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StudentAcademicTermBoardingStatus)) {
            return false;
        }
        StudentAcademicTermBoardingStatus other = (StudentAcademicTermBoardingStatus) object;
        if ((this.studentAcademicTermBoardingStatusId == null && other.studentAcademicTermBoardingStatusId != null) || (this.studentAcademicTermBoardingStatusId != null && !this.studentAcademicTermBoardingStatusId.equals(other.studentAcademicTermBoardingStatusId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sabonay.timetable.entities.StudentAcademicTermBoardingStatus[ studentAcademicTermBoardingStatusId=" + studentAcademicTermBoardingStatusId + " ]";
    }
    
}
