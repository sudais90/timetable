package com.sabonay.timetable.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-02-27T15:02:50")
@StaticMetamodel(SchoolSubjectClassTeacher.class)
public class SchoolSubjectClassTeacher_ { 

    public static volatile SingularAttribute<SchoolSubjectClassTeacher, String> classTeacher;
    public static volatile SingularAttribute<SchoolSubjectClassTeacher, String> updated;
    public static volatile SingularAttribute<SchoolSubjectClassTeacher, String> schoolSubjectId;
    public static volatile SingularAttribute<SchoolSubjectClassTeacher, String> schoolSubjectClassTeacherId;
    public static volatile SingularAttribute<SchoolSubjectClassTeacher, String> deleted;

}